#include "stdafx.h"
#include <Windows.h>
#include <string.h>
#include <math.h>
#include <sstream>
#include <iostream>
#define INITGUID
#include "../sea/PacketMaker.h"

class TestPacketReceiver : public IPacketReceiver
{
public:
	void ReceieveNPHandshake (nsqn_t sequence_number, const std::wstring& alias) {throw std::exception ("ReceieveNPHandshake: Unexpected call !");}
	void ReceieveNPHandshakeResponse (nsqn_t sequence_number, const std::wstring& alias, int gameboard_size, bool is_server_move_first, const WarShipPrototypeList& prototypes) {throw std::exception ("ReceieveNPHandshakeResponse: Unexpected call !");}
	void ReceieveNPClientStartGame (nsqn_t sequence_number, gen_t event_number) {throw std::exception ("ReceieveNPClientStartGame: Unexpected call !");}
	void ReceieveNPFire (nsqn_t sequence_number, gen_t event_number, const AbsBoardPos& fire_pos) {throw std::exception ("ReceieveNPFire: Unexpected call !");}
	void ReceieveNPFireResponse (nsqn_t sequence_number, gen_t event_number, gen_t request_sequence_number, FireResult fire_result) {throw std::exception ("ReceieveNPFireResponse: Unexpected call !");}
	void ReceieveNPEndGame (nsqn_t sequence_number, gen_t event_number, EndGameState end_gsme_state, std::shared_ptr<GameBoard> game_board) {throw std::exception ("ReceieveNPEndGame: Unexpected call !");}
	void ReceieveNPPing (nsqn_t sequence_number) {throw std::exception ("ReceieveNPPing: Unexpected call !");}
	void ReceieveNPPingResponse (nsqn_t sequence_number) {throw std::exception ("ReceieveNPPingResponse: Unexpected call !");}
};


enum TestOption
{
	to_nothing = 0,
	to_max_size,
	to_min_size
};

int LimRand (int limit)
{
	return limit * rand () / RAND_MAX;
}
wchar_t CharRand ()
{
	if (LimRand (1))
	{
		return L'A' + LimRand (26);
	}
	else
	{
		return L'a' + LimRand (26);
	}
}


class ITest
{
public:
	virtual bool Run () = 0;
	virtual bool Run (TestOption option) = 0;

	void Out () {wprintf (L"%s", m_out.str ().c_str ()); m_out.str (L"");}

protected:
	std::wostringstream m_out;

};

class TestNPHandshake : public ITest
{
public:
	bool Run ();
	bool Run (TestOption option);
};

bool TestNPHandshake::Run ()
{
	return Run (to_nothing);
}

bool TestNPHandshake::Run (TestOption option)
{
	std::wstring test_alias = L"";

	int test_alias_length;

	switch (option)
	{
	case to_max_size:
		test_alias_length = npl_max_alias_length -1;
		break;
	case to_min_size:
		test_alias_length = 1;
		break;
	default:
		test_alias_length = LimRand (npl_max_alias_length -1);
		break;
	}

	for (int counter = test_alias_length;counter > 0;counter = counter -1)
	{
		test_alias += CharRand ();
	}

	nsqn_t test_sequence_number = rand ();

	try
	{
		size_t np_buffer_size = ProjectNPHandshake (test_alias);
		void* np_buffer = malloc (np_buffer_size);

		FillNPHandshake (np_buffer, test_sequence_number, test_alias);

		class TestNPHandshakeReceiver : public TestPacketReceiver
		{
		public:
			TestNPHandshakeReceiver (std::wstring alias, nsqn_t sequence_number, bool& result, std::wostringstream& out) : 
				m_test_alias (alias),
				m_sequence_number (sequence_number),
				m_result (result), m_out (out) {}

			void ReceieveNPHandshake (nsqn_t sequence_number, const std::wstring& alias)
			{
				m_result = (alias == m_test_alias && sequence_number == m_sequence_number);

				m_out << L"TestNPHandshake:\n\t#Test alias: " << m_test_alias << L"\n\t#Test sn: " << m_sequence_number << L"\n\n\t#Result alias: " << alias << L"\n\t#Result sn: " << sequence_number << L"\n";
			}
		private:
			nsqn_t m_sequence_number;
			std::wstring m_test_alias;
			std::wostringstream& m_out;
			bool& m_result;
		};

		bool return_val;

		TestNPHandshakeReceiver test_receiver (test_alias, test_sequence_number, return_val, m_out);

		ParseNetworkPacket (np_buffer, np_buffer_size, test_receiver);

		return return_val;
	}
	catch (const std::exception& e)
	{
		m_out << L"TestNPHandshake:\n#Exception catched !\n\t" << AnsiToUnicode (e.what ()) << L"\n";

		return false;
	}
}


class TestNPHanshakeResponse : public ITest
{
public:
	bool Run ();
	bool Run (TestOption option);
};

bool TestNPHanshakeResponse::Run ()
{
	return Run (to_nothing);
}

void FillPrototypesList (WarShipPrototypeList& prototype_list, int amount)
{
	amount = amount % 3;

	RelBoardPosList prototype_a_list;
	prototype_a_list.push_back (RelBoardPos (0, 0));
	prototype_a_list.push_back (RelBoardPos (1, 0));
	prototype_a_list.push_back (RelBoardPos (2, 0));
	prototype_a_list.push_back (RelBoardPos (0, 1));
	WarShipPrototypePtr prototype_a (new WarShipPrototype (prototype_a_list, 2));
	prototype_a->Sort ();
	prototype_list.push_back (prototype_a);

	if (amount == 0) return;

	RelBoardPosList prototype_b_list;
	prototype_b_list.push_back (RelBoardPos (0, 0));
	WarShipPrototypePtr prototype_b (new WarShipPrototype (prototype_b_list, 5));
	prototype_b->Sort ();
	prototype_list.push_back (prototype_b);

	if (amount == 1) return;

	RelBoardPosList prototype_c_list;
	prototype_c_list.push_back (RelBoardPos (0, 0));
	prototype_c_list.push_back (RelBoardPos (1, 0));
	WarShipPrototypePtr prototype_c (new WarShipPrototype (prototype_b_list, 3));
	prototype_c->Sort ();
	prototype_list.push_back (prototype_c);
}

bool TestNPHanshakeResponse::Run (TestOption option)
{
	std::wstring test_alias = L"";
	nsqn_t test_sequence_number = rand ();
	int test_gameboard_size = 0;
	int test_is_server_move_first = LimRand (1);
	WarShipPrototypeList test_prototypes;

	int test_alias_length;
	int test_prototypes_amount;

	switch (option)
	{
	case to_max_size:
		test_alias_length = npl_max_alias_length -1;
		test_prototypes_amount = 2;
		break;
	case to_min_size:
		test_alias_length = 1;
		test_prototypes_amount = 1;
		break;
	default:
		test_alias_length = LimRand (npl_max_alias_length -1);
		test_prototypes_amount = LimRand (2);
		break;
	}	

	for (int counter = test_alias_length;counter > 0;counter = counter -1)
	{
		test_alias += CharRand ();
	}

	for (;test_gameboard_size < 5 || test_gameboard_size > 20;)
	{
		test_gameboard_size = LimRand (20);
	}

	FillPrototypesList (test_prototypes, test_prototypes_amount);


	try
	{
		size_t np_buffer_size = ProjectNPHandshakeResponse (test_alias, test_gameboard_size, 0 != test_is_server_move_first, test_prototypes);
		void* np_buffer = malloc (np_buffer_size);

		FillNPHandshakeResponse (np_buffer, test_sequence_number, test_alias, test_gameboard_size, 0 != test_is_server_move_first, test_prototypes);

		class TestNPHandshakeResponseReceiver : public TestPacketReceiver
		{
		public:
			TestNPHandshakeResponseReceiver (std::wstring alias, nsqn_t sequence_number, int gameboard_size, int is_server_move_first, WarShipPrototypeList& prototypes, bool& result, std::wostringstream& out) : 
				m_alias (alias),
				m_sequence_number (sequence_number),
				m_gameboard_size (gameboard_size),
				m_is_server_move_first (is_server_move_first),
				m_prototypes (prototypes),
				m_result (result), m_out (out) {}

			void ReceieveNPHandshakeResponse (nsqn_t sequence_number, const std::wstring& alias, int gameboard_size, bool is_server_move_first, const WarShipPrototypeList& prototypes)
			{
				m_result = (alias == m_alias && sequence_number == m_sequence_number && m_gameboard_size == gameboard_size && (0 != m_is_server_move_first) == is_server_move_first);

				WarShipPrototypeList::const_iterator counter = prototypes.cbegin ();
				WarShipPrototypeList::const_iterator m_counter = m_prototypes.cbegin ();

				for (;counter != prototypes.cend () && m_counter != m_prototypes.cend ();counter ++, m_counter ++)
				{
					m_result = (m_result && ArePrototypesEqual (*counter, *m_counter));
				}

				m_out << L"TestNPHandshakeResponse:\n\t#Test alias: " << m_alias << L"\n\t#Test sn: " << m_sequence_number << L"\n\t#Test gb size: " << m_gameboard_size << L"\n\t#Test server move: " << m_is_server_move_first
					<< L"\n\n\t#Result alias: " << alias << L"\n\t#Result sn: " << sequence_number << L"\n\t#Result gb size: " << gameboard_size << L"\n\t#Result server move: " << is_server_move_first << L"\n";
					
			}
		private:
			nsqn_t m_sequence_number;
			int m_gameboard_size;
			int m_is_server_move_first;
			std::wstring m_alias;
			WarShipPrototypeList& m_prototypes;
			std::wostringstream& m_out;
			bool& m_result;
		};

		bool return_val;

		TestNPHandshakeResponseReceiver test_receiver (test_alias, test_sequence_number, test_gameboard_size, test_is_server_move_first, test_prototypes, return_val, m_out);

		ParseNetworkPacket (np_buffer, np_buffer_size, test_receiver);

		return return_val;
	}
	catch (const std::exception& e)
	{
		m_out << L"TestNPHandshakeResponseReceiver:\n#Exception catched !\n\t" << AnsiToUnicode (e.what ()) << L"\n";

		return false;
	}
}

class TestNPClientStartGame : public ITest
{
public:
	bool Run ();
	bool Run (TestOption option);
};

bool TestNPClientStartGame::Run ()
{
	return Run (to_nothing);
}

bool TestNPClientStartGame::Run (TestOption option)
{
	nsqn_t test_sequence_number = rand ();

	try
	{
		size_t np_buffer_size = ProjectNPClientStartGame ();
		void* np_buffer = malloc (np_buffer_size);

		FillNPClientStartGame (np_buffer, test_sequence_number, 1);

		class TestReceiver : public TestPacketReceiver
		{
		public:
			TestReceiver (nsqn_t sequence_number, bool& result, std::wostringstream& out) :
				m_sequence_number (sequence_number),
				m_result (result), m_out (out) {}

			void ReceieveNPClientStartGame (nsqn_t sequence_number, gen_t event_number)
			{
				m_result = (sequence_number == m_sequence_number);

				m_out << L"TestNPClientStartGame:\n\t" << L"\n\t#Test sn: " << m_sequence_number << L"\n\t#Result sn: " << sequence_number << L"\n";
					
			}
		private:
			nsqn_t m_sequence_number;
			std::wostringstream& m_out;
			bool& m_result;
		};

		bool return_val;

		TestReceiver test_receiver (test_sequence_number, return_val, m_out);

		ParseNetworkPacket (np_buffer, np_buffer_size, test_receiver);

		return return_val;
	}
	catch (const std::exception& e)
	{
		m_out << L"TestNPClientStartGame:\n#Exception catched !\n\t" << AnsiToUnicode (e.what ()) << L"\n";

		return false;
	}
}


class TestNPFire : public ITest
{
public:
	bool Run ();
	bool Run (TestOption option);
};

bool TestNPFire::Run ()
{
	return Run (to_nothing);
}

bool TestNPFire::Run (TestOption option)
{
	nsqn_t test_sequence_number = rand ();
	AbsBoardPos fire_position (LimRand (5), LimRand (5));

	try
	{
		size_t np_buffer_size = ProjectNPFire (fire_position);
		void* np_buffer = malloc (np_buffer_size);

		FillNPFire (np_buffer, test_sequence_number, fire_position, 1);

		class TestReceiver : public TestPacketReceiver
		{
		public:
			TestReceiver (nsqn_t sequence_number, const AbsBoardPos& fire_position, bool& result, std::wostringstream& out) :
				m_sequence_number (sequence_number),
				m_fire_position(fire_position),
				m_result (result), m_out (out) {}

			void ReceieveNPFire (nsqn_t sequence_number, gen_t event_number, const AbsBoardPos& fire_pos)
			{
				m_result = (sequence_number == m_sequence_number) && (fire_pos == m_fire_position);

				m_out << L"TestTestNPFire:\n\t"
					<< L"\n\t#Test sn: " << m_sequence_number
					<< L"\n\t#Test fp: (" << m_fire_position.x << L"," << m_fire_position.y << L")"
					<< L"\n\t#Result sn: " << sequence_number
					<< L"\n\t#Result fp: (" << fire_pos.x << L"," << fire_pos.y << L")"
					<< L"\n";
					
			}
		private:
			nsqn_t m_sequence_number;
			const AbsBoardPos& m_fire_position;
			std::wostringstream& m_out;
			bool& m_result;
		};

		bool return_val;

		TestReceiver test_receiver (test_sequence_number, fire_position, return_val, m_out);

		ParseNetworkPacket (np_buffer, np_buffer_size, test_receiver);

		return return_val;
	}
	catch (const std::exception& e)
	{
		m_out << L"TestTestNPFire:\n#Exception catched !\n\t" << AnsiToUnicode (e.what ()) << L"\n";

		return false;
	}
}



class TestNPFireResponse : public ITest
{
public:
	bool Run ();
	bool Run (TestOption option);
};

bool TestNPFireResponse::Run ()
{
	return Run (to_nothing);
}

bool TestNPFireResponse::Run (TestOption option)
{
	nsqn_t test_sequence_number = rand ();
	gen_t test_request_sequence_number = rand ();
	FireResult test_fire_result  = (FireResult) LimRand (fr_kill);

	try
	{
		size_t np_buffer_size = ProjectNPFireResponse (test_request_sequence_number, test_fire_result);
		void* np_buffer = malloc (np_buffer_size);

		FillNPFireResponse (np_buffer, test_sequence_number, test_request_sequence_number, test_fire_result, 1);

		class TestReceiver : public TestPacketReceiver
		{
		public:
			TestReceiver (nsqn_t sequence_number, gen_t request_sequence_number, const FireResult& fire_result, bool& result, std::wostringstream& out) :
				m_sequence_number (sequence_number),
				m_request_sequence_number (request_sequence_number),
				m_fire_result (fire_result),
				m_result (result), m_out (out) {}

			void ReceieveNPFireResponse (nsqn_t sequence_number, gen_t event_number, gen_t request_sequence_number, FireResult fire_result)
			{
				m_result = (sequence_number == m_sequence_number) && (fire_result == m_fire_result);

				m_out << L"TestNPFireResponse:\n\t"
					<< L"\n\t#Test sn: " << m_sequence_number
					<< L"\n\t#Test request sn: " << m_request_sequence_number
					<< L"\n\t#Test fire result: " << m_fire_result
					<< L"\n\t#Result sn: " << sequence_number
					<< L"\n\t#Result request sn: " << request_sequence_number
					<< L"\n\t#Result fire result: " << m_fire_result
					<< L"\n";
					
			}
		private:
			nsqn_t m_sequence_number;
			gen_t m_request_sequence_number;
			FireResult m_fire_result;
			std::wostringstream& m_out;
			bool& m_result;
		};

		bool return_val;

		TestReceiver test_receiver (test_sequence_number, test_request_sequence_number, test_fire_result, return_val, m_out);

		ParseNetworkPacket (np_buffer, np_buffer_size, test_receiver);

		return return_val;
	}
	catch (const std::exception& e)
	{
		m_out << L"TestNPFireResponse:\n#Exception catched !\n\t" << AnsiToUnicode (e.what ()) << L"\n";

		return false;
	}
}


class TestNPEndGame : public ITest
{
public:
	bool Run ();
	bool Run (TestOption option);
};

bool TestNPEndGame::Run ()
{
	return Run (to_nothing);
}

bool TestNPEndGame::Run (TestOption option)
{
	nsqn_t test_sequence_number = rand ();
	int test_gameboard_size = 0;
	EndGameState test_game_state = (EndGameState) LimRand (egs_canceled);
	GameBoard test_gameboard;

	switch (option)
	{
	case to_max_size:
		test_gameboard_size = GameBoard::max_size;
		break;
	case to_min_size:
		test_gameboard_size = GameBoard::min_size;
		break;
	default:
		{
			for (;test_gameboard_size < GameBoard::min_size;)
			{
				test_gameboard_size = LimRand (GameBoard::max_size);
			}
			break;
		}
	}	

	test_gameboard.CreateEmpty (test_gameboard_size);

	for (int x = 0;x < test_gameboard_size;x = x + 1)
	{
		for (int y = 0;y < test_gameboard_size;y = y + 1)
		{
			test_gameboard.Cell (AbsBoardPos (x, y)).cell_type = (LimRand (1)) ? ct_ship : ct_empty;
		}
	}


	try
	{
		size_t np_buffer_size = ProjectNPEndGame (test_game_state, test_gameboard);
		void* np_buffer = malloc (np_buffer_size);

		FillNPEndGame (np_buffer, test_sequence_number, test_game_state, test_gameboard, 1);

		class TestReceiver : public TestPacketReceiver
		{
		public:
			TestReceiver (nsqn_t sequence_number, EndGameState game_state, const GameBoard& gameboard, bool& result, std::wostringstream& out) : 
				m_sequence_number (sequence_number),
				m_game_state (game_state),
				m_gameboard (gameboard),
				m_result (result), m_out (out) {}

			void ReceieveNPEndGame (nsqn_t sequence_number, gen_t event_number, EndGameState end_gsme_state, const GameBoard& game_board)
			{
				m_result = (sequence_number == m_sequence_number && m_game_state == end_gsme_state);

				int gb_size = game_board.GetSize ();

				if (gb_size == m_gameboard.GetSize ())
				{
					for (int x = 0;x < gb_size;x = x + 1)
					{
						for (int y = 0;y < gb_size;y = y + 1)
						{
							if (game_board.Cell (AbsBoardPos (x, y)).cell_type != m_gameboard.Cell (AbsBoardPos (x, y)).cell_type)
							{
								m_result = false;
							}
						}
					}
				}
				else
				{
					m_result = false;
				}

				m_out << L"TestNPEndGame:\n\t"
					<< L"\n\t#Test sn: " << m_sequence_number
					<< L"\n\t#Test egs: " << m_game_state
					<< L"\n\t#Result sn: " << sequence_number
					<< L"\n\t#Result egs: " << m_game_state
					<< L"\n";	
			}
		private:
			nsqn_t m_sequence_number;
			const GameBoard& m_gameboard;
			EndGameState m_game_state;
			std::wostringstream& m_out;
			bool& m_result;
		};

		bool return_val;

		TestReceiver test_receiver (test_sequence_number, test_game_state, test_gameboard, return_val, m_out);

		ParseNetworkPacket (np_buffer, np_buffer_size, test_receiver);

		return return_val;
	}
	catch (const std::exception& e)
	{
		m_out << L"TestNPEndGame:\n#Exception catched !\n\t" << AnsiToUnicode (e.what ()) << L"\n";

		return false;
	}
}

void TestNPPing ()
{
}

void TestNPPingResponse ()
{
}


class TestExecuter
{
public:
	TestExecuter () : m_passed (0), m_failed (0) {}
	
	void Execute (ITest& test);
	void Execute (ITest& test, int additional_tests_amount);

	enum
	{
		default_tests_amount = 5
	};

private:
	void Finished (bool result) {if (result) Passed (); else Failed ();}

	void Passed () {wprintf (L"Test PASSED !\n"); m_passed = m_passed +1;};
	void Failed () {wprintf (L"Test FAILED !\n"); m_failed = m_failed +1;};
	
	void Out ();

	int m_passed;
	int m_failed;
};

void TestExecuter::Execute (ITest& test)
{
	Execute (test, default_tests_amount);
}

void TestExecuter::Execute (ITest& test, int tests_amount)
{
	Finished (test.Run (to_max_size));
	test.Out ();

	Finished (test.Run (to_min_size));
	test.Out ();

	for (int counter = tests_amount -2; counter > 0; counter = counter -1)
	{
		Finished (test.Run ());

		test.Out ();
	}

	Out ();
}

void TestExecuter::Out ()
{
	wprintf (L"%d tests finished.\n\t# Passed: %d\n\t# Failed: %d\n", m_passed + m_failed, m_passed, m_failed);
}


void main()
{
	srand (GetTickCount ());

	TestExecuter executer;

	executer.Execute (TestNPHandshake ());
	executer.Execute (TestNPHanshakeResponse ());
	executer.Execute (TestNPClientStartGame ());
	executer.Execute (TestNPFire ());
	executer.Execute (TestNPFireResponse ());
	executer.Execute (TestNPEndGame ());

	_gettch ();
}
