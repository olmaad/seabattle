// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <string>

inline std::wstring AnsiToUnicode (const std::string& src_string, std::locale loc  = std::locale())
{
	std::wstring out_string (src_string.length(), 0);
	std::string::const_iterator i = src_string.begin(), ie = src_string.end();
	std::wstring::iterator j = out_string.begin();

	for( ; i!=ie; ++i, ++j )
		*j = std::use_facet< std::ctype< wchar_t > > ( loc ).widen( *i );
	
	return out_string;
}

// TODO: reference additional headers your program requires here
