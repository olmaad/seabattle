//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by sea.rc
//
#define IDC_MYICON                      2
#define IDD_SEA_DIALOG                  102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_SEA                         107
#define IDI_SMALL                       108
#define IDC_SEA                         109
#define IDR_MAINFRAME                   128
#define IDD_MAINBOX                     129
#define IDD_CREATEGAME                  130
#define IDD_PLACESHIPS                  131
#define IDD_JOINGAME                    133
#define IDD_WAITINGBOX                  134
#define IDD_GAMEWINDOW                  136
#define IDD_PLAYEMULATOR                137
#define IDC_CREATE                      1000
#define IDC_CREATENEWGAME               1000
#define IDC_SHIP_TORPEDO                1000
#define IDC_BUTTON2                     1001
#define IDC_JOINGAME                    1001
#define IDC_EDIT1                       1002
#define IDC_GAMENAME                    1002
#define IDC_EDIT2                       1003
#define IDC_SERVERADRESS                1003
#define IDC_EMULATOR                    1003
#define IDC_EDIT3                       1004
#define IDC_SERVERPORT                  1004
#define IDC_BUTTON3                     1005
#define IDC_SHIP_MOVE                   1005
#define IDC_BOARDSIZE                   1006
#define IDC_CREATENEXT                  1007
#define IDC_GAMENAME_FEEDBACK           1008
#define IDC_SERVERADRESS_FEEDBACK       1009
#define IDC_SERVERPORT_FEEDBACK         1010
#define IDC_BOARDSIZE_FEEDBACK          1011
#define IDC_JOINNEXT                    1012
#define IDC_STATUS                      1014
#define IDC_STATUSGB                    1015
#define IDC_GAMESTATUS                  1016
#define IDC_SURRENDER                   1017
#define IDC_DEXPERT                     1018
#define IDC_SURRENDER2                  1018
#define IDC_EXIT                        1018
#define IDC_DNOOB                       1019
#define IDC_FTME                        1020
#define IDC_FTENEMY                     1021
#define IDC_EMULATORNEXT                1022
#define IDC_FIRSTMOVE_ME                1023
#define IDC_FIRSTMOVE_ENEMY             1024
#define IDC_SHIP_SHOT                   1024
#define IDC_SHIP_ROCKET                 1026
#define IDC_SHIP_MAAC                   1027
#define IDC_SHIP_ILLUSION               1028
#define IDC_SHIP_EMP                    1029
#define IDC_SHIP_SCOUT                  1030
#define IDC_SHIP_SCOUTING               1030
#define IDC_SHIP_FIGHTER_DRONE          1031
#define IDC_SHIP_INTERCEPTOR_DRONE      1032
#define IDC_SHIP_PROSPECTOR_DRONE       1033
#define IDC_SHIP_CARPET_BOMBING         1034
#define IDC_SHIP_BARRAGE_FIRE           1035
#define IDC_SHIP_FULL_COMBAT_READINESS  1036
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        138
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1038
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
