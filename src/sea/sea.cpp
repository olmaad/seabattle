#include "stdafx.h"

#include "sea.h"

#include "ui/seawindow.h"
#include "ui/UI_Main.h"

#include <libGameEngine/GameBoard.h>

#include <Winsock2.h>

#define INITGUID
#include <libNetwork/NetworkProtocol.h>

//todo (global): ПИУ ПИУ !!!

// ############### Global ################# //

HINSTANCE hInst;

// ############### Main ################### //

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	MSG msg;
	HACCEL hAccelTable;

	hInst = hInstance;

	WORD sock_version = MAKEWORD(2, 2);
	WSADATA the_must_useful_information;

	WSAStartup (sock_version, &the_must_useful_information);

	MainWindow* main_window = new MainWindow ();

	main_window->Create (hInstance);

	//todo: добавить обработку исключения (память)
	main_window->Show (nCmdShow);

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_SEA));

	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			if (IsDialogMessage (GetActiveWindow (), &msg))
			{
				continue;
			}

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

#ifdef _DEBUG

	wchar_t output [150];

	swprintf (output, L"\n##############################\nWarShips: %ld, Prototypes: %ld\n##############################\n", WarShip::warship_counter, WarShipPrototype::prototype_counter);

	OutputDebugString (output);

#endif

	WSACleanup();

	return (int) msg.wParam;
}

// ########### END Main ################### //
