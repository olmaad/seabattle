#include "stdafx.h"
#include "resources/resource.h"
#include "UI_Connect.h"


const LPCTSTR WaitingWindow::lpTemplateName = MAKEINTRESOURCE(IDD_WAITINGBOX);


//// WaitingWindow ////////////////////////////////////////////////////////////////////////////////////////////////////


//// Constructor

WaitingWindow::WaitingWindow () : m_parent (0)
{
}


//// Destructor

WaitingWindow::~WaitingWindow ()
{
	if (m_parent)
	{
		m_parent->Show (SW_SHOWNORMAL);
	}
}


//// 

bool WaitingWindow::Create (HINSTANCE hInstance, AbstractTopLvlWindow* parent)
{
	HWND parent_handle = 0;

	if (0 != parent)
	{
		parent_handle = parent->GetHandle ();
	}
	else
	{
		return false;
	}

	if (AbstractTopLvlWindow::Create (hInstance, lpTemplateName, parent_handle))
	{
		m_parent = parent;
		return true;
	}

	return false;
}


void WaitingWindow::NotifyPlayerConnected (std::wstring alias)
{
	std::wstring status = L"Player \"";
	status += alias;
	status += L"\" connected. Waiting for placing ships...";

	SendMessage (GetDlgItem (IDC_STATUS), WM_SETTEXT, 0, (LPARAM) status.c_str ());
}


//// 

LRESULT WaitingWindow::OnCommand (WPARAM wParam, LPARAM lParam)
{
	switch (LOWORD(wParam))
	{
	case IDCANCEL:
		{
			Destroy ();
			return 0;
		}
	default:
		return AbstractTopLvlWindow::OnCommand (wParam, lParam);
	}
}


////

LRESULT WaitingWindow::OnDestroy (HWND hWnd)
{
	delete this;

	return 0;
}
