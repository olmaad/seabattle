#include "stdafx.h"

#include "UI_ShipsController.h"

#include "GameBoardUI.h"

#include <libGameEngine/GameEngine.h>
#include <libGameEngine/IGameBoardShipController.h>


class ShipsController : public IShipController, public IGameBoardShipController
{
public:
	ShipsController (GameBoard& src_game_board);
	~ShipsController ();

	void OnClickLeft (AbsBoardPos cur_pos);
	void OnClickLeft (int prototype_number);
	void OnClickRight (AbsBoardPos cur_pos);

	void OnSelectChange (AbsBoardPos cur_pos);

	void OnReleaseLeft ();
	void OnReleaseRight (AbsBoardPos& cur_pos);

	void NotifyPrototypeAdded (const WarShipPrototype* prototype);
	void NotifyPrototypeDeleted (const WarShipPrototype* prototype);
	void NotifyWarshipCountChanged ();

	// Throws InvalidPrivateParameterStateException
	void BindPrototypeWidget (PrototypeWidget* src_widget)
	{
		if (m_prototype_widget != 0)
		{
			throw InvalidPrivateParameterStateException (L"m_prototype_widget != 0 - prototype widget already binded");
		}

		m_prototype_widget = src_widget;
	}
	// Throws InvalidPrivateParameterStateException
	void UnBindPrototypeWidget (PrototypeWidget* src_widget)
	{
		if (m_prototype_widget != src_widget)
		{
			throw InvalidPrivateParameterStateException (L"m_prototype_widget != src_widget - unbinding unknown widget");
		}

		m_prototype_widget = 0;
	}

	void SetCurrentShipAbility(ShipAbilityType type) {}

	bool IsHost ()
	{
		return m_is_host;
	}

	EditField* GetEdit ();
	NewWarshipField* GetNew ();
private:
	ShipsController (const ShipsController&);
	ShipsController& operator = (const ShipsController&);

	GameBoard& m_gameboard;
	EditFieldPtr m_edit;
	NewWarshipFieldPtr m_new;

	bool m_is_host;

	PrototypeWidget* m_prototype_widget;

	bool IsCellAllowed (AbsBoardPos cell);
};


class JoinShipsController : public IShipController, public IGameBoardShipController
{
public:
	JoinShipsController (GameBoard& src_game_board);
	~JoinShipsController ();

	void OnClickLeft (AbsBoardPos cur_pos) {}
	void OnClickLeft (int prototype_number);
	void OnClickRight (AbsBoardPos cur_pos) {}

	void OnSelectChange (AbsBoardPos cur_pos);

	void OnReleaseLeft ();
	void OnReleaseRight (AbsBoardPos& cur_pos);

	void NotifyPrototypeAdded (const WarShipPrototype* prototype) {}
	void NotifyPrototypeDeleted (const WarShipPrototype* prototype) {}
	void NotifyWarshipCountChanged ();

	// Throws InvalidPrivateParameterStateException
	void BindPrototypeWidget (PrototypeWidget* src_widget)
	{
		if (m_prototype_widget != 0)
		{
			throw InvalidPrivateParameterStateException (L"m_prototype_widget != 0 - prototype widget already binded");
		}

		m_prototype_widget = src_widget;
	}
	// Throws InvalidPrivateParameterStateException
	void UnBindPrototypeWidget (PrototypeWidget* src_widget)
	{
		if (m_prototype_widget != src_widget)
		{
			throw InvalidPrivateParameterStateException (L"m_prototype_widget != src_widget - unbinding unknown widget");
		}

		m_prototype_widget = 0;
	}

	void SetCurrentShipAbility(ShipAbilityType type) {}

	bool IsHost ()
	{
		return m_is_host;
	}

	NewWarshipField* GetNew ();
	EditField* GetEdit ();

private:
	JoinShipsController (const ShipsController&);
	JoinShipsController& operator = (const ShipsController&);

	bool IsCellAllowed (AbsBoardPos cell);

	bool m_is_host;

	NewWarshipFieldPtr m_new;
	GameBoard& m_gameboard;
	PrototypeWidget* m_prototype_widget;
};


//// ControllerException ////////////////////////////////////////////////////////////////////////////////////////////////////


//// Constructor

ControllerException::ControllerException ()
{
}


//// Destructor

ControllerException::~ControllerException ()
{
}


//// NewWarshipField ////////////////////////////////////////////////////////////////////////////////////////////////////


//// Constructor

NewWarshipField::NewWarshipField (AbsBoardPos src_pos, WarShipPrototypePtr src_prototype) : m_pos (src_pos), m_prototype (src_prototype)
{
}


//// Destructor

NewWarshipField::~NewWarshipField ()
{
}


//// EditField ////////////////////////////////////////////////////////////////////////////////////////////////////


//// Constructor

EditField::EditField (AbsBoardPos& create_pos, EditFlag edit_flag) : m_pos (create_pos), m_flag (edit_flag), m_last_added (create_pos)
{
	WarShipPrototypePtr new_prototype (new WarShipPrototype ());

	m_prototype = new_prototype;
}


//// Destructor

EditField::~EditField ()
{
}


//// Get edit_flag

EditFlag EditField::GetFlag ()
{
	return m_flag;
}


//// Add point to edit-prototype

void EditField::AddPoint (AbsBoardPos& point_pos)
{
	m_prototype->AddPoint (RelBoardPos (point_pos.x - m_pos.x, point_pos.y - m_pos.y));
	m_last_added = point_pos;
}


//// Get last added point

const AbsBoardPos& EditField::GetLast ()
{
	return m_last_added;
}


//// Get edit-position

const AbsBoardPos& EditField::GetPos ()
{
	return m_pos;
}


//// Get prototype points

const RelBoardPosList& EditField::GetPoints ()
{
	return m_prototype->GetPoints ();
}


//// Get prototype

WarShipPrototypePtr& EditField::GetPrototype ()
{
	return m_prototype;
}

//// ShipsController ////////////////////////////////////////////////////////////////////////////////////////////////////


//// Constructor

ShipsController::ShipsController (GameBoard& src_game_board) : m_gameboard (src_game_board), m_prototype_widget (0), m_is_host (true)
{
	src_game_board.BindController (this);
}


//// Destructor

ShipsController::~ShipsController ()
{
	m_gameboard.UnBindController (this);
	//TODO: нужен ли ассерт ?
	_ASSERT (m_prototype_widget == 0);
}


//// Create new "edit" on left mouse click

void ShipsController::OnClickLeft (AbsBoardPos cur_pos)
{
	// read about sequence points
	//TODO: что за коммент ?

	// Reserved for completed warship-editing
	//
	//if (m_edit.get ())
	//{
	//	throw CExc_TwoKeysPressed ();
	//}

	if (!m_new.get ())
	{
		m_edit.reset (new EditField (cur_pos, ed_add));
	}
}


////

void ShipsController::OnClickLeft (int prototype_number)
{
	const WarShipPrototypeList list = m_gameboard.GetPrototypeList ();

	if (list.size () < (size_t) prototype_number)
	{
		return;
	}

	WarShipPrototypeList::const_iterator counter = list.begin ();

	for (int pos = 1;pos < prototype_number;pos = pos +1)
	{
		counter ++;
	}

	m_new.reset (new NewWarshipField (AbsBoardPos (0, 0), *counter));
}


//// Create new del-"edit" on right mouse click

void ShipsController::OnClickRight (AbsBoardPos cur_pos)
{
	// Reserved for completed warship-editing
	//
	//if (m_edit.get ())
	//{
	//	throw CExc_TwoKeysPressed ();
	//}

	
}


//// Extend "edit" if exist
// Throws std::exception
void ShipsController::OnSelectChange (AbsBoardPos cur_pos)
{
	if (!m_edit.get () && !m_new.get ())
	{
		THROW_TEXT ("Controller called with unpressed mouse button !");
	}

	if (!m_new.get ())
	{
		m_edit->AddPoint (cur_pos);
	}
	else
	{
		m_new->SetNewPosition (cur_pos);
	}
}


//// Check if cell crossed with exist cell on gameboard

bool ShipsController::IsCellAllowed (AbsBoardPos cell)
{
	for (int x = -1;x <= 1;x = x +1)
	{
		for (int y = -1;y <= 1;y = y +1)
		{
			if (cell.x + x < 0 || cell.x + x >= m_gameboard.GetSize () || cell.y + y < 0 || cell.y + y >= m_gameboard.GetSize ())
			{
				continue;
			}

			if (ct_empty != m_gameboard.Cell (AbsBoardPos (cell.x + x, cell.y + y)).cell_type)
			{
				return false;
			}
		}
	}

	return true;
}


//// Calculate straight neihgbours (side by side)

int CalculateNeihgbours (const RelBoardPosList& edit_points, RelBoardPosList::const_iterator& point)
{
	int return_val = 0;

	for (RelBoardPosList::const_iterator counter = edit_points.begin ();counter != edit_points.end ();counter ++)
	{
		int x_diiference = abs (point->x - counter->x);
		int y_difference = abs (point->y - counter->y);

		if (1 == x_diiference + y_difference)
		{
			return_val = return_val +1;
		}
	}

	return return_val;
}


//// If all right must turn "edit" into warship. Another way kill "edit"

void ShipsController::OnReleaseLeft ()
{
	if (m_new.get ())
	{
		const RelBoardPosList& new_points = m_new->GetPoints ();

		try
		{
			for (RelBoardPosList::const_iterator counter = new_points.begin ();counter != new_points.end ();counter ++)
			{
				if (!IsCellAllowed (m_new->GetPos () + *counter))
				{
					throw CExc_DisallowedShip ();
				}
			}
		}
		catch (CExc_DisallowedShip)
		{
			m_new.reset (0);
			return;
		}

		try
		{
			m_new->GetPrototype ()->LimitUp ();

			m_gameboard.AddWarShip (m_new->GetPrototype (), m_new->GetPos ());
		}
		catch (...)
		{
			m_new->GetPrototype ()->LimitDown ();
		}

		m_new.reset (0);

		return;
	}

	const RelBoardPosList& edit_points = m_edit->GetPoints ();

	int ends = 0;

	try
	{
		if (edit_points.size () > (size_t) m_edit->GetPrototype ()->max_size)
		{
			throw CExc_DisallowedShip ();
		}

		for (RelBoardPosList::const_iterator counter = edit_points.begin ();counter != edit_points.end ();counter ++)
		{
			if (!IsCellAllowed (m_edit->GetPos () + *counter))
			{
				throw CExc_DisallowedShip ();
			}
		
			int cell_neihgbours = CalculateNeihgbours (edit_points, counter);

			if (1 == cell_neihgbours)
			{
				ends = ends +1;
			}

			if (cell_neihgbours > 2 || (cell_neihgbours == 0 && edit_points.size () != 1))
			{
				throw CExc_DisallowedShip ();
			}
		}

		if (ends != 2 && edit_points.size () != 1)
		{
			throw CExc_DisallowedShip ();
		}
	}
	catch (CExc_DisallowedShip)
	{
		m_edit.reset (0);
		return;
	}

	RelBoardPos warship_offset (0, 0);

	WarShipPrototypePtr board_prototype = m_gameboard.AddPrototype (m_edit->GetPrototype (), &warship_offset);

	try
	{
		board_prototype->LimitUp ();

		m_gameboard.AddWarShip (board_prototype, m_edit->GetPos () + warship_offset);
	}
	catch (...)
	{
		board_prototype->LimitDown ();
	}

	m_edit.reset (0);
}


//// 

void ShipsController::OnReleaseRight (AbsBoardPos& cur_pos)
{
	if (ct_ship == m_gameboard.Cell (cur_pos).cell_type)
	{
		WarShipPrototypePtr dead_warship_prototype = m_gameboard.Cell (cur_pos).ship->GetPrototype ();

		m_gameboard.DelWarShip (cur_pos);

		dead_warship_prototype->LimitDown ();
	}
}


////

void ShipsController::NotifyPrototypeAdded (const WarShipPrototype* prototype)
{
	if (m_prototype_widget)
	{
		m_prototype_widget->NotifyPrototypeAdded (prototype);
	}
}


////

void ShipsController::NotifyPrototypeDeleted (const WarShipPrototype* prototype)
{
	if (m_prototype_widget)
	{
		m_prototype_widget->NotifyPrototypeDeleted (prototype);
	}
}


////

void ShipsController::NotifyWarshipCountChanged ()
{
	if (m_prototype_widget)
	{
		m_prototype_widget->NotifyWarshipCountChanged ();
	}
}


//// Get edit-field

EditField* ShipsController::GetEdit ()
{
	return m_edit.get ();
}


//// 

NewWarshipField* ShipsController::GetNew ()
{
	return m_new.get ();
}


//// JoinShipsController ////////////////////////////////////////////////////////////////////////////////////////////////////


//// Constructor

JoinShipsController::JoinShipsController (GameBoard& src_game_board) : m_gameboard (src_game_board), m_prototype_widget (0), m_is_host (false)
{
	src_game_board.BindController (this);
}


//// Destructor

JoinShipsController::~JoinShipsController ()
{
	m_gameboard.UnBindController (this);

	// Здесь можно, т.к. кидать исключения из деструктора плохо
	// TODO: но нужен ли тут ассерт ?
	_ASSERT (m_prototype_widget == 0);
}


//// 

void JoinShipsController::OnClickLeft (int prototype_number)
{
	const WarShipPrototypeList list = m_gameboard.GetPrototypeList ();

	if (list.size () < (size_t) prototype_number)
	{
		return;
	}

	WarShipPrototypeList::const_iterator counter = list.begin ();

	for (int pos = 1;pos < prototype_number;pos = pos +1)
	{
		counter ++;
	}

	m_new.reset (new NewWarshipField (AbsBoardPos (0, 0), *counter));
}


////

void JoinShipsController::OnSelectChange (AbsBoardPos cur_pos)
{
	if (m_new.get ())
	{
		m_new->SetNewPosition (cur_pos);
	}
}


////

void JoinShipsController::OnReleaseLeft ()
{
	if (m_new.get ())
	{
		const RelBoardPosList& new_points = m_new->GetPoints ();

		try
		{
			for (RelBoardPosList::const_iterator counter = new_points.begin ();counter != new_points.end ();counter ++)
			{
				if (!IsCellAllowed (m_new->GetPos () + *counter))
				{
					throw CExc_DisallowedShip ();
				}
			}
		}
		catch (CExc_DisallowedShip)
		{
			m_new.reset (0);
			return;
		}

		try
		{
			m_gameboard.AddWarShip (m_new->GetPrototype (), m_new->GetPos ());
		}
		catch (...)
		{
		}

		m_new.reset (0);
	}

}


////

bool JoinShipsController::IsCellAllowed (AbsBoardPos cell)
{
	for (int x = -1;x <= 1;x = x +1)
	{
		for (int y = -1;y <= 1;y = y +1)
		{
			if (cell.x + x < 0 || cell.x + x >= m_gameboard.GetSize () || cell.y + y < 0 || cell.y + y >= m_gameboard.GetSize ())
			{
				continue;
			}

			if (ct_empty != m_gameboard.Cell (AbsBoardPos (cell.x + x, cell.y + y)).cell_type)
			{
				return false;
			}
		}
	}

	return true;
}


////

void JoinShipsController::OnReleaseRight (AbsBoardPos& cur_pos)
{
	if (ct_ship == m_gameboard.Cell (cur_pos).cell_type)
	{
		m_gameboard.DelWarShip (cur_pos);
	}
}


////

void JoinShipsController::NotifyWarshipCountChanged ()
{
	if (m_prototype_widget)
	{
		m_prototype_widget->NotifyWarshipCountChanged ();
	}
}


////

NewWarshipField* JoinShipsController::GetNew ()
{
	return m_new.get ();
}


////

EditField* JoinShipsController::GetEdit ()
{
	return 0;
}


//// FictitiuosShipsController //////////////////////////////////////////////////////////////////////////////////////////////

class FictitiuosShipsController : public IShipController, public IGameBoardShipController
{
public:
	FictitiuosShipsController (GameBoard& src_game_board);
	~FictitiuosShipsController ();

	void OnClickLeft (AbsBoardPos cur_pos) {}
	void OnClickLeft (int prototype_number) {}
	void OnClickRight (AbsBoardPos cur_pos) {}

	void OnSelectChange (AbsBoardPos cur_pos) {}

	void OnReleaseLeft () {}
	void OnReleaseRight (AbsBoardPos& cur_pos) {}

	void NotifyPrototypeAdded (const WarShipPrototype* prototype) {}
	void NotifyPrototypeDeleted (const WarShipPrototype* prototype) {}
	void NotifyWarshipCountChanged () {}

	void BindPrototypeWidget (PrototypeWidget* src_widget) {}
	void UnBindPrototypeWidget (PrototypeWidget* src_widget) {}

	void SetCurrentShipAbility(ShipAbilityType type) {}

	bool IsHost ()
	{
		return false;
	}

	EditField* GetEdit () { return 0; }
	NewWarshipField* GetNew () { return 0; }

private:
	FictitiuosShipsController (const FictitiuosShipsController&);
	FictitiuosShipsController& operator = (const FictitiuosShipsController&);

	GameBoard& m_gameboard;
};

FictitiuosShipsController::FictitiuosShipsController (GameBoard& src_game_board) : m_gameboard (src_game_board)
{
	src_game_board.BindController (this);
}

FictitiuosShipsController::~FictitiuosShipsController ()
{
	m_gameboard.UnBindController (this);
}


//// BattleShipsController //////////////////////////////////////////////////////////////////////////////////////////////////

class BattleShipsController : public IShipController, public IGameBoardShipController
{
public:
	BattleShipsController (GameBoard& src_game_board, IGameControllerSide* game_engine);
	~BattleShipsController ();

	void OnClickLeft (AbsBoardPos cur_pos);
	void OnClickLeft (int prototype_number) {}
	void OnClickRight (AbsBoardPos cur_pos) {}

	void OnSelectChange (AbsBoardPos cur_pos) {}

	void OnReleaseLeft ();
	void OnReleaseRight (AbsBoardPos& cur_pos) {}

	//Throws InvalidFunctionCallException
	void NotifyPrototypeAdded (const WarShipPrototype* prototype)
	{
		throw InvalidFunctionCallException (L"NotifyPrototypeAdded in BattleShipsController");
	}
	//Throws InvalidFunctionCallException
	void NotifyPrototypeDeleted (const WarShipPrototype* prototype)
	{
		throw InvalidFunctionCallException (L"NotifyPrototypeDeleted in BattleShipsController");
	}
	//Throws InvalidFunctionCallException
	void NotifyWarshipCountChanged ()
	{
		throw InvalidFunctionCallException (L"NotifyWarshipCountChanged in BattleShipsController");
	}
	//Throws InvalidFunctionCallException
	void BindPrototypeWidget (PrototypeWidget* src_widget)
	{
		throw InvalidFunctionCallException (L"BindPrototypeWidget in BattleShipsController");
	}
	//Throws InvalidFunctionCallException
	void UnBindPrototypeWidget (PrototypeWidget* src_widget)
	{
		throw InvalidFunctionCallException (L"UnBindPrototypeWidget in BattleShipsController");
	}

	void SetCurrentShipAbility(ShipAbilityType type);

	bool IsHost ()
	{
		return false;
	}

	EditField* GetEdit () { return 0; }
	NewWarshipField* GetNew () { return 0; }

private:
	BattleShipsController (const BattleShipsController&);
	BattleShipsController& operator = (const BattleShipsController&);

	IGameControllerSide* m_game_engine;

	GameBoard& m_gameboard;
	ShipAbilityType m_current_ship_ability;
};

BattleShipsController::BattleShipsController(GameBoard& src_game_board, IGameControllerSide* game_engine) :
	m_gameboard(src_game_board),
	m_game_engine(game_engine),
	m_current_ship_ability(sa_last)
{
	src_game_board.BindController (this);
}

BattleShipsController::~BattleShipsController ()
{
	m_gameboard.UnBindController (this);
}

void BattleShipsController::OnClickLeft(AbsBoardPos cur_pos)
{
	switch (m_current_ship_ability)
	{
	case sa_shot:
	{
		m_game_engine->IUseAbility(std::make_shared<ShotShipAbility>(cur_pos));
		break;
	}
	case sa_move:
		break;
	case sa_torpedo:
		break;
	case sa_rocket:
		break;
	case sa_maac:
		break;
	case sa_illusion:
		break;
	case sa_emp:
		break;
	case sa_scouting:
		break;
	case sa_fighter_drone:
		break;
	case sa_interceptor_drone:
		break;
	case sa_prospector_drone:
		break;
	case sa_prospector_drone_update:
		break;
	case sa_carpet_bombing:
		break;
	case sa_barrage_fire:
		break;
	case sa_full_combat_readiness:
		break;
	case sa_last:
		break;
	default:
		break;
	}
}

void BattleShipsController::OnReleaseLeft ()
{
	// FIXME: Move mouse-click answer to here
}

void BattleShipsController::SetCurrentShipAbility(ShipAbilityType type)
{
	m_current_ship_ability = type;
}

std::unique_ptr<IShipController> CreateHostShipsController (GameBoard& game_board)
{
	return std::make_unique<ShipsController>(game_board);
}

std::unique_ptr<IShipController> CreateJoinShipsController (GameBoard& game_board)
{
	return std::make_unique<JoinShipsController>(game_board);
}

std::unique_ptr<IShipController> CreateFictitiousShipsController (GameBoard& game_board)
{
	return std::make_unique<FictitiuosShipsController>(game_board);
}

std::unique_ptr<IShipController> CreateBattleShipsController (GameBoard& game_board, IGameControllerSide* game_engine)
{
	return std::make_unique<BattleShipsController>(game_board, game_engine);
}
