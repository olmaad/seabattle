#pragma once

#include "seawindow.h"
#include "GameBoardUI.h"
#include "UI_Connect.h"

#include <libGameEngine/GameBoard.h>
#include <libGameEngine/GameEngine.h>
#include <libSystem/Printer.h>

#include <memory>

class BasicGameWindow : public AbstractTopLvlWindow, public IGameControllerNotify, public IControllerNetNotify
{
public:
	BasicGameWindow();
	~BasicGameWindow();

	static const LPCTSTR lpTemplateName;

	enum
	{
		control_spacing = 10,
		group_box_height = 50,
		status_box_height = 20
	};

	//// IGameControllerNotify
	void UpdGameState(GameState state, const std::wstring& reason) override;
	void UpdEnemyGB() override;
	void UpdMyGB() override;
	void NotifyGameStarted() override {}

	//// IControllerNetNotify
	void PlayerConnected() override {}
	void GameProcessEnded() override;

protected:
	virtual void DoUpdGameState(GameState state, const std::wstring& reason);
	void DoUpdEnemyGB();
	void DoUpdMyGB();
	void DoUpdCurrentSelectedAbility(ShipAbilityType type);

	AbstractTopLvlWindow* m_parent;

	WaitingWindow* m_wait_window;

	std::shared_ptr<GameBoard> m_enemy_gameboard;
	std::shared_ptr<IGameEngineManage> m_enemy_game_engine_manage;
	IGameEngineSide* m_enemy_game_engine_side;
	std::unique_ptr<IShipController> m_enemy_controller;
	std::unique_ptr<GameBoardWidget> m_enemy_gbwidget;

	std::shared_ptr<IGameEngineManage> m_game_engine_manage;
	std::shared_ptr<GameBoard> m_my_gameboard;
	IGameControllerSide* m_game_controller_side;
	IGameEngineSide* m_game_engine_side;
	std::unique_ptr<IShipController> m_my_controller;
	std::unique_ptr<GameBoardWidget> m_my_gbwidget;

	std::shared_ptr<Printer> m_log_printer;

	bool m_am_first;

	void EnableCancel() { m_is_cancel_locked = false; }
	void DisableCancel() { m_is_cancel_locked = true; }
	bool m_is_cancel_locked;

	void CreateVisuals(HINSTANCE hInstance, AbstractTopLvlWindow* parent, std::wstring suffix);
	bool Create(HINSTANCE hInstance, AbstractTopLvlWindow* parent);

	void SetParameters(std::shared_ptr<GameBoard> my_gameboard, bool am_first);

	LRESULT OnCommand(WPARAM wParam, LPARAM lParam);
	LRESULT OnDestroy(HWND hWnd);
	LRESULT OnOtherMesage(UINT message, WPARAM wParam, LPARAM lParam);
};

class ServerGameWindow : public BasicGameWindow
{
public:
	bool Create(HINSTANCE hInstance, AbstractTopLvlWindow* parent, std::shared_ptr<GameBoard> my_gameboard, bool am_first, DWORD server_address, int server_port, std::wstring alias);
	void NotifyGameStarted();

	void PlayerConnected();

};

class ClientGameWindow : public BasicGameWindow
{
public:
	bool Create(HINSTANCE hInstance, AbstractTopLvlWindow* parent, DWORD server_address, int server_port, std::wstring alias);

	void AcceptGameBoard(std::shared_ptr<GameBoard> gb);

protected:
	void DoUpdGameState(GameState state, const std::wstring& reason);

	LRESULT OnOtherMesage(UINT message, WPARAM wParam, LPARAM lParam);
};

class EmulatorGameWindow : public BasicGameWindow
{
public:
	bool Create(HINSTANCE hInstance, AbstractTopLvlWindow* parent, std::shared_ptr<GameBoard> my_gameboard, bool am_first, bool am_server, EmulatorDifficulty difficulty);

};
