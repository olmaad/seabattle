#include "stdafx.h"

#include "UI_NewGame.h"

#include "UI_PlaceShips.h"
#include "UI_GameWindow.h"
#include "resources/resource.h"

#include <libNetwork/NetworkProtocol.h>
#include <libGameEngine/GameBoard.h>

#include <string>

#pragma warning (disable: 4996)

const LPCTSTR NewGameWindow::lpTemplateName = MAKEINTRESOURCE(IDD_CREATEGAME);


NewGameWindow::NewGameWindow () : m_parent (0)
{
}

NewGameWindow::~NewGameWindow ()
{
	if (m_parent)
	{
		m_parent->Show (SW_SHOWNORMAL);
	}
}

LRESULT NewGameWindow::OnDestroy (HWND hWnd)
{
	delete this;

	return 0;
}

bool NewGameWindow::Create (HINSTANCE hInstance, AbstractTopLvlWindow* parent)
{
	HWND parent_handle = 0;

	if (0 != parent)
	{
		parent_handle = parent->GetHandle ();
	}
	else
	{
		return false;
	}

	if (!AbstractTopLvlWindow::Create (hInstance, lpTemplateName, parent_handle))
	{
		return false;
	}

	m_parent = parent;

	SendMessage (GetDlgItem (IDC_GAMENAME), WM_SETTEXT, 0, (LPARAM) L"Obi One");
	SendMessage (GetDlgItem (IDC_SERVERADRESS), WM_SETTEXT, 0, (LPARAM) L"127.0.0.1");
	SendMessage (GetDlgItem (IDC_SERVERPORT), WM_SETTEXT, 0, (LPARAM) L"25655");
	SendMessage (GetDlgItem (IDC_BOARDSIZE), WM_SETTEXT, 0, (LPARAM) L"16");
	SendMessage (GetDlgItem (IDC_FIRSTMOVE_ME), BM_SETCHECK, BST_CHECKED, 0);

	return true;
}


LRESULT NewGameWindow::OnCommand (WPARAM wParam, LPARAM lParam)
{
	switch (LOWORD(wParam))
	{
	case IDCANCEL:
		Destroy ();
		return 0;
	case IDC_CREATENEXT:
		{
			try
			{
				std::wstring game_name = GetEditboxText(IDC_GAMENAME);

				if (1 > game_name.length () || npl_max_alias_length <= game_name.length ())
				{
					SendMessage (GetDlgItem (IDC_GAMENAME_FEEDBACK), WM_SETTEXT, 0, (LPARAM) L"Enter better game (nick) name");

					return 0;
				}
				else
				{
					SendMessage (GetDlgItem (IDC_GAMENAME_FEEDBACK), WM_SETTEXT, 0, (LPARAM) L"");
				}

				DWORD server_adress = EnterIp4Address (IDC_SERVERADRESS, IDC_SERVERADRESS_FEEDBACK);

				int board_size = EnterInteger (IDC_BOARDSIZE, IDC_BOARDSIZE_FEEDBACK, GameBoard::min_size, GameBoard::max_size);
				int port_number = EnterInteger (IDC_SERVERPORT, IDC_SERVERPORT_FEEDBACK, 1, 65535);
			
				bool am_first = (SendMessage (GetDlgItem (IDC_FIRSTMOVE_ME), BM_GETCHECK, 0, 0) == BST_CHECKED);

				PlaceShipsWindow* new_game = new PlaceShipsWindow();

				if (!new_game->Create (hInst, this, board_size, true, WarShipPrototypeList ()))
				{
					delete new_game;
					return 0;
				}

				m_server_adress = server_adress;
				m_port_number = port_number;
				m_game_name = game_name;
				m_am_first = am_first;

				Show (SW_HIDE);
				new_game->Show (SW_SHOWNORMAL);
			}
			catch (...)
			{
				TRACE_TEXT ("Exception caught !");
			}

			return 0;
		}
	default:
		return AbstractTopLvlWindow::OnCommand (wParam, lParam);
	}
}

LRESULT NewGameWindow::OnOtherMesage (UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_SEAWIZARD_STEP_COMPLETE:
		{
			if (!m_my_gameboard.get () || !wParam)
			{
				Show (SW_SHOWNORMAL);
				return 0;
			}

			ServerGameWindow* game_window = new ServerGameWindow ();

			if (!game_window->Create (hInst, this, m_my_gameboard, m_am_first, m_server_adress, m_port_number, m_game_name))
			{
				game_window->Destroy ();

				return 0;
			}

			Show (SW_HIDE);
			//game_window->Show (SW_SHOWNORMAL);

			return 0;
		}
	default:
		return AbstractTopLvlWindow::OnOtherMesage (message, wParam, lParam);
	}
}
