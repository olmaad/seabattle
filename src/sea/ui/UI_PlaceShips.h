#pragma once

#include "seawindow.h"
#include "GameBoardUI.h"

#include <libGameEngine/GameBoard.h>

#include <memory>

class PlaceShipsWindow : public AbstractTopLvlWindow
{
public:
	PlaceShipsWindow ();
	~PlaceShipsWindow ();
	bool Create (HINSTANCE hInstance, AbstractTopLvlWindow* parent, int board_size, bool is_host, WarShipPrototypeList& host_prototype_list);

	enum
	{
		control_spacing = 10,
		prototype_widget_width = 150
	};

	static const LPCTSTR lpTemplateName;
private:
	AbstractTopLvlWindow* m_parent;

	//Яверы придумали сборщик мусора... У нас есть smart pointer !

	bool m_accepted;

	std::shared_ptr<GameBoard> m_gameboard;
	std::unique_ptr<IShipController> m_controller;
	std::unique_ptr<GameBoardWidget> m_gb_widget;
	std::unique_ptr<PrototypeWidget> m_prototype_widget;

protected:
	virtual LRESULT OnCommand (WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnDestroy (HWND hWnd);
};

class JoinPlaceShipsWindow : public PlaceShipsWindow
{
};
