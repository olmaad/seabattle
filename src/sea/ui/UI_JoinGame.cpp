#include "stdafx.h"
#include "UI_JoinGame.h"
#include "UI_PlaceShips.h"
#include "UI_GameWindow.h"
#include "resources/resource.h"



const LPCTSTR JoinGameWindow::lpTemplateName = MAKEINTRESOURCE(IDD_JOINGAME);


//// JoinGameWindow ////////////////////////////////////////////////////////////////////////////////////////////////////


//// Constructor

JoinGameWindow::JoinGameWindow () : m_parent (0)
{
}


//// Destructor

JoinGameWindow::~JoinGameWindow ()
{
	if (m_parent)
	{
		m_parent->Show (SW_SHOWNORMAL);
	}
}


////

bool JoinGameWindow::Create (HINSTANCE hInstance, AbstractTopLvlWindow* parent)
{
	HWND parent_handle = 0;

	if (0 != parent)
	{
		parent_handle = parent->GetHandle ();
	}
	else
	{
		return false;
	}

	if (!AbstractTopLvlWindow::Create (hInstance, lpTemplateName, parent_handle))
	{
		return false;
	}

	m_parent = parent;

	SendMessage (GetDlgItem (IDC_GAMENAME), WM_SETTEXT, 0, (LPARAM) L"Han Solo");
	SendMessage (GetDlgItem (IDC_SERVERADRESS), WM_SETTEXT, 0, (LPARAM) L"127.0.0.1");
	SendMessage (GetDlgItem (IDC_SERVERPORT), WM_SETTEXT, 0, (LPARAM) L"25655");

	return true;
}


////

LRESULT JoinGameWindow::OnCommand (WPARAM wParam, LPARAM lParam)
{
	switch (LOWORD(wParam))
	{
	case IDCANCEL:
		Destroy ();
		return 0;
	case IDC_JOINNEXT:
		{
			std::wstring game_name = GetEditboxText(IDC_GAMENAME);

			if (1 > game_name.length () || 256 < game_name.length ())
			{
				SendMessage (GetDlgItem (IDC_GAMENAME_FEEDBACK), WM_SETTEXT, 0, (LPARAM) L"Enter better game (nick) name");

				return 0;
			}
			else
			{
				SendMessage (GetDlgItem (IDC_GAMENAME_FEEDBACK), WM_SETTEXT, 0, (LPARAM) L"");
			}

			DWORD server_adress = EnterIp4Address (IDC_SERVERADRESS, IDC_SERVERADRESS_FEEDBACK);

			int port_number = EnterInteger (IDC_SERVERPORT, IDC_SERVERPORT_FEEDBACK, 1, 65535);

			m_server_adress = server_adress;
			m_port_number = port_number;
			m_game_name = game_name;

			//TODO: ловить исключения
			ClientGameWindow* game_window = new ClientGameWindow ();

			if (!game_window->Create (hInst, this, m_server_adress, m_port_number, m_game_name))
			{
				delete game_window;

				return 0;
			}

			Show (SW_HIDE);
			
			return 0;
		}
	default:
		return AbstractTopLvlWindow::OnCommand (wParam, lParam);
	}
}


////

LRESULT JoinGameWindow::OnDestroy (HWND hWnd)
{
	delete this;

	return 0;
}


////
