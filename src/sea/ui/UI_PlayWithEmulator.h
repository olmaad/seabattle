#pragma once

#include "seawindow.h"

#include <libGameEngine/GameBoard.h>
#include <libGameEngine/GameEngine.h>

class PlayWithEmulatorWindow : public AbstractTopLvlWindow
{
public:
	PlayWithEmulatorWindow ();
	~PlayWithEmulatorWindow ();

	static const LPCTSTR lpTemplateName;

	void AcceptGameBoard(std::shared_ptr<GameBoard> gb)
	{
		m_my_gameboard = gb;
	}

	bool Create (HINSTANCE hInstance, AbstractTopLvlWindow* parent);

private:
	AbstractTopLvlWindow* m_parent;
	std::shared_ptr<GameBoard> m_my_gameboard;

	bool m_is_first;
	EmulatorDifficulty m_is_hard;

protected:
	virtual LRESULT OnCommand (WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnDestroy (HWND hWnd);
	virtual LRESULT OnOtherMesage (UINT message, WPARAM wParam, LPARAM lParam);

};
