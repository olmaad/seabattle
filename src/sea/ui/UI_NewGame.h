#pragma once

#include "seawindow.h"

#include <libGameEngine/GameBoard.h>

class NewGameWindow : public AbstractTopLvlWindow		//Окно создания новой игры
{
public:
	NewGameWindow ();		//m_parent = 0
	~NewGameWindow ();		//Показывает m_parent-a
	bool Create (HINSTANCE hInstance, AbstractTopLvlWindow* parent);		//Создание окна (через AbstractTopLvlWindow). Проверка на существование родителя.

	void AcceptGameBoard(std::shared_ptr<GameBoard> gb)
	{
		m_my_gameboard = gb;
	}

	static const LPCTSTR lpTemplateName;
private:
	AbstractTopLvlWindow* m_parent;		//Указатель на родительское окно

	std::shared_ptr<GameBoard> m_my_gameboard;

	DWORD m_server_adress;
	int m_port_number;
	std::wstring m_game_name;
	bool m_am_first;
protected:
	virtual LRESULT OnCommand (WPARAM wParam, LPARAM lParam);		//Обработка WM_COMMAND
	virtual LRESULT OnDestroy (HWND hWnd);		//Удаление объекта
	virtual LRESULT OnOtherMesage (UINT message, WPARAM wParam, LPARAM lParam);
};
