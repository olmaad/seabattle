#pragma once
#include <map>
#include "seawindow.h"
#include "UI_ShipsController.h"




class GameBoard;

//test

class GameBoardWidget : public AbstractChildWindow
{
public:
	GameBoardWidget (HINSTANCE hInstance, AbstractTopLvlWindow& parent, GameBoard &game_board, IShipController& ships_controller);
	void Create (int x, int y, HINSTANCE hInstance);

	SIZE CalculateSize () const;
	SIZE GetCellSize () const;

	int LeftBorder () const;
	int TopBorder () const;

	void Lock () {m_is_locked = true; m_is_invisible_locked = false;}
	void Unlock () {m_is_locked = false; m_is_invisible_locked = false;}

	void InvisibleLock () {m_is_invisible_locked = true; m_is_locked = false;}
	void InvisibleUnlock () {m_is_invisible_locked = false; m_is_locked = false;}

	LRESULT OnLeftMouseDown (LPARAM lParam);
	LRESULT OnLeftMouseUp ();

	LRESULT OnRightMouseDown (LPARAM lParam);
	LRESULT OnRightMouseUp (LPARAM lParam);

	LRESULT OnMouseMove (LPARAM lParam);
	
	LRESULT OnTimer ();

private:
	LRESULT OnPaint ();
	void PaintChecks (HDC hdc, RECT ClientRect);
	void PaintDecorations (HDC hdc, RECT ClientRect);
	void PaintEdit (HDC hdc, RECT ClientRect);
	void PaintNew (HDC hdc, RECT ClientRect);
	void PaintLock (HDC hdc, RECT ClientRect);

	void InvalidateRect (AbsBoardPos mouse_pos);

	void OnLeftClick (int x, int y);

	bool IsCurPosAllowed (const AbsBoardPos& cur_pos);
	AbsBoardPos GetMouseBoardPosition (int x, int y);

	IShipController& m_ships_controller;

	bool m_is_locked;
	bool m_is_invisible_locked;
	double m_lock_angle;
	RECT m_lock_rect;

	enum
	{
		cell_size = 25
	};

	std::unique_ptr<ColorBitmap> m_screen;

	GameBoard &m_game_board;
	VectorFont m_border_font;
};


class PrototypeWidget : public AbstractChildWindow
{
public:
	PrototypeWidget (HINSTANCE hInstance, AbstractTopLvlWindow& parent, GameBoard &game_board, IShipController& src_ships_controller);
	~PrototypeWidget ();

	void Create (int x, int y, int height, int width, HINSTANCE hInstance);

	void NotifyPrototypeAdded (const WarShipPrototype* prototype);
	void NotifyPrototypeDeleted (const WarShipPrototype* prototype);
	void NotifyWarshipCountChanged ();

	LRESULT OnMouseMove (LPARAM lParam);
	
	LRESULT OnLeftMouseDown (LPARAM lParam);

	LRESULT OnOtherMesage (UINT message, WPARAM wParam, LPARAM lParam);

private:
	GameBoard& m_game_board;
	IShipController& m_ships_controller;

	typedef std::map<const WarShipPrototype*, std::unique_ptr<ColorBitmap>> TIconMap;
	TIconMap m_prototype_icons_map;

	int m_icon_amount;
	int m_mouse_select;
	int m_current_page;
	int m_prototypes_amount;
	SIZE m_icon_size;

	bool m_mouse_captured;

	LRESULT OnPaint();
	void PaintControls(HDC hdc, RECT ClienRect);
	void RenderIcon(HDC dc, SIZE size, POINT offset, const WarShipPrototype* prototype, bool selected);

	void InvalidateRect(RECT update_rect);

	int GetMousePosition(POINT cur_pos, RECT& client_rect);
	int CalculatePageAmount();

	std::unique_ptr<ColorBitmap> m_screen;

	TRACKMOUSEEVENT m_track_mouse_event;

	enum
	{
		control_width = 25,
		control_footer_height = 30,
		control_spacing = 10,
		cell_size = 10
	};
	enum
	{
		mp_left_list = -2,
		mp_right_list
	};
};
