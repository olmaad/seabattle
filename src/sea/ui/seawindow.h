#pragma once

#include <Windows.h>
#include <string>



class AbstractTopLvlWindow
{
public:
	AbstractTopLvlWindow ();

	bool Show (int nCmdShow);
	void Destroy ();
	bool Create (HINSTANCE hInstance, const LPCTSTR lpTemplateName, HWND hWndParent = 0);
	HWND GetHandle () const;
	HWND GetDlgItem (int item_id) const;
protected:
	HWND m_handle;
	
	static INT_PTR CALLBACK DlgProc (HWND, UINT, WPARAM, LPARAM);

	virtual ~AbstractTopLvlWindow ();

	virtual LRESULT OnCreate ();
	virtual LRESULT OnDestroy (HWND hWnd);
	virtual LRESULT OnPaint ();
	virtual LRESULT OnCommand (WPARAM wParam, LPARAM lParam);

	virtual LRESULT OnOtherMesage (UINT message, WPARAM wParam, LPARAM lParam);

	int EnterInteger (UINT box_id, UINT feedback_id, int low_limit, int max_limit);
	DWORD EnterIp4Address (UINT box_id, UINT feedback_id);

	std::wstring GetEditboxText (UINT box_id);
	int GetEditboxInteger (UINT box_id);
};



class AbstractChildWindow
{
public:
	AbstractChildWindow (HINSTANCE hInstance, HWND parent_handle);
	virtual ~AbstractChildWindow ();

	void ForceUpdate ();

	void Destroy ();
	HWND GetHandle () const;
	RECT GetRect () const;
protected:
	HWND m_handle;
	HWND m_parent_handle;

	static LRESULT CALLBACK WndProc (HWND, UINT, WPARAM, LPARAM);

	void Create (DWORD dwExStyle, DWORD dwStyle, LPCTSTR lpWindowName, int x, int y, int nWidth, int nHeight, HINSTANCE hInstance);
	virtual LRESULT OnCreate ();
	virtual LRESULT OnDestroy (HWND hWnd);
	virtual LRESULT OnPaint ();
	virtual LRESULT OnCommand (WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnLeftMouseDown (LPARAM lParam);
	virtual LRESULT OnLeftMouseUp ();
	
	virtual LRESULT OnOtherMesage (UINT message, WPARAM wParam, LPARAM lParam);

	virtual LRESULT OnRightMouseDown (LPARAM lParam);
	virtual LRESULT OnRightMouseUp (LPARAM lParam);

	virtual LRESULT OnMouseMove (LPARAM lParam);
	virtual LRESULT OnTimer ();

	static void RegisterClass(HINSTANCE hInstance);

	static const wchar_t* class_name;
	static int class_init_counter;
	static ATOM class_atom;
};



class SolidBrush
{
public:
	SolidBrush (const COLORREF& colors);
	~SolidBrush ();

	operator HBRUSH () const
	{
		return m_brush;
	}
private:
	HBRUSH m_brush;

	SolidBrush ();
	SolidBrush (const SolidBrush&);
};



class ColorPen
{
public:
	ColorPen (const COLORREF& colors, int width);
	~ColorPen ();

	operator HPEN () const
	{
		return m_pen;
	}

private:
	HPEN m_pen;

	ColorPen ();
	ColorPen (const ColorPen&);
};



class VectorFont
{
public:
	VectorFont ();
	void Create (int f_height, int f_weight, DWORD pitch_and_family, const wchar_t* face_name);
	~VectorFont ();

	operator HFONT () const
	{
		return m_font;
	}
private:
	VectorFont (const VectorFont&);
	VectorFont& operator = (const VectorFont&);

	HFONT m_font;
};


class ColorBitmap
{
public:
	ColorBitmap (SIZE size);
	ColorBitmap (HINSTANCE hInstance, LPCTSTR res_name);
	~ColorBitmap ();

	operator HBITMAP ()
	{
		return m_bitmap;
	}

	SIZE GetSize () const
	{
		return m_size;
	}

private:
	SIZE m_size;
	HBITMAP m_bitmap;

	ColorBitmap ();
	ColorBitmap (const ColorBitmap&);

	ColorBitmap& operator = (const ColorBitmap&);
};


class DrawSurface
{
public:
	DrawSurface ();
	~DrawSurface ();

	operator HDC ()
	{
		return m_hdc;
	}

private:
	HDC m_hdc;

	DrawSurface (const DrawSurface&);

};
