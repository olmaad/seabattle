#pragma once

#include "seawindow.h"

#include <libGameEngine/GameBoard.h>

#include <memory>

class JoinGameWindow : public AbstractTopLvlWindow
{
public:
	JoinGameWindow ();
	~JoinGameWindow ();

	bool Create (HINSTANCE hInstance, AbstractTopLvlWindow* parent);

	virtual LRESULT OnCommand (WPARAM wParam, LPARAM lParam);
	virtual LRESULT OnDestroy (HWND hWnd);

	static const LPCTSTR lpTemplateName;

private:
	AbstractTopLvlWindow* m_parent;

	DWORD m_server_adress;
	int m_port_number;
	std::wstring m_game_name;
};
