#include "stdafx.h"
#include "UI_PlayWithEmulator.h"
#include "UI_PlaceShips.h"
#include "UI_GameWindow.h"
#include "resources/resource.h"


const LPCTSTR PlayWithEmulatorWindow::lpTemplateName = MAKEINTRESOURCE(IDD_PLAYEMULATOR);


PlayWithEmulatorWindow::PlayWithEmulatorWindow () : m_parent (0)
{
}


PlayWithEmulatorWindow::~PlayWithEmulatorWindow ()
{
	if (m_parent)
	{
		m_parent->Show (SW_SHOWNORMAL);
	}
}


LRESULT PlayWithEmulatorWindow::OnDestroy (HWND hWnd)
{
	delete this;

	return 0;
}


bool PlayWithEmulatorWindow::Create (HINSTANCE hInstance, AbstractTopLvlWindow* parent)
{
	HWND parent_handle = 0;

	if (0 != parent)
	{
		parent_handle = parent->GetHandle ();
	}
	else
	{
		return false;
	}

	if (AbstractTopLvlWindow::Create (hInstance, lpTemplateName, parent_handle))
	{
		m_parent = parent;

		::SendMessage (GetDlgItem (IDC_DEXPERT), BM_SETCHECK, BST_CHECKED, 0);
		::SendMessage (GetDlgItem (IDC_FTME), BM_SETCHECK, BST_CHECKED, 0);

		::SendMessage (GetDlgItem (IDC_BOARDSIZE), WM_SETTEXT, 0, (WPARAM) L"16");

		return true;
	}

	return false;
}


LRESULT PlayWithEmulatorWindow::OnCommand (WPARAM wParam, LPARAM lParam)
{
	switch (LOWORD(wParam))
	{
	case IDCANCEL:
		Destroy ();
		return 0;
	case IDC_EMULATORNEXT:
		{
			std::wstring board_size_text = GetEditboxText(IDC_BOARDSIZE);

			m_is_first = (BST_CHECKED == ::SendMessage (GetDlgItem (IDC_FTME), BM_GETSTATE, 0, 0));
			m_is_hard = (BST_CHECKED == ::SendMessage (GetDlgItem (IDC_DEXPERT), BM_GETSTATE, 0, 0)) ? ed_expert : ed_noob;

			int board_size;
			if (!swscanf (board_size_text.c_str (), L"%d", &board_size) || (board_size < GameBoard::min_size) || (board_size > GameBoard::max_size))
			{
				wchar_t feedback [50];

				_snwprintf (feedback, sizeof(feedback)/sizeof(wchar_t), L"Enter number from %d to %d", GameBoard::min_size, GameBoard::max_size);

				SendMessage (GetDlgItem (IDC_BOARDSIZE_FEEDBACK), WM_SETTEXT, 0, (LPARAM) feedback);

				return 0;
			}
			else
			{
				SendMessage (GetDlgItem (IDC_BOARDSIZE_FEEDBACK), WM_SETTEXT, 0, (LPARAM) L"");
			}

			PlaceShipsWindow* new_game = new PlaceShipsWindow();
			if (!new_game->Create (hInst, this, board_size, true, WarShipPrototypeList ()))
			{
				delete new_game;
				return 0;
			}
			Show (SW_HIDE);
			new_game->Show (SW_SHOWNORMAL);

			return 0;
		}
	default:
		return AbstractTopLvlWindow::OnCommand (wParam, lParam);
	}
}


LRESULT PlayWithEmulatorWindow::OnOtherMesage (UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_SEAWIZARD_STEP_COMPLETE:
		{
			if (!m_my_gameboard.get () || !wParam)
			{
				Show (SW_SHOWNORMAL);
				return 0;
			}

			EmulatorGameWindow* game_window = new EmulatorGameWindow ();

			if (!game_window->Create (hInst, this, m_my_gameboard, m_is_first, true, m_is_hard))
			{
				game_window->Destroy ();

				return 0;
			}

			Show (SW_HIDE);
			game_window->Show (SW_SHOWNORMAL);

			return 0;
		}

	default:
		return AbstractTopLvlWindow::OnOtherMesage (message, wParam, lParam);
	}
}
