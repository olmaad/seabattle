#pragma once

#include "seawindow.h"

//
class MainWindow : public AbstractTopLvlWindow		//Главное окно (наследник AbstractTopLvlWindow)
{
public:
	bool Create (HINSTANCE hInstance, HWND hWndParent = 0);		//Метод создания главного окна (через AbstractTopLvlWindow)

	static const LPCTSTR lpTemplateName;
protected:
	virtual LRESULT OnCommand (WPARAM wParam, LPARAM lParam);		//Обработка WM_COMMAND
};
