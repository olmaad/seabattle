#include "stdafx.h"

#include "GameBoardUI.h"

#include <libGameEngine/GameBoard.h>

#define _USE_MATH_DEFINES 
#include <math.h>


//// GameBoardWidget ////////////////////////////////////////////////////////////////////////////////////////////////////


//// Get left border size

int GameBoardWidget::LeftBorder () const
{
	return 25;
}


//// Get top border size

int GameBoardWidget::TopBorder () const
{
	return 25;
}


//// Calculate widget size

SIZE GameBoardWidget::CalculateSize () const
{
	SIZE size;

	int board_size = m_game_board.GetSize ();
	SIZE cell_size = GetCellSize ();
	
	size.cx = (cell_size.cx + 1) * board_size + LeftBorder ();
	size.cy = (cell_size.cy + 1) * board_size + TopBorder ();

	return size;
}


//// Constructor

GameBoardWidget::GameBoardWidget (HINSTANCE hInstance, AbstractTopLvlWindow& parent, GameBoard &game_board, IShipController& ships_controller) : AbstractChildWindow (hInstance, parent.GetHandle ()), m_game_board (game_board), m_ships_controller (ships_controller), m_is_locked (false), m_lock_angle (0), m_is_invisible_locked (false)
{
}


//// Create widget window

void GameBoardWidget::Create (int x, int y, HINSTANCE hInstance)
{
	SIZE size = CalculateSize ();

	m_screen.reset (new ColorBitmap (size));

	AbstractChildWindow::Create (0, WS_CHILD | WS_CLIPSIBLINGS, 0, x, y, size.cx, size.cy, hInstance);

	//AHTUNG !!! MAGIC NUMBER (font) !!!
	m_border_font.Create (-14, FW_EXTRALIGHT, FF_SWISS, L"Segoe UI");
}


//// Get cell size (now square)

SIZE GameBoardWidget::GetCellSize () const
{
	SIZE size;

	size.cx = cell_size;
	size.cy = cell_size;

	return size;
}


//// Paint board cells
// Throws InvalidFunctionResultException
void GameBoardWidget::PaintChecks (HDC hdc, RECT ClientRect)
{
	SolidBrush bg_brush (RGB (0xc0, 0xc0, 0xc0));

	FillRect (hdc, &ClientRect, bg_brush);

	int gb_size = m_game_board.GetSize ();
	SolidBrush empty_brush (RGB (0xff, 0xff, 0xff));
	SolidBrush prototype_brush (RGB (0xff, 0x99, 0x00));
	SolidBrush ship_brush (RGB (0x33, 0x66, 0xcc));
	SolidBrush unknown_brush (RGB (0x66, 0x99, 0x66));
	SolidBrush damaged_brush (RGB (0x00, 0x31, 0x53));
	SolidBrush conflict_brush (RGB (0xff, 0x00, 0x00));
	SolidBrush killed_brush (RGB (0x3a, 0x3a, 0x3a));
	SolidBrush miss_brush (RGB (0xc0, 0xc0, 0xc0));

	for (int x = 0;x < gb_size;x = x +1)
	{
		for (int y = 0;y < gb_size;y = y +1)
		{
			const BoardCell& cell_to_fill = m_game_board.Cell (AbsBoardPos (x, y));

			SolidBrush* cell_brush = 0;

			switch (cell_to_fill.cell_type)
			{
			case ct_empty:
				{
					cell_brush = &empty_brush;
					break;
				}
			case ct_prototype:
				{
					cell_brush = &prototype_brush;
					break;
				}
			case ct_ship:
				{
					cell_brush = &ship_brush;
					break;
				}
			case ct_unknown:
				{
					cell_brush = &unknown_brush;
					break;
				}
			case ct_damaged:
				{
					cell_brush = &damaged_brush;
					break;
				}
			case ct_conflict:
				{
					cell_brush = &conflict_brush;
					break;
				}
			case ct_killed:
				{
					cell_brush = &killed_brush;
					break;
				}
			case ct_miss:
				{
					cell_brush = &empty_brush;
					break;
				}
			default:
				throw InvalidFunctionResultException (L"switch for cell_to_fill.cell_type going to 'default' state");
			}

			RECT cell_rect;
			SIZE cell_size = GetCellSize ();

			cell_rect.top = y * cell_size.cy + TopBorder () + y +1;
			cell_rect.bottom = cell_rect.top + cell_size.cy;

			cell_rect.left = x * cell_size.cx + LeftBorder () + x +1;
			cell_rect.right = cell_rect.left + cell_size.cx;

			FillRect (hdc, &cell_rect, *cell_brush);

			if (cell_to_fill.cell_type == ct_miss)
			{
				cell_rect.left = cell_rect.left + cell_size.cx/3;
				cell_rect.right = cell_rect.right - cell_size.cx/3;
				cell_rect.top = cell_rect.top + cell_size.cx/3;
				cell_rect.bottom = cell_rect.bottom - cell_size.cx/3;

				FillRect (hdc, &cell_rect, miss_brush);
			}
		}
	}
}


//// Paint lines + chars

void GameBoardWidget::PaintDecorations (HDC hdc, RECT ClientRect)
{
	int gb_size = m_game_board.GetSize ();

	for (int x = 0;x < gb_size;x = x +1)
	{
		MoveToEx (hdc, x * GetCellSize ().cx + x + LeftBorder (), 0, 0);
		LineTo (hdc, x * GetCellSize ().cx + x + LeftBorder (), ClientRect.bottom);
	}

	for (int y = 0; y < gb_size; y = y +1)
	{
		MoveToEx (hdc, 0, y * GetCellSize ().cy + y + TopBorder (), 0);
		LineTo (hdc, ClientRect.right, y * GetCellSize ().cy + y + TopBorder ());
	}

	SelectObject (hdc, m_border_font);
	SetTextColor(hdc, RGB(80, 80, 80));

	SetBkMode (hdc, TRANSPARENT);

	for (int y = 0;y < gb_size;y = y +1)
	{
		RECT number_text_rect;

		number_text_rect.top = TopBorder () + 1 + y + y * GetCellSize ().cy;
		number_text_rect.bottom = number_text_rect.top + GetCellSize ().cy;

		number_text_rect.left = 0;
		number_text_rect.right = LeftBorder ();

		wchar_t number_text [3];
		swprintf (number_text, L"%d", y +1);

		DrawText(hdc, number_text, -1, &number_text_rect, DT_CENTER | DT_SINGLELINE | DT_VCENTER);
	}

	for (int x = 0;x < gb_size;x = x +1)
	{
		RECT char_text_rect;

		char_text_rect.top = 0;
		char_text_rect.bottom = TopBorder ();

		char_text_rect.left = LeftBorder () + 1 + x + x * GetCellSize ().cx;
		char_text_rect.right = char_text_rect.left + GetCellSize ().cx;

		wchar_t char_text [2];
		char_text[0] = L'A' + x;
		char_text[1] = 0;

		DrawText(hdc, char_text, -1, &char_text_rect, DT_CENTER | DT_SINGLELINE | DT_VCENTER);
	}
}


//// Paint "edit" warship

void GameBoardWidget::PaintEdit (HDC hdc, RECT ClientRect)
{
	EditField* edit_field = m_ships_controller.GetEdit ();

	if (0 == edit_field)
	{
		return;
	}

	SolidBrush add_brush (RGB (0x33, 0x66, 0xcc));
	SolidBrush del_brush (RGB (0xff, 0xff, 0xff));

	AbsBoardPos edit_field_pos = edit_field->GetPos ();
	RelBoardPosList edit_field_points = edit_field->GetPrototype ()->GetPoints ();

	for (RelBoardPosList::const_iterator counter = edit_field_points.begin ();counter != edit_field_points.end ();counter ++)
	{
		SolidBrush* cell_brush = &del_brush;

		if (ed_add == edit_field->GetFlag ())
		{
			cell_brush = &add_brush;
		}

		int x = edit_field_pos.x + counter->x;
		int y = edit_field_pos.y + counter->y;

		RECT cell_rect;
		SIZE cell_size = GetCellSize ();

		cell_rect.top = y * cell_size.cy + TopBorder () + y +1;
		cell_rect.bottom = cell_rect.top + cell_size.cy;

		cell_rect.left = x * cell_size.cx + LeftBorder () + x +1;
		cell_rect.right = cell_rect.left + cell_size.cx;
			
		FillRect (hdc, &cell_rect, *cell_brush);
	}
}


////

void GameBoardWidget::PaintNew (HDC hdc, RECT ClientRect)
{
	NewWarshipField* new_field = m_ships_controller.GetNew ();

	if (0 == new_field)
	{
		return;
	}

	SolidBrush field_brush (RGB (0x33, 0x66, 0xcc));

	AbsBoardPos field_pos = new_field->GetPos ();
	RelBoardPosList field_points = new_field->GetPrototype ()->GetPoints ();

	for (RelBoardPosList::const_iterator counter = field_points.begin ();counter != field_points.end ();counter ++)
	{
		int x = field_pos.x + counter->x;
		int y = field_pos.y + counter->y;

		RECT cell_rect;
		SIZE cell_size = GetCellSize ();

		cell_rect.top = y * cell_size.cy + TopBorder () + y +1;
		cell_rect.bottom = cell_rect.top + cell_size.cy;

		cell_rect.left = x * cell_size.cx + LeftBorder () + x +1;
		cell_rect.right = cell_rect.left + cell_size.cx;
			
		FillRect (hdc, &cell_rect, field_brush);
	}
}


////

void GameBoardWidget::PaintLock (HDC hdc, RECT ClientRect)
{
	if (m_is_locked)
	{
		ColorPen lock_pen_one (RGB (192, 192, 192), 10);
		//192
		//176
		//160

		SIZE client_size = {ClientRect.right - ClientRect.left, ClientRect.bottom - ClientRect.top};

		RECT lock_rect = {(client_size.cx - cell_size - 1)/4 + cell_size + 1, (client_size.cy - cell_size - 1)/4 + cell_size + 1, 3 * (client_size.cx - cell_size - 1)/4 + cell_size + 1, 3 * (client_size.cy - cell_size - 1)/4 + cell_size + 1};

		SIZE lock_size = {lock_rect.right - lock_rect.left, lock_rect.bottom - lock_rect.top};

		m_lock_rect.top = lock_rect.top - lock_size.cx/12;
		m_lock_rect.left = lock_rect.left - lock_size.cx/12;
		m_lock_rect.right = lock_rect.right + lock_size.cx/12;
		m_lock_rect.bottom = lock_rect.bottom + lock_size.cx/12;

		for (int counter = 0;counter < 8;counter = counter +1)
		{
			double move_x = lock_size.cx/2 + lock_size.cx * cos (m_lock_angle + counter * M_PI_4)/4;
			double move_y = lock_size.cy/2 + lock_size.cy * sin (m_lock_angle + counter * M_PI_4)/4;

			double to_x = lock_size.cx/2 + lock_size.cx * cos (m_lock_angle + counter * M_PI_4)/2;
			double to_y = lock_size.cy/2 + lock_size.cy * sin (m_lock_angle + counter * M_PI_4)/2;

			ColorPen lock_pen (RGB (192 - 20 * counter, 192 - 20 * counter, 192 - 20 * counter), lock_size.cx/6);

			SelectObject (hdc, lock_pen);

			MoveToEx (hdc, (int) (lock_rect.left + move_x), (int) (lock_rect.top + move_y), 0);
			LineTo (hdc, (int) (lock_rect.left + to_x), (int) (lock_rect.top + to_y));
		}
	}
}


//// WM_PAINT processor

LRESULT GameBoardWidget::OnPaint ()
{
	PAINTSTRUCT ps;
	HDC hdc;
	DrawSurface dc;
	RECT ClientRect;

	SelectObject (dc, *m_screen);

	GetClientRect(m_handle, &ClientRect);

	PaintChecks (dc, ClientRect);
	PaintDecorations (dc, ClientRect);
	PaintEdit (dc, ClientRect);
	PaintNew (dc, ClientRect);
	PaintLock (dc, ClientRect);

	hdc = BeginPaint(m_handle, &ps);

	BitBlt (hdc, 0, 0, ClientRect.right - ClientRect.left, ClientRect.bottom - ClientRect.top, dc, 0, 0, SRCCOPY);

	EndPaint(m_handle, &ps);

	return 1;
}


//// Get mouse position in cell coordinate (absboardpos) from pixels coordinate

AbsBoardPos GameBoardWidget::GetMouseBoardPosition (int x, int y)
{
	x = (x - LeftBorder () -1) / (GetCellSize ().cx + 1);
	y = (y - TopBorder () -1) / (GetCellSize ().cy + 1);

	return AbsBoardPos (x, y);
}


//// Invalidate cell rect

void GameBoardWidget::InvalidateRect (AbsBoardPos mouse_pos)
{
	RECT mouse_rect;

	mouse_rect.left = LeftBorder () + 1 + mouse_pos.x * (cell_size + 1);
	mouse_rect.right = mouse_rect.left + cell_size + 1;

	mouse_rect.top = TopBorder () + 1 + mouse_pos.y * (cell_size + 1);
	mouse_rect.bottom = mouse_rect.top + cell_size + 1;

	::InvalidateRect (m_handle, &mouse_rect, true);
}


//// 

bool GameBoardWidget::IsCurPosAllowed (const AbsBoardPos& cur_pos)
{
	int board_size = m_game_board.GetSize ();

	return cur_pos.x >= 0 && cur_pos.x < board_size && cur_pos.y >= 0 && cur_pos.y < board_size;
}


//// GetMouseBoardPosition + call controller + invalidaterect

LRESULT GameBoardWidget::OnLeftMouseDown (LPARAM lParam)
{
	if (m_is_invisible_locked || m_is_locked)
	{
		return 0;
	}

	AbsBoardPos mouse_pos = GetMouseBoardPosition (LOWORD (lParam), HIWORD (lParam));

	if (IsCurPosAllowed (mouse_pos))
	{
		m_ships_controller.OnClickLeft (mouse_pos);

		InvalidateRect (mouse_pos);
	}

	return 0;
}


////

LRESULT GameBoardWidget::OnLeftMouseUp ()
{
	if (m_is_invisible_locked || m_is_locked)
	{
		return 0;
	}

	if (!m_ships_controller.GetEdit () && !m_ships_controller.GetNew ())
	{
		// It's normal because i don't know about mouse outside widget
		//
		//THROW_TEXT ("Recieved mouse_up when mouse_down don't !");

		return 0;
	}

	m_ships_controller.OnReleaseLeft ();

	::InvalidateRect (m_handle, 0, true);

	return 0;
}


////

LRESULT GameBoardWidget::OnRightMouseDown (LPARAM lParam)
{
	if (m_is_invisible_locked || m_is_locked)
	{
		return 0;
	}

	AbsBoardPos mouse_pos = GetMouseBoardPosition (LOWORD (lParam), HIWORD (lParam));

	if (IsCurPosAllowed (mouse_pos))
	{
		m_ships_controller.OnClickRight (mouse_pos);

		InvalidateRect (mouse_pos);
	}

	return 0;
}


////

LRESULT GameBoardWidget::OnRightMouseUp (LPARAM lParam)
{
	if (m_is_invisible_locked || m_is_locked)
	{
		return 0;
	}

	AbsBoardPos mouse_pos = GetMouseBoardPosition (LOWORD (lParam), HIWORD (lParam));

	m_ships_controller.OnReleaseRight (mouse_pos);

	::InvalidateRect (m_handle, 0, true);

	return 0;
}


//// If cursor position changed call controller + invalidaterect

LRESULT GameBoardWidget::OnMouseMove (LPARAM lParam)
{
	if (m_is_invisible_locked || m_is_locked)
	{
		return 0;
	}

	if (m_ships_controller.GetEdit () && ed_add == m_ships_controller.GetEdit ()->GetFlag ())
	{
		AbsBoardPos mouse_pos = GetMouseBoardPosition (LOWORD (lParam), HIWORD (lParam));

		if (mouse_pos != m_ships_controller.GetEdit ()->GetLast () && IsCurPosAllowed (mouse_pos))
		{
			m_ships_controller.OnSelectChange (mouse_pos);

			InvalidateRect (mouse_pos);
		}
	}
	if (m_ships_controller.GetNew ())
	{
		AbsBoardPos mouse_pos = GetMouseBoardPosition (LOWORD (lParam), HIWORD (lParam));

		if (mouse_pos != m_ships_controller.GetNew ()->GetPos () && IsCurPosAllowed (mouse_pos))
		{
			m_ships_controller.OnSelectChange (mouse_pos);

			RECT client_rect;
			GetClientRect (m_handle, &client_rect);

			::InvalidateRect (m_handle, &client_rect, true);
		}
	}

	return 0;
}


////

LRESULT GameBoardWidget::OnTimer ()
{
	m_lock_angle = m_lock_angle + M_PI_4;

	if (abs(m_lock_angle - 2 * M_PI) < M_PI_4)
	{
		m_lock_angle = 0;
	}

	::InvalidateRect (m_handle, &m_lock_rect, false);

	return 0;
}


//// PrototypeWidget ////////////////////////////////////////////////////////////////////////////////////////////////////


//// Constructor

PrototypeWidget::PrototypeWidget (HINSTANCE hInstance, AbstractTopLvlWindow& parent, GameBoard &game_board, IShipController& src_ships_controller) : AbstractChildWindow (hInstance, parent.GetHandle ()), m_game_board (game_board), m_ships_controller (src_ships_controller), m_icon_amount (0), m_current_page (1), m_prototypes_amount (0), m_mouse_captured (false)
{
	m_ships_controller.BindPrototypeWidget (this);
}


//// 

PrototypeWidget::~PrototypeWidget ()
{
	m_ships_controller.UnBindPrototypeWidget (this);
}


////

void PrototypeWidget::Create (int x, int y, int height, int width, HINSTANCE hInstance)
{
	m_icon_amount = (height -control_footer_height) / 80;
	m_icon_size.cy = (height - control_footer_height - control_spacing) / m_icon_amount - control_spacing;
	m_icon_size.cx = width - 2 * control_width - 2 * control_spacing;


	SIZE widget_size = {width, height};
	m_screen.reset (new ColorBitmap (widget_size));

	AbstractChildWindow::Create (0, WS_CHILD | WS_CLIPSIBLINGS, 0, x, y, width, height, hInstance);

	m_track_mouse_event.dwHoverTime = HOVER_DEFAULT;
	m_track_mouse_event.dwFlags = TME_LEAVE;
	m_track_mouse_event.hwndTrack = m_handle;
	m_track_mouse_event.cbSize = sizeof (TRACKMOUSEEVENT);

	TrackMouseEvent (&m_track_mouse_event);

	const WarShipPrototypeList& prototype_list = m_game_board.GetPrototypeList ();

	if (prototype_list.size () != 0)
	{
		for (WarShipPrototypeList::const_iterator counter = prototype_list.begin ();counter != prototype_list.end ();counter ++)
		{
			NotifyPrototypeAdded (counter->get ());
		}
	}
}


// Throws InvalidFunctionCallException
void PrototypeWidget::NotifyPrototypeAdded(const WarShipPrototype* prototype)
{
	// create bitmap
	// create dc
	// render icon
	// paste entry in map
	// invalidate (v)

	auto new_bitmap = std::make_unique<ColorBitmap>(m_icon_size);

	DrawSurface dc;
	SelectObject(dc, *new_bitmap);

	POINT offset = { 0, 0 };

	RenderIcon(dc, m_icon_size, offset, prototype, false);

	if (m_prototype_icons_map.find(prototype) != m_prototype_icons_map.end())
	{
		throw InvalidFunctionCallException(L"m_prototype_icons_map.find (prototype) != m_prototype_icons_map.end () - prototype icon already rendered");
	}

	m_prototype_icons_map.emplace(prototype, std::move(new_bitmap));

	RECT client_rect;
	GetClientRect(m_handle, &client_rect);

	InvalidateRect(client_rect);
}

// InvalidArgumentException
void PrototypeWidget::NotifyPrototypeDeleted (const WarShipPrototype* prototype)
{
	// find entry in map; erase
	// invalidate

	TIconMap::iterator find_res = m_prototype_icons_map.find (prototype);

	if (find_res == m_prototype_icons_map.end ())
	{
		throw InvalidArgumentException (L"find_res == m_prototype_icons_map.end () - deleting prototype not exist");
	}

	if (find_res != m_prototype_icons_map.end ())
	{
		m_prototype_icons_map.erase (find_res);
	}

	RECT client_rect;
	GetClientRect (m_handle, &client_rect);

	InvalidateRect (client_rect);
}


////

void PrototypeWidget::NotifyWarshipCountChanged ()
{
	RECT client_rect;
	GetClientRect (m_handle, &client_rect);
	InvalidateRect (client_rect);
}


////

void PrototypeWidget::InvalidateRect (RECT update_rect)
{
	::InvalidateRect (m_handle, &update_rect, true);
}


////

int PrototypeWidget::GetMousePosition (POINT cur_pos, RECT& client_rect)
{
	if (cur_pos.x >= client_rect.left && cur_pos.x <= client_rect.left + control_width)
	{
		return mp_left_list;
	}
	if (cur_pos.x >= client_rect.right - control_width && cur_pos.x <= client_rect.right)
	{
		return mp_right_list;
	}
	if (cur_pos.x >= client_rect.left + control_width + control_spacing && cur_pos.x <= client_rect.right - control_width - control_spacing && cur_pos.y <= client_rect.bottom - control_footer_height)
	{
		int icon_height = (client_rect.bottom - client_rect.top - control_footer_height - control_spacing) / m_icon_amount - control_spacing;
		return cur_pos.y / (icon_height + control_spacing) + 1;
	}

	return 0;
}


////

int PrototypeWidget::CalculatePageAmount ()
{
	int page_amount = m_prototypes_amount / m_icon_amount;

	if (m_prototypes_amount % m_icon_amount != 0)
	{
		page_amount = page_amount +1;
	}
	if (!m_prototypes_amount)
	{
		page_amount = 1;
	}

	return page_amount;
}


////

LRESULT PrototypeWidget::OnMouseMove (LPARAM lParam)
{
	POINT mouse_position = {LOWORD (lParam), HIWORD (lParam)};
	RECT client_rect;

	if (!m_mouse_captured)
	{
		m_mouse_captured = true;

		TrackMouseEvent (&m_track_mouse_event);
	}

	GetClientRect (m_handle, &client_rect);

	int cur_selection = GetMousePosition (mouse_position, client_rect);

	if (m_mouse_select != cur_selection)
	{
		m_mouse_select = cur_selection;

		InvalidateRect (client_rect);
	}

	return 0;
}


////

LRESULT PrototypeWidget::OnLeftMouseDown (LPARAM lParam)
{
	RECT client_rect;
	GetClientRect (m_handle, &client_rect);

	POINT mouse_position = {LOWORD (lParam), HIWORD (lParam)};

	int cur_selection = GetMousePosition (mouse_position, client_rect);
	int page_amount = CalculatePageAmount ();

	if (cur_selection == mp_left_list)
	{
		m_current_page = m_current_page -1;

		if (!m_current_page)
		{
			m_current_page = page_amount;
		}
	}
	if (cur_selection == mp_right_list)
	{
		m_current_page = m_current_page +1;

		if (m_current_page > page_amount)
		{
			m_current_page = 1;
		}
	}
	if (cur_selection > 0)
	{
		m_ships_controller.OnClickLeft ((m_current_page -1) * m_icon_amount + cur_selection);
	}

	InvalidateRect (client_rect);

	return 0;
}


////

LRESULT PrototypeWidget::OnOtherMesage (UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_MOUSELEAVE)
	{
		m_mouse_select = 0;
		m_mouse_captured = false;

		RECT client_rect;
		GetClientRect (m_handle, &client_rect);
		InvalidateRect (client_rect);

		return 0;
	}

	return AbstractChildWindow::OnOtherMesage (message, wParam, lParam);
}


////

LRESULT PrototypeWidget::OnPaint ()
{
	PAINTSTRUCT ps;
	HDC hdc;
	RECT ClientRect;
	
	DrawSurface dc;

	SelectObject (dc, *m_screen);
	GetClientRect(m_handle, &ClientRect);

	PaintControls (dc, ClientRect);
	
	hdc = BeginPaint(m_handle, &ps);

	BitBlt (hdc, 0, 0, ClientRect.right - ClientRect.left, ClientRect.bottom - ClientRect.top, dc, 0, 0, SRCCOPY);

	EndPaint(m_handle, &ps);

	return 1;
}


// Throws InvalidArgumentException
void PrototypeWidget::PaintControls (HDC hdc, RECT ClientRect)
{
	RECT left_control;
	RECT right_control;

	VectorFont control_page_font;
	VectorFont icon_font;

	control_page_font.Create (-15, FW_EXTRALIGHT, FF_SWISS, L"Segoe UI");
	icon_font.Create (-20, FW_LIGHT, FF_SWISS, L"Segoe UI");

	left_control.top = ClientRect.top;
	left_control.bottom = ClientRect.bottom;

	right_control.top = ClientRect.top;
	right_control.bottom = ClientRect.bottom;

	left_control.left = ClientRect.left;
	left_control.right = ClientRect.left + control_width;

	right_control.left = ClientRect.right - control_width;
	right_control.right = ClientRect.right;

	SolidBrush control_brush_light (RGB (0xc0, 0xc0, 0xc0));
	SolidBrush control_brush_dark (RGB (0x50, 0x50, 0x50));
	SolidBrush bg_brush (RGB (0xff, 0xff, 0xff));

	SolidBrush* control_brush_left = &control_brush_light;
	SolidBrush* control_brush_right = &control_brush_light;

	if (mp_left_list == m_mouse_select)
	{
		control_brush_left = &control_brush_dark;
	}
	if (mp_right_list == m_mouse_select)
	{
		control_brush_right = &control_brush_dark;
	}

	int client_height = ClientRect.bottom - ClientRect.top;

	FillRect (hdc, &ClientRect, bg_brush);

	SetBkMode (hdc, TRANSPARENT);

	const WarShipPrototypeList& list = m_game_board.GetPrototypeList ();

	WarShipPrototypeList::const_iterator painting_prototype = list.begin ();

	int list_size = m_game_board.GetPrototypeList ().size ();

	if (list_size != m_prototypes_amount)
	{
		m_prototypes_amount = list_size;
	}

	int max_page_amount = CalculatePageAmount ();

	if (m_current_page > max_page_amount)
	{
		m_current_page = max_page_amount;
	}

	for (int counter = 0;counter < (m_current_page -1) * m_icon_amount;counter = counter +1)
	{
		painting_prototype ++;
	}

	DrawSurface bitmap_dc;
	POINT icon_point = {left_control.right + control_spacing, control_spacing};

	SelectObject (hdc, icon_font);
	SetTextColor(hdc, RGB (0x7a, 0x7a, 0x7a));

	if (list.size () != 0)
	{
		for (int counter = 0;counter < m_icon_amount;counter = counter + 1)
		{
			if (counter == m_mouse_select -1)
			{
			}

			if (painting_prototype != list.end ())
			{
				TIconMap::iterator find_res = m_prototype_icons_map.find (painting_prototype->get ());

				if (find_res == m_prototype_icons_map.end ())
				{
					throw InvalidArgumentException (L"find_res == m_prototype_icons_map.end () - can't find prototype's icon");
				}

				ColorBitmap& color_bitmap = *(*find_res).second;

				SelectObject (bitmap_dc, color_bitmap);

				BitBlt (hdc, icon_point.x, icon_point.y, m_icon_size.cx, m_icon_size.cy, bitmap_dc, 0, 0, SRCCOPY);


				RECT icon_rect = {icon_point.x, icon_point.y, icon_point.x + m_icon_size.cx, icon_point.y + m_icon_size.cy};

				wchar_t icon_text [10];

				swprintf (icon_text, L"%d/%d", (*painting_prototype)->GetAllocated (), (*painting_prototype)->GetLimit ());

				RECT text_rect = {icon_rect.left, icon_rect.bottom - 25, icon_rect.right - 5, icon_rect.bottom};

				DrawText(hdc, icon_text, -1, &text_rect, DT_RIGHT | DT_SINGLELINE);

				icon_point.y = icon_point.y + m_icon_size.cy + control_spacing;
				painting_prototype ++;
			}
		}
	}

	FillRect (hdc, &left_control, *control_brush_left);
	FillRect (hdc, &right_control, *control_brush_right);

	ColorPen control_pen (RGB (0x7a, 0x7a, 0x7a), 5);
	ColorPen bottom_pen (RGB (0xc0, 0xc0, 0xc0), 5);

	SelectObject (hdc, control_pen);

	MoveToEx (hdc, left_control.right - control_width/3, client_height/2 - 15, 0);
	LineTo (hdc, left_control.left + control_width/3, client_height/2);
	LineTo (hdc, left_control.right - control_width/3, client_height/2 + 15);

	MoveToEx (hdc, right_control.left + control_width/3, client_height/2 - 15, 0);
	LineTo (hdc, right_control.right - control_width/3, client_height/2);
	LineTo (hdc, right_control.left + control_width/3, client_height/2 + 15);

	SelectObject (hdc, bottom_pen);

	MoveToEx (hdc, left_control.right + control_spacing, ClientRect.bottom - control_footer_height, 0);
	LineTo (hdc, right_control.left - control_spacing, ClientRect.bottom - control_footer_height);

	wchar_t footer_text [10];

	SelectObject (hdc, control_page_font);
	SetTextColor(hdc, RGB(0xc0, 0xc0, 0xc0));

	swprintf (footer_text, L"%d/%d", m_current_page, CalculatePageAmount ());

	RECT footer_rect = {left_control.right + control_spacing, ClientRect.bottom - control_footer_height + control_spacing, right_control.left - control_spacing, ClientRect.bottom - control_spacing};

	DrawText(hdc, footer_text, -1, &footer_rect, DT_CENTER | DT_SINGLELINE | DT_VCENTER);
}


////

void PrototypeWidget::RenderIcon (HDC dc, SIZE size, POINT offset, const WarShipPrototype* prototype, bool selected)
{
	RECT icon_rect = {offset.x, offset.y, size.cx + offset.x, size.cy + offset.y};

	SolidBrush brush_unselected (RGB (0xc0, 0xc0, 0xc0));
	SolidBrush brush_selected (RGB (0x50, 0x50, 0x50));

	SolidBrush cell_ship_brush (RGB (0x33, 0x66, 0xcc));
	SolidBrush cell_empty_brush (RGB (0xd0, 0xd0, 0xd0));

	SolidBrush* icon_brush = &brush_unselected;

	if (selected)
	{
		icon_brush = &brush_selected;
	}

	FillRect (dc, &icon_rect, *icon_brush);

	int max_prototype_size = prototype->max_size;

	POINT begin_point = {(size.cx - max_prototype_size * control_spacing) / 2 + offset.x, 2 * (size.cy - max_prototype_size * control_spacing) / 3 + offset.y};

	const RelBoardPosList& point_list = prototype->GetPoints ();

	POINT ship_max = {0, 0};
	POINT ship_offset;

	for (RelBoardPosList::const_iterator counter = point_list.begin ();counter != point_list.end ();counter ++)
	{
		if (counter->x > ship_max.x)
		{
			ship_max.x = counter->x;
		}
		if (counter->y > ship_max.y)
		{
			ship_max.y = counter->y;
		}
	}

	ship_offset.x = (max_prototype_size - ship_max.x - 1) / 2;
	ship_offset.y = (max_prototype_size - ship_max.y - 1) / 2;

	RECT ship_rect = {begin_point.x, begin_point.y, begin_point.x + max_prototype_size * cell_size, begin_point.y + max_prototype_size * cell_size};

	FillRect (dc, &ship_rect, cell_empty_brush);

	for (RelBoardPosList::const_iterator counter = point_list.begin ();counter != point_list.end ();counter ++)
	{
		RECT cell_rect = {begin_point.x + (counter->x + ship_offset.x) * cell_size, begin_point.y + (counter->y + ship_offset.y) * cell_size, begin_point.x + cell_size + (counter->x + ship_offset.x) * cell_size, begin_point.y + cell_size + (counter->y + ship_offset.y) * cell_size};

		FillRect (dc, &cell_rect, cell_ship_brush);
	}
}
