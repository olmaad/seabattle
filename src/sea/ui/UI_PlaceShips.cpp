#include "stdafx.h"
#include "resources/resource.h"
#include "UI_PlaceShips.h"
#include "UI_NewGame.h"
#include "UI_JoinGame.h"
#include "UI_GameWindow.h"
#include "GameBoardUI.h"



const LPCTSTR PlaceShipsWindow::lpTemplateName = MAKEINTRESOURCE(IDD_PLACESHIPS);



//// PlaceShipsWindow ////////////////////////////////////////////////////////////////////////////////////////////////////


//// Constructor

PlaceShipsWindow::PlaceShipsWindow () : m_parent (0), m_accepted (false)
{
}


//// Destructor

PlaceShipsWindow::~PlaceShipsWindow ()
{
	if (0 != m_parent)
	{
		::PostMessage (m_parent->GetHandle(), WM_SEAWIZARD_STEP_COMPLETE, m_accepted, 0);
	}
}


//// Delete placeshipwindow object

LRESULT PlaceShipsWindow::OnDestroy (HWND hWnd)
{
	try
	{
		delete this;
	}
	catch (std::exception &e)
	{
		MessageBox (m_handle, AnsiToUnicode (e.what ()).c_str (), __FUNCTIONW__, MB_ICONSTOP);
	}
	catch (...)
	{
		MessageBox (hWnd, L"Something wrong...", __FUNCTIONW__, MB_ICONSTOP);
	}

	return 0;
}


//// Import points from array to relboardposlist

template<int x_size, int y_size> RelBoardPosList ImportPointList (bool (&points)[y_size][x_size])
{
	RelBoardPosList points_list;

	for (int x = 0;x < x_size;x = x +1)
	{
		for(int y = 0;y < y_size;y = y +1)
		{
			if (points[y][x])
			{
				points_list.push_back (RelBoardPos (x, y));
			}
		}
	}

	return points_list;
}


//// Create window + call widget create + resize window

bool PlaceShipsWindow::Create (HINSTANCE hInstance, AbstractTopLvlWindow* parent, int board_size, bool is_host, WarShipPrototypeList& host_prototype_list)
{
	try
	{
		HWND parent_handle = 0;

		if (0 != parent)
		{
			parent_handle = parent->GetHandle ();
		}
		else
		{
			return false;
		}

		m_gameboard.reset (new GameBoard ());

		m_gameboard->CreateEmpty (board_size);

		if (!AbstractTopLvlWindow::Create (hInstance, lpTemplateName, parent_handle))
		{
			return false;
		}

		m_parent = parent;

		if (is_host)
		{
			m_controller = CreateHostShipsController (*m_gameboard);
		}
		else
		{
			m_controller = CreateJoinShipsController (*m_gameboard);
			m_gameboard->SetAllowedWarShips (host_prototype_list);
		}

		m_prototype_widget.reset (new PrototypeWidget (hInstance, *this, *m_gameboard, *m_controller));

		m_gb_widget.reset (new GameBoardWidget (hInstance, *this, *m_gameboard, *m_controller));

		m_gb_widget->Create (control_spacing, control_spacing, hInstance);

		SIZE widget_size = m_gb_widget->CalculateSize ();
		SIZE window_size;
		SIZE client_size;
		SIZE button_size;

		RECT button_rect;
		GetWindowRect (GetDlgItem (IDCANCEL), &button_rect);

		SIZE extra_size;
		RECT client_rect;
		RECT window_rect;
		GetClientRect (m_handle, &client_rect);
		GetWindowRect (m_handle, &window_rect);

		button_size.cx = button_rect.right - button_rect.left;
		button_size.cy = button_rect.bottom - button_rect.top;

		extra_size.cx = (window_rect.right - window_rect.left) - (client_rect.right - client_rect.left);
		extra_size.cy = (window_rect.bottom - window_rect.top) - (client_rect.bottom - client_rect.top);

		client_size.cx = max (widget_size.cx + 2 * control_spacing, 3 * control_spacing + 2 * button_size.cx) + control_spacing + prototype_widget_width;
		client_size.cy = widget_size.cy + 3 * control_spacing + button_size.cy;

		m_prototype_widget->Create (max (widget_size.cx + 2 * control_spacing, 3 * control_spacing + 2 * button_size.cx), control_spacing, widget_size.cy, prototype_widget_width, hInstance);

		window_size.cx = client_size.cx + extra_size.cx;
		window_size.cy = client_size.cy + extra_size.cy;

		SetWindowPos (m_handle, 0, 0, 0, window_size.cx, window_size.cy, SWP_NOMOVE | SWP_NOZORDER);
		SetWindowPos (GetDlgItem (IDCANCEL), 0, client_size.cx - 2 * control_spacing - 2 * button_size.cx, client_size.cy - control_spacing - button_size.cy, button_size.cx, button_size.cy, SWP_NOZORDER);
		SetWindowPos (GetDlgItem (IDOK), 0, client_size.cx - control_spacing - button_size.cx, client_size.cy - control_spacing - button_size.cy, button_size.cx, button_size.cy, SWP_NOZORDER);


		ShowWindow (m_prototype_widget->GetHandle (), SW_SHOWNORMAL);
		ShowWindow (m_gb_widget->GetHandle (), SW_SHOWNORMAL);
	}
	catch (std::exception &e)
	{
		MessageBox (m_handle, AnsiToUnicode (e.what ()).c_str (), __FUNCTIONW__, MB_ICONSTOP);
		return false;
	}
	catch (...)
	{
		MessageBox (m_handle, L"Something wrong...", __FUNCTIONW__, MB_ICONSTOP);
		return false;
	}

	return true;
}





//// WM_COMMAND processor

LRESULT PlaceShipsWindow::OnCommand (WPARAM wParam, LPARAM lParam)
{
	try
	{
		switch (LOWORD(wParam))
		{
		case IDCANCEL:
			// LOL
			Destroy ();
			break;
		case IDOK:
			if (0 != m_parent)
			{
				if (m_controller->IsHost ())
				{
					((NewGameWindow*) m_parent) -> AcceptGameBoard (m_gameboard);
				}
				else
				{
					const WarShipPrototypeList& list = m_gameboard->GetPrototypeList ();

					for (WarShipPrototypeList::const_iterator counter = list.begin ();counter != list.end ();counter ++)
					{
						if ((*counter)->GetAllocated () != (*counter)->GetLimit ())
						{
							return 0;
						}
					}

					((ClientGameWindow*) m_parent) -> AcceptGameBoard (m_gameboard);
				}

				m_accepted = true;
			}
			Destroy ();
			return 0;

		default:
			return AbstractTopLvlWindow::OnCommand (wParam, lParam);
		}
	}
	catch (std::exception &e)
	{
		MessageBox (m_handle, AnsiToUnicode (e.what ()).c_str (), __FUNCTIONW__, MB_ICONSTOP);
	}
	catch (...)
	{
		MessageBox (m_handle, L"Something wrong...", __FUNCTIONW__, MB_ICONSTOP);
	}

	return 0;
}

