#include "stdafx.h"

#include "UI_GameWindow.h"

#include "UI_PlaceShips.h"
#include "resources/resource.h"

#include <libNetwork/NetworkController.h>
#include <libSystem/Socket.h>

namespace
{
	const wchar_t* AbilityTypeToString(ShipAbilityType type)
	{
		switch (type)
		{
		case sa_shot:
			return L"shot";
		case sa_move:
			return L"move";
		case sa_torpedo:
			return L"torpedo";
		case sa_rocket:
			return L"rocket";
		case sa_maac:
			return L"MAAC";
		case sa_illusion:
			return L"illusion";
		case sa_emp:
			return L"EMP";
		case sa_scouting:
			return L"scouting";
		case sa_fighter_drone:
			return L"fighter drone";
		case sa_interceptor_drone:
			return L"interceptor drone";
		case sa_prospector_drone:
			return L"prospector drone";
		case sa_carpet_bombing:
			return L"carpet bombing";
		case sa_barrage_fire:
			return L"barrage fire";
		case sa_full_combat_readiness:
			return L"full combat readiness";
		}

		return L"unknown ability";
	}
}

const LPCTSTR BasicGameWindow::lpTemplateName = MAKEINTRESOURCE(IDD_GAMEWINDOW);

BasicGameWindow::BasicGameWindow() :
	m_parent(0),
	m_game_engine_side(0),
	m_game_controller_side(0),
	m_is_cancel_locked(false),
	m_log_printer(0)
{}

BasicGameWindow::~BasicGameWindow()
{
	if (m_parent)
	{
		m_parent->Show(SW_SHOWNORMAL);
	}
}

// Throws InvalidArgumentException, WrongCalculationException
void BasicGameWindow::CreateVisuals(HINSTANCE hInstance, AbstractTopLvlWindow* parent, std::wstring suffix)
{
	if (m_my_gameboard.get() == 0 ||
		m_enemy_gameboard.get() == 0 ||
		m_game_engine_manage.get() == 0 ||
		m_game_controller_side == 0)
	{
		throw InvalidPrivateParameterStateException(L"try create visuals without init");
	}

	m_my_controller = CreateFictitiousShipsController(*m_my_gameboard);
	m_enemy_controller = CreateBattleShipsController(*m_enemy_gameboard, m_game_controller_side);

	m_my_gbwidget.reset(new GameBoardWidget(hInstance, *this, *m_my_gameboard, *m_my_controller));
	m_my_gbwidget->Create(control_spacing, 2 * control_spacing + group_box_height, hInstance);

	const SIZE my_gbwidget_size = m_my_gbwidget->CalculateSize();

	m_enemy_gbwidget.reset(new GameBoardWidget(hInstance, *this, *m_enemy_gameboard, *m_enemy_controller));
	m_enemy_gbwidget->Create(3 * control_spacing + my_gbwidget_size.cx, 2 * control_spacing + group_box_height, hInstance);

	const SIZE enemy_gbwidget_size = m_enemy_gbwidget->CalculateSize();

	if (my_gbwidget_size.cx != enemy_gbwidget_size.cx)
	{
		throw WrongCalculationException(L"my_gbwidget_size.cx != enemy_gbwidget_size.cx - widgets hasn't same size");
	}

	SIZE window_size;
	SIZE client_size;

	RECT button_rect;
	GetWindowRect(GetDlgItem(IDC_SURRENDER), &button_rect);

	SIZE extra_size;
	RECT client_rect;
	RECT window_rect;
	GetClientRect(m_handle, &client_rect);
	GetWindowRect(m_handle, &window_rect);

	SIZE button_size;
	button_size.cx = button_rect.right - button_rect.left;
	button_size.cy = button_rect.bottom - button_rect.top;

	extra_size.cx = (window_rect.right - window_rect.left) - (client_rect.right - client_rect.left);
	extra_size.cy = (window_rect.bottom - window_rect.top) - (client_rect.bottom - client_rect.top);

	client_size.cx = max(my_gbwidget_size.cx + enemy_gbwidget_size.cx + 4 * control_spacing, 2 * control_spacing + button_size.cx);
	client_size.cy = max(my_gbwidget_size.cy, enemy_gbwidget_size.cy) + 4 * control_spacing + button_size.cy + group_box_height;

	window_size.cx = client_size.cx + extra_size.cx;
	window_size.cy = client_size.cy + extra_size.cy;

	//SetWindowPos (m_handle, 0, 0, 0, window_size.cx, window_size.cy, SWP_NOMOVE | SWP_NOZORDER);
	//SetWindowPos (GetDlgItem (IDC_SURRENDER), 0, control_spacing, client_size.cy - control_spacing - button_size.cy, button_size.cx, button_size.cy, SWP_NOZORDER);
	//SetWindowPos (GetDlgItem (IDC_STATUSGB), 0, control_spacing, control_spacing, client_size.cx - 2 * control_spacing, group_box_height, SWP_NOZORDER);
	//SetWindowPos (GetDlgItem (IDC_GAMESTATUS), 0, 2 * control_spacing, 3 * control_spacing, client_size.cx - 4 * control_spacing, status_box_height, SWP_NOZORDER);

	std::wstring caption = suffix;
	caption += L" (";
	std::wstring fgfg = m_game_controller_side->IGetMyAlias();
	caption += fgfg;
	caption += L")";

	SendMessage(m_handle, WM_SETTEXT, 0, (LPARAM)caption.c_str());

	ShowWindow(m_my_gbwidget->GetHandle(), SW_SHOWNORMAL);
	ShowWindow(m_enemy_gbwidget->GetHandle(), SW_SHOWNORMAL);

	m_enemy_gbwidget->InvisibleLock();
	//TODO: Разобрать, что это было
	//EnableWindow (GetDlgItem (IDCANCEL), FALSE);
	DisableCancel();
}

// Throws InvalidPrivateParameterStateException
bool BasicGameWindow::Create(HINSTANCE hInstance, AbstractTopLvlWindow* parent)
{
	try
	{
		HWND parent_handle = 0;

		if (0 != parent)
		{
			parent_handle = parent->GetHandle();
		}
		else
		{
			return false;
		}

		if (!AbstractTopLvlWindow::Create(hInstance, lpTemplateName, parent_handle))
		{
			return false;
		}

		m_parent = parent;

		if (m_game_engine_manage.get() == 0 ||
			m_game_controller_side == 0 ||
			m_game_engine_side == 0 ||
			m_enemy_game_engine_manage.get() == 0 ||
			m_enemy_game_engine_side == 0)
		{
			throw InvalidPrivateParameterStateException(L"creating (basic) window without init");
		}

		m_game_engine_manage->ConnectOtherSide(m_enemy_game_engine_side);
		m_enemy_game_engine_manage->ConnectOtherSide(m_game_engine_side);

		m_game_engine_manage->ConnectGameController(this);
		//TODO: нужен ли коммент
		// Нужно только ServerGameWindow
		// CreateVisuals (hInstance, parent);

		m_enemy_game_engine_manage->Ready();
		m_game_engine_manage->Ready();

		Show(SW_HIDE);

		// NetworkController устанавливает соединение с сервером
		// NetworkController получает параметры игры и зовет ConnectionEstablished --> IGameControllerNotify (clientwindow), сообщая ему параметры игры
		// gamewindow создает place-ships-window
		// после заполнения доски, полученная game-board запоминается, зовется create-visuals, game-window отображается
		// зовется IGameControllerSide::ControllerReady (написать), в ответ на что client-network-controller посылает start-game
	}
	catch (std::exception &e)
	{
		MessageBox(m_handle, AnsiToUnicode(e.what()).c_str(), __FUNCTIONW__, MB_ICONSTOP);
		return false;
	}
	catch (...)
	{
		MessageBox(m_handle, L"Something wrong...", __FUNCTIONW__, MB_ICONSTOP);
		return false;
	}

	return true;
}

// Throws InvalidPrivateParameterStateException
void BasicGameWindow::SetParameters(std::shared_ptr<GameBoard> my_gameboard, bool am_first)
{
	if (m_game_controller_side == 0)
	{
		throw InvalidPrivateParameterStateException(L"m_game_controller_side == 0");
	}

	m_my_gameboard = my_gameboard;
	m_am_first = am_first;

	m_enemy_gameboard.reset(new GameBoard());
	m_enemy_gameboard->CreateEnemyUnknown(m_my_gameboard->GetSize());

	m_game_controller_side->ISetGameParameters(m_my_gameboard, m_enemy_gameboard, m_am_first);
}

LRESULT BasicGameWindow::OnCommand(WPARAM wParam, LPARAM lParam)
{
	try
	{
		switch (LOWORD(wParam))
		{
		case IDCANCEL:
		{
			if (!m_is_cancel_locked)
			{
				Destroy();
			}

			return 0;
		}
		case IDC_SURRENDER:
		{
			m_game_controller_side->IResign();

			return 0;
		}
		case IDC_SHIP_SHOT:
		{
			DoUpdCurrentSelectedAbility(sa_shot);
			break;
		}
		case IDC_SHIP_MOVE:
		{
			DoUpdCurrentSelectedAbility(sa_move);
			break;
		}
		case IDC_SHIP_TORPEDO:
		{
			DoUpdCurrentSelectedAbility(sa_torpedo);
			break;
		}
		case IDC_SHIP_ROCKET:
		{
			DoUpdCurrentSelectedAbility(sa_rocket);
			break;
		}
		case IDC_SHIP_MAAC:
		{
			DoUpdCurrentSelectedAbility(sa_maac);
			break;
		}
		case IDC_SHIP_ILLUSION:
		{
			DoUpdCurrentSelectedAbility(sa_illusion);
			break;
		}
		case IDC_SHIP_EMP:
		{
			DoUpdCurrentSelectedAbility(sa_emp);
			break;
		}
		case IDC_SHIP_SCOUTING:
		{
			DoUpdCurrentSelectedAbility(sa_scouting);
			break;
		}
		case IDC_SHIP_FIGHTER_DRONE:
		{
			DoUpdCurrentSelectedAbility(sa_fighter_drone);
			break;
		}
		case IDC_SHIP_INTERCEPTOR_DRONE:
		{
			DoUpdCurrentSelectedAbility(sa_interceptor_drone);
			break;
		}
		case IDC_SHIP_PROSPECTOR_DRONE:
		{
			DoUpdCurrentSelectedAbility(sa_prospector_drone);
			break;
		}
		case IDC_SHIP_CARPET_BOMBING:
		{
			DoUpdCurrentSelectedAbility(sa_carpet_bombing);
			break;
		}
		case IDC_SHIP_BARRAGE_FIRE:
		{
			DoUpdCurrentSelectedAbility(sa_barrage_fire);
			break;
		}
		case IDC_SHIP_FULL_COMBAT_READINESS:
		{
			DoUpdCurrentSelectedAbility(sa_full_combat_readiness);
			break;
		}
		default:
		{
			return AbstractTopLvlWindow::OnCommand(wParam, lParam);
		}
		}
	}
	catch (std::exception &e)
	{
		MessageBox(m_handle, AnsiToUnicode(e.what()).c_str(), __FUNCTIONW__, MB_ICONSTOP);
	}
	catch (...)
	{
		MessageBox(m_handle, L"Something wrong...", __FUNCTIONW__, MB_ICONSTOP);
	}

	return 0;
}

LRESULT BasicGameWindow::OnDestroy(HWND hWnd)
{
	if (m_game_engine_manage.get())
	{
		m_game_engine_manage->Unready();

		m_game_engine_manage->ConnectGameController(0);
		m_game_engine_manage->ConnectOtherSide(0);
	}
	if (m_enemy_game_engine_manage.get())
	{
		m_enemy_game_engine_manage->Unready();

		m_enemy_game_engine_manage->ConnectOtherSide(0);
	}

	delete this;

	return 0;
}

struct PostGameStatus
{
	GameState state;
	std::wstring reason;
};

// Throws InvalidGameStateException
void BasicGameWindow::DoUpdGameState(GameState state, const std::wstring& reason)
{
	switch (state)
	{
	case gs_not_ready:
	{
		throw InvalidGameStateException(L"case gs_not_ready - updating game state when not ready");
	}
	case gs_waiting_for_start:
	{
		break;
	}
	case gs_my_move:
	{
		m_enemy_gbwidget->Unlock();

		EnableWindow(GetDlgItem(IDC_SURRENDER), TRUE);
		//TODO: проверить коммент
		//EnableWindow (GetDlgItem (IDCANCEL), FALSE);
		DisableCancel();

		break;
	}
	case gs_my_move_wait_for_response:
	{
		m_enemy_gbwidget->Lock();

		break;
	}
	case gs_enemy_move:
	{
		m_enemy_gbwidget->Lock();

		EnableWindow(GetDlgItem(IDC_SURRENDER), TRUE);
		//EnableWindow (GetDlgItem (IDCANCEL), FALSE);
		DisableCancel();

		break;
	}
	case gs_win:
	case gs_fail:
	case gs_canceled:
	{
		m_enemy_gbwidget->InvisibleLock();
		//Почему-то иногда не зовется при завершении игры с эмулятором

		//EnableWindow (GetDlgItem (IDCANCEL), TRUE);
		EnableWindow(GetDlgItem(IDC_SURRENDER), FALSE);

		if (state == gs_canceled)
		{
			EnableCancel();
		}

		break;
	}
	case gs_final:
	{
		//EnableWindow (GetDlgItem (IDCANCEL), TRUE);
		EnableCancel();

		break;
	}
	default:
	{
		throw InvalidGameStateException(L"switch (state) going to 'default' state");
	}
	}

	DoUpdEnemyGB();
	DoUpdMyGB();

	if (state != gs_final)
	{
		SendMessage(GetDlgItem(IDC_GAMESTATUS), WM_SETTEXT, 0, (LPARAM)reason.c_str());
	}
}

void BasicGameWindow::DoUpdEnemyGB()
{
	m_enemy_gbwidget->ForceUpdate();
}

void BasicGameWindow::DoUpdMyGB()
{
	m_my_gbwidget->ForceUpdate();
}

void BasicGameWindow::DoUpdCurrentSelectedAbility(ShipAbilityType type)
{
	m_enemy_controller->SetCurrentShipAbility(type);

	std::wostringstream output;
	output << L"Current selected ability: " << AbilityTypeToString(type);

	SendMessage(GetDlgItem(IDC_GAMESTATUS), WM_SETTEXT, 0, (LPARAM)output.str().c_str());
}

// Throws InvalidFunctionResultException
void BasicGameWindow::UpdGameState(GameState state, const std::wstring& reason)
{
	PostGameStatus* post_status = new PostGameStatus;

	post_status->state = state;
	post_status->reason = reason;

	BOOL post_result = ::PostMessage(m_handle, WM_GAMECONTROLLER_NOTIFY_GAMESTATE, 0, (LPARAM)post_status);

	if (post_result == 0)
	{
		throw InvalidFunctionResultException(L"post_result == 0 - invalid PostMessage result");
	}
}

// Throws InvalidFunctionResultException
void BasicGameWindow::UpdEnemyGB()
{
	BOOL post_result = ::PostMessage(m_handle, WM_GAMECONTROLLER_NOTIFY_ENEMYUPDATE, 0, 0);

	if (post_result == 0)
	{
		throw InvalidFunctionResultException(L"post_result == 0 - invalid PostMessage result");
	}
}

// Throws InvalidFunctionResultException
void BasicGameWindow::UpdMyGB()
{
	BOOL post_result = ::PostMessage(m_handle, WM_GAMECONTROLLER_NOTIFY_MYUPDATE, 0, 0);

	if (post_result == 0)
	{
		throw InvalidFunctionResultException(L"post_result == 0 - invalid PostMessage result");
	}
}

// Throws InvalidFunctionResultException
void BasicGameWindow::GameProcessEnded()
{
	BOOL post_result = ::PostMessage(m_handle, WM_NOTIFY_GAME_END, 0, 0);

	if (post_result == 0)
	{
		throw InvalidFunctionResultException(L"post_result == 0 - invalid PostMessage result");
	}
}

LRESULT BasicGameWindow::OnOtherMesage(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_GAMECONTROLLER_NOTIFY_ENEMYUPDATE:
	{
		DoUpdEnemyGB();

		return 0;
	}
	case WM_GAMECONTROLLER_NOTIFY_GAMESTATE:
	{
		if (lParam == 0)
		{
			throw InvalidArgumentException(L"lParam == 0 - invalid argument in WM_GAMECONTROLLER_NOTIFY_GAMESTATE case");
		}

		PostGameStatus* post_status = (PostGameStatus*)lParam;

		DoUpdGameState(post_status->state, post_status->reason);

		delete post_status;

		return 0;
	}
	case WM_GAMECONTROLLER_NOTIFY_MYUPDATE:
	{
		DoUpdMyGB();

		return 0;
	}
	case WM_WAIT_FOR_CONNECT_FINISHED:
	{
		m_wait_window->Destroy();

		return 0;
	}
	case WM_NOTIFY_GAME_END:
	{
		// Игра полностью завершается и никаких изменений после этого сообщения не происходит
		// соединение разорвано, игровых событий нет

		if (m_game_engine_manage == 0 ||
			m_enemy_game_engine_manage == 0)
		{
			throw InvalidPrivateParameterStateException(L"game engine null in WM_NOTIFY_GAME_END case");
		}

		m_enemy_gbwidget->ForceUpdate();

		m_game_engine_manage->Unready();
		m_enemy_game_engine_manage->Unready();

		DoUpdGameState(gs_final, L"");

		return 0;
	}
	default:
		return AbstractTopLvlWindow::OnOtherMesage(message, wParam, lParam);
	}
}

// Throws InvalidFunctionResultException
bool ServerGameWindow::Create(HINSTANCE hInstance, AbstractTopLvlWindow* parent, std::shared_ptr<GameBoard> my_gameboard, bool am_first, DWORD server_address, int server_port, std::wstring alias)
{
	INetworkServer* network_server_side = 0;

	m_enemy_game_engine_manage = CreateServerController(m_enemy_game_engine_side, network_server_side);

	if (network_server_side == 0)
	{
		throw InvalidFunctionResultException(L"network_server_side == 0 - server controller not created");
	}

	network_server_side->SetAddress(server_address);
	network_server_side->SetPort(server_port);
	network_server_side->SetAlias(alias);

	m_log_printer.reset(new Printer());

	m_game_engine_manage = CreateGameEngine(alias, true, m_log_printer, m_game_controller_side, m_game_engine_side);

	SetParameters(my_gameboard, am_first);

	if (!BasicGameWindow::Create(hInstance, parent))
	{
		return false;
	}
	m_enemy_game_engine_manage->ConnectNetworkNotify(this);

	CreateVisuals(hInstance, parent, L"Server");

	m_wait_window = new WaitingWindow();

	if (!m_wait_window->Create(hInst, this))
	{
		delete m_wait_window;
		return 0;
	}

	m_wait_window->Show(SW_SHOWNORMAL);
	Show(SW_HIDE);

	return true;
}

void ServerGameWindow::NotifyGameStarted()
{
	::PostMessage(m_handle, WM_WAIT_FOR_CONNECT_FINISHED, 0, 0);
	//TODO: коммент
	//m_wait_window->Destroy ();
}

void ServerGameWindow::PlayerConnected()
{
	m_wait_window->NotifyPlayerConnected(m_game_controller_side->IGetEnemyAlias());
}

// Throws InvalidFunctionResultException
bool ClientGameWindow::Create(HINSTANCE hInstance, AbstractTopLvlWindow* parent, DWORD server_address, int server_port, std::wstring alias)
{
	INetworkClient* network_client_side = 0;

	m_enemy_game_engine_manage = CreateClientController(m_enemy_game_engine_side, network_client_side);

	if (network_client_side == 0)
	{
		throw InvalidFunctionResultException(L"network_client_side == 0 - client controller not created");
	}

	network_client_side->SetServerAddress(server_address);
	network_client_side->SetServerPort(server_port);
	network_client_side->SetAlias(alias);

	m_log_printer.reset(new Printer());

	m_game_engine_manage = CreateGameEngine(alias, false, m_log_printer, m_game_controller_side, m_game_engine_side);

	m_enemy_game_engine_manage->ConnectNetworkNotify(this);

	//TODO: запустить крутилку

	return BasicGameWindow::Create(hInstance, parent);
}

void ClientGameWindow::AcceptGameBoard(std::shared_ptr<GameBoard> gb)
{
	m_my_gameboard = gb;
}

void ClientGameWindow::DoUpdGameState(GameState state, const std::wstring& reason)
{
	if (state == gs_waiting_for_start)
	{
		int gameboard_size = 0;
		WarShipPrototypeList prototypes;
		bool is_server_first;

		m_game_controller_side->IGetGameParameters(gameboard_size, is_server_first, prototypes);

		m_am_first = !is_server_first;

		PlaceShipsWindow* new_game = new PlaceShipsWindow();

		if (!new_game->Create(hInst, this, gameboard_size, false, prototypes))
		{
			delete new_game;
			return;
		}

		Show(SW_HIDE);
		new_game->Show(SW_SHOWNORMAL);
	}
	else
	{
		BasicGameWindow::DoUpdGameState(state, reason);
	}
}

LRESULT ClientGameWindow::OnOtherMesage(UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_SEAWIZARD_STEP_COMPLETE)
	{
		if (!m_my_gameboard.get() || !wParam)
		{
			Show(SW_SHOWNORMAL);
			return 0;
		}

		SetParameters(m_my_gameboard, m_am_first);

		CreateVisuals(hInst, m_parent, L"Client");

		m_game_controller_side->IStartGame();

		Show(SW_SHOWNORMAL);
	}
	else
	{
		return BasicGameWindow::OnOtherMesage(message, wParam, lParam);
	}

	return 0;
}

bool EmulatorGameWindow::Create(HINSTANCE hInstance, AbstractTopLvlWindow* parent, std::shared_ptr<GameBoard> my_gameboard, bool am_first, bool am_server, EmulatorDifficulty difficulty)
{
	m_log_printer.reset(new Printer());

	m_enemy_game_engine_manage = CreateEnemyEmulator(my_gameboard->GetPrototypeList(), my_gameboard->GetSize(), !am_first, !am_server, m_log_printer, difficulty, m_enemy_game_engine_side);

	m_game_engine_manage = CreateGameEngine(std::wstring(L"You"), am_server, m_log_printer, m_game_controller_side, m_game_engine_side);

	SetParameters(my_gameboard, am_first);

	m_enemy_game_engine_manage->ConnectNetworkNotify(this);

	if (!BasicGameWindow::Create(hInstance, parent))
	{
		return false;
	}

	CreateVisuals(hInstance, parent, L"Emulator");

	return true;
}
