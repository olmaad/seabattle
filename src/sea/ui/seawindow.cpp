#include "stdafx.h"
#include "seawindow.h"
#include <exception>


//// AbstractTopLvlWindow ////////////////////////////////////////////////////////////////////////////////////////////////////


//// 

LRESULT AbstractTopLvlWindow::OnCreate ()
{
	return 0;
}


//// 

LRESULT AbstractTopLvlWindow::OnDestroy (HWND hWnd)
{
	_ASSERT (!m_handle);

	delete this;

	return 0;
}


////

LRESULT AbstractTopLvlWindow::OnPaint ()
{
	return 0;
}


////

LRESULT AbstractTopLvlWindow::OnCommand (WPARAM wParam, LPARAM lParam)
{
	return 0;
}

LRESULT AbstractTopLvlWindow::OnOtherMesage (UINT message, WPARAM wParam, LPARAM lParam)
{
	return (LRESULT) FALSE;
}


////

int AbstractTopLvlWindow::EnterInteger (UINT box_id, UINT feedback_id, int low_limit, int max_limit)
{
	try
	{
		int box_integer = GetEditboxInteger (box_id);

		if (box_integer < low_limit || box_integer > max_limit)
		{
			std::ostringstream except_text;
			except_text << "Enter number from " << low_limit << " to " << max_limit;

			throw std::exception (except_text.str ().c_str ());
		}

		SendMessage (GetDlgItem (feedback_id), WM_SETTEXT, 0, (LPARAM) L"");

		return box_integer;
	}
	catch (std::exception& e)
	{
		std::wstring except_wtext = AnsiToUnicode (std::string (e.what ()));

		SendMessage (GetDlgItem (feedback_id), WM_SETTEXT, 0, (LPARAM) except_wtext.c_str ());

		throw;
	}
}


DWORD AbstractTopLvlWindow::EnterIp4Address (UINT box_id, UINT feedback_id)
{
	try
	{
		std::wstring ip4_address_text = GetEditboxText (box_id);

		unsigned int a, b, c, d;

		int scan_result = swscanf (ip4_address_text.c_str (), L"%d.%d.%d.%d", &a, &b, &c, &d);

		if (4 != scan_result || 1 > a || 254 < a || 0 > b || 254 < b || 0 > c || 254 < c || 1 > d || 254 < d)
		{
			throw std::exception ("Enter valid ip4 address");
		}

		SendMessage (GetDlgItem (feedback_id), WM_SETTEXT, 0, (LPARAM) L"");

		DWORD ip4_address = (d << 24) | (c << 16) | (b << 8) | (a);

		return ip4_address;
	}
	catch (std::exception& e)
	{
		std::wstring except_wtext = AnsiToUnicode (std::string (e.what ()));

		SendMessage (GetDlgItem (feedback_id), WM_SETTEXT, 0, (LPARAM) except_wtext.c_str ());

		throw;
	}
}



////

AbstractTopLvlWindow::~AbstractTopLvlWindow ()
{
	_ASSERT (0 == m_handle);
}


////

AbstractTopLvlWindow::AbstractTopLvlWindow ():m_handle (0)
{
}


//// Get window hwnd

HWND AbstractTopLvlWindow::GetHandle () const
{
	return m_handle;
}


//// Create window (dialog)

bool AbstractTopLvlWindow::Create (HINSTANCE hInstance, const LPCTSTR lpTemplateName, HWND hWndParent)
{
	_ASSERT (0 == m_handle);

	if (0 != m_handle)
	{
		return false;
	}

	m_handle = ::CreateDialogParam(hInstance, lpTemplateName, 0, DlgProc, (LPARAM)this);

	return 0 != m_handle;
}


//// Show window

bool AbstractTopLvlWindow::Show (int nCmdShow)
{
	ShowWindow(m_handle, nCmdShow);
	UpdateWindow(m_handle);

	return true;
}


//// Kill window

void AbstractTopLvlWindow::Destroy ()
{
	if (0 == m_handle)
	{
		delete this;

		return;
	}

	::DestroyWindow (m_handle);
}


//// Message processor

INT_PTR CALLBACK AbstractTopLvlWindow::DlgProc (HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);

	if (message == WM_INITDIALOG)
	{
		if (!lParam)
		{
			_ASSERT (lParam);

			return (INT_PTR)FALSE;
		}

		::SetWindowLongPtr (hDlg, GWL_USERDATA, (LONG_PTR) lParam);

		return (INT_PTR) ((AbstractTopLvlWindow*)lParam)->OnCreate ();
	}

	AbstractTopLvlWindow* that = (AbstractTopLvlWindow*) ::GetWindowLongPtr (hDlg, GWL_USERDATA);

	switch (message)
	{
	case WM_COMMAND:
		return (INT_PTR) that->OnCommand (wParam, lParam);

	case WM_PAINT:
		return (INT_PTR) that->OnPaint ();

	case WM_NCDESTROY:
		{
			_ASSERT (that->m_handle == hDlg);

			that->m_handle = 0;
			INT_PTR destroy_return = (INT_PTR) that->OnDestroy (hDlg);

			return destroy_return;
		}

	default:
		if (that != 0)
		{
			return (INT_PTR) that->OnOtherMesage (message, wParam, lParam);
		}
	}

	return (INT_PTR) FALSE;
}


//// Get text from edibox with box_id (return std::wstring)

std::wstring AbstractTopLvlWindow::GetEditboxText (UINT box_id)
{
	std::wstring edit_box_text;
	HWND box_handle = GetDlgItem (box_id);

	LRESULT edit_box_text_length = SendMessage (box_handle, WM_GETTEXTLENGTH, 0, 0);

	edit_box_text.reserve (edit_box_text_length +1);
	edit_box_text.resize (edit_box_text_length);

	SendMessage (box_handle, WM_GETTEXT, (WPARAM) edit_box_text.size () +1, (LPARAM) edit_box_text.c_str ());

	return edit_box_text;
}


//// 

int AbstractTopLvlWindow::GetEditboxInteger (UINT box_id)
{
	std::wstring edit_box_text;
	HWND box_handle = GetDlgItem (box_id);

	LRESULT edit_box_text_length = SendMessage (box_handle, WM_GETTEXTLENGTH, 0, 0);

	edit_box_text.resize (edit_box_text_length +1);

	SendMessage (box_handle, WM_GETTEXT, (WPARAM) edit_box_text.size (), (LPARAM) edit_box_text.c_str ());

	int box_integer = 0;
	int scan_result = swscanf (edit_box_text.c_str (), L"%d", &box_integer);

	if (!scan_result)
	{
		THROW_TEXT ("Entered invalid number !");
	}

	return box_integer;
}


//// Gedlgitem without handle

HWND AbstractTopLvlWindow::GetDlgItem (int item_id) const
{
	return ::GetDlgItem (m_handle, item_id);
}


//// AbstractChildWindow ////////////////////////////////////////////////////////////////////////////////////////////////////

int AbstractChildWindow::class_init_counter = 0;
ATOM AbstractChildWindow::class_atom = 0;
const wchar_t* AbstractChildWindow::class_name = L"AbstractChildWindow";


//// Constructor

AbstractChildWindow::AbstractChildWindow (HINSTANCE hInstance, HWND parent_handle):m_handle (0), m_parent_handle (parent_handle)
{
	_ASSERT (0 != m_parent_handle);

	if (!class_init_counter)
	{
		RegisterClass (hInstance);

		class_init_counter = class_init_counter +1;
	}
}


//// Destructor

AbstractChildWindow::~AbstractChildWindow ()
{
	_ASSERT (0 == m_handle);
}


////

void AbstractChildWindow::ForceUpdate ()
{
	RECT rect;

	GetClientRect (m_handle, &rect);

	InvalidateRect (m_handle, &rect, TRUE);
}


//// Call global DestroyWindow

void AbstractChildWindow::Destroy ()
{
	if (0 == m_handle)
	{
		return;
	}

	::DestroyWindow (m_handle);
}


//// Get window hwnd

HWND AbstractChildWindow::GetHandle () const
{
	return m_handle;
}


//// Get window (!) rect

RECT AbstractChildWindow::GetRect () const
{
	RECT ReturnRect = {0};

	if (0 != m_handle)
	{
		if (!GetWindowRect (m_handle, &ReturnRect))
		{
			THROW_TEXT ("Can't get window RECT !");
		}
	}

	return ReturnRect;
}


//// message processor

LRESULT CALLBACK AbstractChildWindow::WndProc (HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_NCCREATE)
	{
		if (!lParam)
		{
			_ASSERT (lParam);

			return (LRESULT)FALSE;
		}

		CREATESTRUCT* create_struct = (CREATESTRUCT*) lParam;

		::SetWindowLongPtr (hWnd, GWL_USERDATA, (LONG_PTR) create_struct->lpCreateParams);

		return (LRESULT) ((AbstractChildWindow*)create_struct->lpCreateParams)->OnCreate ();
	}

	AbstractChildWindow* that = (AbstractChildWindow*) ::GetWindowLongPtr (hWnd, GWL_USERDATA);

	if (!that)
	{
		return DefWindowProc (hWnd, message, wParam, lParam);
	}

	switch (message)
	{
	case WM_COMMAND:
		return (LRESULT) that->OnCommand (wParam, lParam);

	case WM_LBUTTONDOWN:
		return (LRESULT) that->OnLeftMouseDown (lParam);

	case WM_LBUTTONUP:
		return (LRESULT) that->OnLeftMouseUp ();

	case WM_RBUTTONDOWN:
		return (LRESULT) that->OnRightMouseDown (lParam);

	case WM_RBUTTONUP:
		return (LRESULT) that->OnRightMouseUp (lParam);

	case WM_MOUSEMOVE:
		return (LRESULT) that->OnMouseMove (lParam);

	case WM_TIMER:
		return (LRESULT) that->OnTimer ();

	case WM_PAINT:
		return (LRESULT) that->OnPaint ();

	case WM_NCDESTROY:
		{
			_ASSERT (that->m_handle == hWnd);

			that->m_handle = 0;
			LRESULT destroy_return = (LRESULT) that->OnDestroy (hWnd);

			return destroy_return;
		}
	default:
		return (LRESULT) that->OnOtherMesage (message, wParam, lParam);
		//return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}


//// 

LRESULT AbstractChildWindow::OnLeftMouseDown (LPARAM lParam)
{
	return 0;
}


////

LRESULT AbstractChildWindow::OnLeftMouseUp ()
{
	return 0;
}


LRESULT AbstractChildWindow::OnOtherMesage (UINT message, WPARAM wParam, LPARAM lParam)
{
	return DefWindowProc(m_handle, message, wParam, lParam);
}


//// 

LRESULT AbstractChildWindow::OnRightMouseDown (LPARAM lParam)
{
	return 0;
}


////

LRESULT AbstractChildWindow::OnRightMouseUp (LPARAM lParam)
{
	return 0;
}


////

LRESULT AbstractChildWindow::OnMouseMove (LPARAM lParam)
{
	return 0;
}


////

LRESULT AbstractChildWindow::OnTimer ()
{
	return 0;
}


//// Register window class (for Win)

void AbstractChildWindow::RegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_PARENTDC;
	wcex.lpfnWndProc	= AbstractChildWindow::WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= 0;
	wcex.hCursor		= 0;
	wcex.hbrBackground	= 0; //  WM_ERASEBKGND 
	wcex.lpszMenuName	= 0;
	wcex.lpszClassName	= class_name;
	wcex.hIconSm		= 0;

	class_atom = ::RegisterClassEx(&wcex);
}


//// Create child window

void AbstractChildWindow::Create (DWORD dwExStyle, DWORD dwStyle, LPCTSTR lpWindowName, int x, int y, int nWidth, int nHeight, HINSTANCE hInstance)
{
	_ASSERT (0 == m_handle);

	HWND new_window = ::CreateWindowEx (dwExStyle, class_name, lpWindowName, dwStyle, x, y, nWidth, nHeight, m_parent_handle, 0, hInstance, this);

	if (!new_window)
	{
		throw std::exception ("Can't create window !");
	}

	m_handle = new_window;

	SetTimer (m_handle, 0, 200, 0);
}


////

LRESULT AbstractChildWindow::OnCreate ()
{
	return TRUE;
}


////

LRESULT AbstractChildWindow::OnDestroy (HWND hWnd)
{
	return 0;
}


////

LRESULT AbstractChildWindow::OnPaint ()
{
	return 0;
}


////

LRESULT AbstractChildWindow::OnCommand (WPARAM wParam, LPARAM lParam)
{
	return 0;
}


//// SolidBrush ////////////////////////////////////////////////////////////////////////////////////////////////////


//// Constructor

SolidBrush::SolidBrush (const COLORREF& colors)
{
	m_brush = CreateSolidBrush (colors);

	if (!m_brush)
	{
		THROW_TEXT ("Can't create brush !");
	}
}


//// Destructor

SolidBrush::~SolidBrush ()
{
	if (m_brush)
	{
		DeleteObject (m_brush);
	}
}


//// ColorPen ////////////////////////////////////////////////////////////////////////////////////////////////////


//// Constructor

ColorPen::ColorPen (const COLORREF& colors, int width)
{
	m_pen = CreatePen (PS_SOLID, width, colors);

	if (!m_pen)
	{
		THROW_TEXT ("Can't create pen !");
	}
}


//// Destructor

ColorPen::~ColorPen ()
{
	if (m_pen)
	{
		DeleteObject (m_pen);
	}
}


//// VectorFont ////////////////////////////////////////////////////////////////////////////////////////////////////


//// Constructor

VectorFont::VectorFont () : m_font (0)
{
}


//// Destructor

VectorFont::~VectorFont ()
{
	if (m_font)
	{
		DeleteObject (m_font);
	}
}


//// Create/recreate font 

void VectorFont::Create (int f_height, int f_weight, DWORD pitch_and_family, const wchar_t* face_name)
{
	HFONT created_font = ::CreateFont (f_height, 0, 0, 0, f_weight, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_OUTLINE_PRECIS, CLIP_DEFAULT_PRECIS, ANTIALIASED_QUALITY, pitch_and_family, face_name);

	if (0 == created_font)
	{
		THROW_TEXT ("Can't create font !")
	}

	if (0 != m_font)
	{
		DeleteObject (m_font);
	}
	m_font = created_font;
}


//// ColorBitmap ////////////////////////////////////////////////////////////////////////////////////////////////////


//// Constructor

ColorBitmap::ColorBitmap (SIZE size) : m_size (size)
{
	HDC screen_dc = GetDC (0);

	if (!screen_dc)
	{
		THROW_TEXT ("Can't get screen dc !");
	}

	m_bitmap = CreateCompatibleBitmap (screen_dc, size.cx, size.cy);

	ReleaseDC (0, screen_dc);

	if (!m_bitmap)
	{
		THROW_TEXT ("Can't create bitmap !");
	}

	BITMAP bitmap_info;

	GetObject (m_bitmap, sizeof (bitmap_info), &bitmap_info);

	if (32 != bitmap_info.bmBitsPixel)
	{
		DeleteObject (m_bitmap);

		THROW_TEXT ("Invalid bitmap palette !");
	}
}


////

ColorBitmap::ColorBitmap (HINSTANCE  hInstance, LPCTSTR res_name)
{
	m_bitmap = LoadBitmap (hInstance, res_name);

	if (!m_bitmap)
	{
		THROW_TEXT ("Can't create bitmap !");
	}

	BITMAP bitmap_info;

	GetObject (m_bitmap, sizeof (bitmap_info), &bitmap_info);

	if (32 != bitmap_info.bmBitsPixel)
	{
		DeleteObject (m_bitmap);

		THROW_TEXT ("Invalid bitmap palette !");
	}

	m_size.cx = bitmap_info.bmWidth;
	m_size.cy = bitmap_info.bmHeight;
}


//// Destructor

ColorBitmap::~ColorBitmap ()
{
	DeleteObject (m_bitmap);
}


//// DrawSurface ////////////////////////////////////////////////////////////////////////////////////////////////////


//// Constructor

DrawSurface::DrawSurface ()
{
	HDC screen_dc = GetDC (0);

	if (!screen_dc)
	{
		THROW_TEXT ("Can't get screen DC !");
	}

	m_hdc = CreateCompatibleDC (screen_dc);

	ReleaseDC (0, screen_dc);

	if (!m_hdc)
	{
		THROW_TEXT ("Can't get DC !");
	}
}


////

DrawSurface::~DrawSurface ()
{
	DeleteDC (m_hdc);
}















