#pragma once

#include "seawindow.h"



class WaitingWindow : public AbstractTopLvlWindow
{
public:
	WaitingWindow ();
	~WaitingWindow ();

	bool Create (HINSTANCE hInstance, AbstractTopLvlWindow* parent);
	void NotifyPlayerConnected (std::wstring alias);

	static const LPCTSTR lpTemplateName;

private:
	AbstractTopLvlWindow* m_parent;

protected:
	virtual LRESULT OnCommand (WPARAM wParam, LPARAM lParam);		//Обработка WM_COMMAND
	virtual LRESULT OnDestroy (HWND hWnd);		//Удаление объекта
	
};
