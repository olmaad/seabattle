#include "stdafx.h"
#include "resources/resource.h"
#include "UI_Main.h"
#include "UI_NewGame.h"
#include "UI_JoinGame.h"
#include "UI_PlaceShips.h"
#include "UI_PlayWithEmulator.h"

const LPCTSTR MainWindow::lpTemplateName = MAKEINTRESOURCE(IDD_MAINBOX);


bool MainWindow::Create (HINSTANCE hInstance, HWND hWndParent)
{
	return AbstractTopLvlWindow::Create (hInstance, lpTemplateName, hWndParent);
}

LRESULT MainWindow::OnCommand (WPARAM wParam, LPARAM lParam)
{
	switch (LOWORD(wParam))
	{
	case IDCANCEL:
		::PostQuitMessage (0);
		Destroy ();
		return 0;
	case IDC_CREATENEWGAME:
		{
			NewGameWindow* new_game = new NewGameWindow();
			if (!new_game->Create (hInst, this))
			{
				delete new_game;
			}
			Show (SW_HIDE);
			new_game->Show (SW_SHOWNORMAL);

			return 0;
		}
	case IDC_JOINGAME:
		{
			JoinGameWindow* join_game = new JoinGameWindow ();

			if (!join_game->Create (hInst, this))
			{
				delete join_game;
			}

			Show (SW_HIDE);
			join_game->Show (SW_SHOWNORMAL);
			
			return 0;
		}
	case IDC_EMULATOR:
		{
			PlayWithEmulatorWindow* emulator_game = new PlayWithEmulatorWindow ();

			if (!emulator_game->Create (hInst, this))
			{
				delete emulator_game;
			}

			Show (SW_HIDE);
			emulator_game->Show (SW_SHOWNORMAL);

			return 0;
		}
	default:
		return AbstractTopLvlWindow::OnCommand (wParam, lParam);
	}
}

//////////////////////////////////////////////
//LRESULT MainWindow::OnPaint ()
//{
//	PAINTSTRUCT ps;
//	HDC hdc;
//
//	hdc = ::BeginPaint(m_handle, &ps);
//	::EndPaint(m_handle, &ps);
//	return 0;
//}
//////////////////////////////////////////////
