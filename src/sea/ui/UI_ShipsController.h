#pragma once

#include <libGameEngine/GameBoard.h>
#include <libGameEngine/GameEngine.h>

#include <list>
#include <algorithm>

class ControllerException
{
public:
	ControllerException ();
	~ControllerException ();
};

class CExc_TwoKeysPressed : ControllerException
{
};

class CExc_DisallowedShip : ControllerException
{
};


enum EditFlag
{
	ed_del = 0,
	ed_add
};


class EditField
{
public:
	EditField (AbsBoardPos& create_pos, EditFlag edit_flag);
	~EditField ();

	EditFlag GetFlag ();
	WarShipPrototypePtr& GetPrototype ();
	const AbsBoardPos& GetPos ();
	void AddPoint (AbsBoardPos& point_pos);
	const RelBoardPosList& GetPoints ();
	const AbsBoardPos& GetLast ();

private:
	EditFlag m_flag;
	AbsBoardPos m_pos;
	AbsBoardPos m_last_added;
	WarShipPrototypePtr m_prototype;
};


class NewWarshipField
{
public:
	NewWarshipField (AbsBoardPos src_pos, WarShipPrototypePtr src_prototype);
	~NewWarshipField ();

	void SetNewPosition (AbsBoardPos pos)
	{
		m_pos = pos;
	}
	const AbsBoardPos& GetNewPos ()
	{
		return m_pos;
	}
	WarShipPrototypePtr& GetPrototype ()
	{
		return m_prototype;
	}
	const AbsBoardPos& GetPos ()
	{
		return m_pos;
	}
	const RelBoardPosList& GetPoints ()
	{
		return m_prototype->GetPoints ();
	}

private:
	AbsBoardPos m_pos;
	WarShipPrototypePtr m_prototype;
};


typedef std::unique_ptr<NewWarshipField> NewWarshipFieldPtr;
typedef std::unique_ptr<EditField> EditFieldPtr;

class PrototypeWidget;

class IShipController
{
public:
	virtual ~IShipController() {}

	virtual void OnClickLeft(AbsBoardPos cur_pos) = 0;
	virtual void OnClickLeft(int prototype_number) = 0;
	virtual void OnClickRight(AbsBoardPos cur_pos) = 0;

	virtual void OnSelectChange(AbsBoardPos cur_pos) = 0;

	virtual void OnReleaseLeft() = 0;
	virtual void OnReleaseRight(AbsBoardPos& cur_pos) = 0;

	virtual void BindPrototypeWidget(PrototypeWidget* src_widget) = 0;
	virtual void UnBindPrototypeWidget(PrototypeWidget* src_widget) = 0;

	virtual void SetCurrentShipAbility(ShipAbilityType type) = 0;

	// Notifycation comes AFTER add/del

	virtual void NotifyPrototypeAdded(const WarShipPrototype* prototype) = 0;
	virtual void NotifyPrototypeDeleted(const WarShipPrototype* prototype) = 0;
	virtual void NotifyWarshipCountChanged() = 0;

	virtual bool IsHost() = 0;

	virtual EditField* GetEdit() = 0; // K()('Гbl|\b
	virtual NewWarshipField* GetNew() = 0;

};

std::unique_ptr<IShipController> CreateHostShipsController (GameBoard& game_board);
std::unique_ptr<IShipController> CreateJoinShipsController (GameBoard& game_board);
std::unique_ptr<IShipController> CreateFictitiousShipsController (GameBoard& game_board);
std::unique_ptr<IShipController> CreateBattleShipsController (GameBoard& game_board, IGameControllerSide* game_engine);
