// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#pragma warning (disable:4996)

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <crtdbg.h>
#include <memory>
#include <string>
#include <sstream>
#include <iomanip>

#include <libSystem/SeaExceptions.h>

extern HINSTANCE hInst;

#define TRACE_TEXT(_text)																\
{																						\
	std::ostringstream _trace_text;														\
	_trace_text << std::dec << __LINE__ << ":" << __FUNCTION__ << ": " << (_text) << "\n";	\
	OutputDebugStringA (_trace_text.str ().c_str ());									\
}

#define TRACE_TEXT_CONTEXT(_text, _context)														\
{																								\
	std::ostringstream trace_text;																\
	trace_text << std::dec << __LINE__ << ":" << __FUNCTION__ << ":" << "0x" << std::hex << std::setw(sizeof(void*)) << _context << ": " << _text << "\n";			\
	OutputDebugStringA (trace_text.str ().c_str ());											\
}

#define ALIGN_UP(_what, _alignment) (((_what) + (_alignment) -1) & (-(_alignment)))


inline std::wstring AnsiToUnicode (const std::string& src_string, std::locale loc  = std::locale())
{
	std::wstring out_string (src_string.length(), 0);
	std::string::const_iterator i = src_string.begin(), ie = src_string.end();
	std::wstring::iterator j = out_string.begin();

	for( ; i!=ie; ++i, ++j )
		*j = std::use_facet< std::ctype< wchar_t > > ( loc ).widen( *i );
	
	return out_string;
}

enum
{
	WM_GAMECONTROLLER_NOTIFY_GAMESTATE = WM_USER +1,
	WM_GAMECONTROLLER_NOTIFY_ENEMYUPDATE,
	WM_GAMECONTROLLER_NOTIFY_MYUPDATE,
	WM_SEAWIZARD_STEP_COMPLETE,
	WM_WAIT_FOR_CONNECT_FINISHED,
	WM_NOTIFY_GAME_END
};
