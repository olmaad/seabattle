#include "PacketMaker.h"

size_t ProjectNPHandshake(const std::wstring& alias)
{
	if (1 > alias.size() || npl_max_alias_length <= alias.size())
	{
		THROW_TEXT("Invalid alias !");
	}

	size_t return_val = sizeof(NPHandshake);

	if (return_val > npl_max_packet_size)
	{
		THROW_TEXT("Anallowed packet size !");
	}

	return return_val;
}


void FillNPHandshake(void* buffer, nsqn_t sequence_number, const std::wstring& alias)
{
	_ASSERT(buffer);

	size_t np_size = ProjectNPHandshake(alias);
	NPHandshake* np_buffer = (NPHandshake*)buffer;

	memset(buffer, 0, np_size);

	np_buffer->PacketType = net_handshake;
	np_buffer->PacketSize = np_size;
	np_buffer->SequenceNumber = sequence_number;
	np_buffer->protocol_version = protocol_version_current;
	np_buffer->seabattle_app_guid = seabattle_application_guid;
	memcpy(np_buffer->alias, alias.c_str(), (alias.size() + 1) * sizeof(wchar_t));
}

size_t ProjectNPHandshakeResponse(const std::wstring& alias, int gameboard_size, bool is_server_move_first, const WarShipPrototypeList& prototypes)
{
	if (!alias.size() || npl_max_alias_length <= alias.size())
	{
		THROW_TEXT("Invalid alias length !");
	}
	if (!prototypes.size())
	{
		THROW_TEXT("No prototypes !");
	}

	for (WarShipPrototypeList::const_iterator counter = prototypes.cbegin(); counter != prototypes.cend(); counter++)
	{
		if ((*counter)->GetPoints().size() > npl_max_ship_length)
		{
			THROW_TEXT("Too big prototype !");
		}
	}

	size_t return_val = sizeof(NPHandshakeResponse) + (prototypes.size() - 1) * sizeof(PrototypeDescription);

	if (return_val > npl_max_packet_size)
	{
		THROW_TEXT("Anallowed packet size !");
	}

	return return_val;
}


void FillNPHandshakeResponse(void* buffer, nsqn_t sequence_number, const std::wstring& alias, int gameboard_size, bool is_server_move_first, const WarShipPrototypeList& prototypes)
{
	_ASSERT(buffer);

	size_t np_size = ProjectNPHandshakeResponse(alias, gameboard_size, is_server_move_first, prototypes);
	NPHandshakeResponse* np_buffer = (NPHandshakeResponse*)buffer;

	memset(buffer, 0, np_size);

	np_buffer->PacketType = net_handshake_response;
	np_buffer->PacketSize = np_size;
	np_buffer->SequenceNumber = sequence_number;
	np_buffer->protocol_version = protocol_version_current;
	np_buffer->seabattle_app_guid = seabattle_application_guid;
	memcpy(np_buffer->alias, alias.c_str(), (alias.size() + 1) * sizeof(wchar_t));
	np_buffer->gameboard_size = gameboard_size;
	np_buffer->is_server_move_first = (unsigned __int8)is_server_move_first;
	np_buffer->prototype_count = prototypes.size();

	PrototypeDescription* np_prototype_it = np_buffer->prototypes;

	for (WarShipPrototypeList::const_iterator counter = prototypes.cbegin(); counter != prototypes.cend(); counter++, np_prototype_it = np_prototype_it + 1)
	{
		const RelBoardPosList& prototypes_points = (*counter)->GetPoints();

		np_prototype_it->point_amount = prototypes_points.size();
		np_prototype_it->limit = (*counter)->GetLimit();

		BoardPoint* np_point_it = np_prototype_it->points;

		for (RelBoardPosList::const_iterator p_counter = prototypes_points.cbegin(); p_counter != prototypes_points.cend(); p_counter++, np_point_it = np_point_it + 1)
		{
			np_point_it->x = p_counter->x;
			np_point_it->y = p_counter->y;
		}
	}
}


size_t ProjectNPClientStartGame()
{
	size_t return_val = sizeof(NPClientStartGame);

	if (return_val > npl_max_packet_size)
	{
		THROW_TEXT("Anallowed packet size !");
	}

	return return_val;
}


void FillNPClientStartGame(void* buffer, nsqn_t sequence_number, ngen_t event_number)
{
	_ASSERT(buffer);

	size_t np_size = ProjectNPClientStartGame();
	NPClientStartGame* np_buffer = (NPClientStartGame*)buffer;

	memset(buffer, 0, np_size);

	np_buffer->PacketType = net_client_start_game;
	np_buffer->PacketSize = np_size;
	np_buffer->SequenceNumber = sequence_number;
	np_buffer->event_number = event_number;
}


size_t ProjectNPFire(const AbsBoardPos& fire_pos)
{
	size_t return_val = sizeof(NPFire);

	if (return_val > npl_max_packet_size)
	{
		THROW_TEXT("Anallowed packet size !");
	}

	return return_val;
}


void FillNPFire(void* buffer, nsqn_t sequence_number, const AbsBoardPos& fire_pos, ngen_t event_number)
{
	_ASSERT(buffer);

	size_t np_size = ProjectNPFire(fire_pos);
	NPFire* np_buffer = (NPFire*)buffer;

	memset(buffer, 0, np_size);

	np_buffer->PacketType = net_fire;
	np_buffer->PacketSize = np_size;
	np_buffer->SequenceNumber = sequence_number;
	np_buffer->fire_position.x = fire_pos.x;
	np_buffer->fire_position.y = fire_pos.y;
	np_buffer->event_number = event_number;
}


size_t ProjectNPFireResponse(int request_sequence_number, FireResult fire_result)
{
	size_t return_val = sizeof(NPFireResponse);

	if (return_val > npl_max_packet_size)
	{
		THROW_TEXT("Anallowed packet size !");
	}

	return return_val;
}


void FillNPFireResponse(void* buffer, nsqn_t sequence_number, ngen_t request_sequence_number, FireResult fire_result, ngen_t event_number)
{
	_ASSERT(buffer);

	size_t np_size = ProjectNPFireResponse(request_sequence_number, fire_result);
	NPFireResponse* np_buffer = (NPFireResponse*)buffer;

	memset(buffer, 0, np_size);

	np_buffer->PacketType = net_fire_response;
	np_buffer->PacketSize = np_size;
	np_buffer->SequenceNumber = sequence_number;
	np_buffer->request_sequence_number = request_sequence_number;
	np_buffer->fire_result = fire_result;
	np_buffer->event_number = event_number;
}

size_t ProjectNPUseAbility(const ShipAbilityPtr& ability)
{
	size_t return_val = sizeof(NPUseAbility);

	if (return_val > npl_max_packet_size)
	{
		THROW_TEXT("Anallowed packet size !");
	}

	return return_val;
}

void FillNPUseAbility(void* buffer, nsqn_t sequence_number, const ShipAbilityPtr& ability, ngen_t event_number)
{
	_ASSERT(buffer);
	_ASSERT(ability);

	size_t np_size = ProjectNPUseAbility(ability);
	NPUseAbility* np_buffer = (NPUseAbility*)buffer;

	np_buffer->PacketType = net_use_ability;
	np_buffer->PacketSize = np_size;
	np_buffer->SequenceNumber = sequence_number;
	np_buffer->ability_type = ability->m_type;

	switch (ability->m_type)
	{
	case sa_shot:
	{
		auto ability_typed = std::static_pointer_cast<ShotShipAbility>(ability);

		np_buffer->shot_ability.pos.x = ability_typed->m_pos.x;
		np_buffer->shot_ability.pos.y = ability_typed->m_pos.y;

		break;
	}
	default:
	{
		THROW_TEXT("Unsupported ability type !");
		break;
	}
	}

	np_buffer->event_number = event_number;
}

size_t ProjectNPGameUpdate(const GameUpdatePtr& update)
{
	size_t return_val = sizeof(NPGameUpdate);

	if (return_val > npl_max_packet_size)
	{
		THROW_TEXT("Anallowed packet size !");
	}

	return return_val;
}

void FillNPGameUpdate(void* buffer, nsqn_t sequence_number, const GameUpdatePtr& update, ngen_t event_number)
{
	_ASSERT(buffer);
	_ASSERT(update);

	size_t np_size = ProjectNPGameUpdate(update);
	NPGameUpdate* np_buffer = (NPGameUpdate*)buffer;

	np_buffer->PacketType = net_game_update;
	np_buffer->PacketSize = np_size;
	np_buffer->SequenceNumber = sequence_number;
	np_buffer->update_type = update->m_type;

	switch (update->m_type)
	{
	case gu_shot_result:
	{
		auto update_typed = std::static_pointer_cast<ShotResultUpdate>(update);

		np_buffer->shot_result.result = update_typed->m_result;
		np_buffer->shot_result.pos.x = update_typed->m_pos.x;
		np_buffer->shot_result.pos.y = update_typed->m_pos.y;

		break;
	}
	default:
	{
		THROW_TEXT("Unsupported update type !");
		break;
	}
	}
}

size_t ProjectNPEndGame(EndGameState end_gsme_state, const GameBoard& game_board)
{
	size_t return_val = ALIGN_UP(sizeof(NPEndGame) + (game_board.GetSize() * game_board.GetSize() - 1), 4);

	if (return_val > npl_max_packet_size)
	{
		THROW_TEXT("Anallowed packet size !");
	}

	return return_val;
}

void FillNPEndGame(void* buffer, nsqn_t sequence_number, EndGameState end_gsme_state, const GameBoard& game_board, ngen_t event_number)
{
	_ASSERT(buffer);

	size_t np_size = ProjectNPEndGame(end_gsme_state, game_board);
	NPEndGame* np_buffer = (NPEndGame*)buffer;

	memset(buffer, 0, np_size);

	np_buffer->PacketType = net_end_game;
	np_buffer->PacketSize = np_size;
	np_buffer->SequenceNumber = sequence_number;
	np_buffer->end_game_state = end_gsme_state;
	np_buffer->event_number = event_number;

	int gb_size = game_board.GetSize();

	np_buffer->gameboard_size = gb_size;

	char* np_bitmap_it = np_buffer->bitmap;

	for (int x = 0; x < gb_size; x = x + 1)
	{
		for (int y = 0; y < gb_size; y = y + 1)
		{
			CellType ct = game_board.Cell(AbsBoardPos(x, y)).cell_type;

			*np_bitmap_it = (char)!(ct == ct_empty || ct == ct_miss);

			np_bitmap_it = np_bitmap_it + 1;
		}
	}
}

size_t ProjectNPPing()
{
	size_t return_val = sizeof(NPPing);

	if (return_val > npl_max_packet_size)
	{
		THROW_TEXT("Anallowed packet size !");
	}

	return return_val;
}

void FillNPPing(void* buffer, nsqn_t sequence_number)
{
	_ASSERT(buffer);

	size_t np_size = ProjectNPPing();
	NPPing* np_buffer = (NPPing*)buffer;

	memset(buffer, 0, np_size);

	np_buffer->PacketType = net_ping;
	np_buffer->PacketSize = np_size;
	np_buffer->SequenceNumber = sequence_number;
}

size_t ProjectNPPingResponse()
{
	size_t return_val = sizeof(NPPingResponse);

	if (return_val > npl_max_packet_size)
	{
		THROW_TEXT("Anallowed packet size !");
	}

	return return_val;
}

void FillNPPingResponse(void* buffer, nsqn_t sequence_number)
{
	_ASSERT(buffer);

	size_t np_size = ProjectNPPingResponse();
	NPPingResponse* np_buffer = (NPPingResponse*)buffer;

	memset(buffer, 0, np_size);

	np_buffer->PacketType = net_ping_response;
	np_buffer->PacketSize = np_size;
	np_buffer->SequenceNumber = sequence_number;
}

void ParseNetworkPacketTyped(NPHandshake* np_buffer, size_t np_size, IPacketReceiver& receiver)
{
	if (np_buffer->PacketSize != sizeof(NPHandshake))
	{
		THROW_TEXT("Invalid packet size !");
	}

	std::wstring alias(np_buffer->alias);

	receiver.ReceiveNPHandshake(np_buffer->SequenceNumber, alias);
}

void ParseNetworkPacketTyped(NPHandshakeResponse* np_buffer, size_t np_size, IPacketReceiver& receiver)
{
	if (np_buffer->PacketSize < sizeof(NPHandshakeResponse))
	{
		THROW_TEXT("Invalid packet size !");
	}
	if (np_buffer->PacketSize != sizeof(NPHandshakeResponse) + (np_buffer->prototype_count - 1) * sizeof(PrototypeDescription))
	{
	}

	std::wstring alias(np_buffer->alias);

	WarShipPrototypeList prototypes;

	PrototypeDescription* np_prototypes_it = np_buffer->prototypes;

	for (unsigned int counter = 0; counter < np_buffer->prototype_count; counter = counter + 1, np_prototypes_it = np_prototypes_it + 1)
	{
		RelBoardPosList prototype_points;

		BoardPoint* np_prototype_points_it = np_prototypes_it->points;

		for (int p_counter = 0; p_counter < np_prototypes_it->point_amount; p_counter = p_counter + 1, np_prototype_points_it = np_prototype_points_it + 1)
		{
			prototype_points.push_back(RelBoardPos(np_prototype_points_it->x, np_prototype_points_it->y));
		}

		prototypes.push_back(WarShipPrototypePtr(new WarShipPrototype(prototype_points, np_prototypes_it->limit)));
	}

	receiver.ReceiveNPHandshakeResponse(np_buffer->SequenceNumber, alias, np_buffer->gameboard_size, 0 != np_buffer->is_server_move_first, prototypes);
}

void ParseNetworkPacketTyped(NPClientStartGame* np_buffer, size_t np_size, IPacketReceiver& receiver)
{
	if (np_buffer->PacketSize != sizeof(NPClientStartGame))
	{
		THROW_TEXT("Invalid packet size !");
	}

	receiver.ReceiveNPClientStartGame(np_buffer->SequenceNumber, np_buffer->event_number);
}

void ParseNetworkPacketTyped(NPFire* np_buffer, size_t np_size, IPacketReceiver& receiver)
{
	if (np_buffer->PacketSize != sizeof(NPFire))
	{
		THROW_TEXT("Invalid packet size !");
	}

	receiver.ReceiveNPFire(np_buffer->SequenceNumber, np_buffer->event_number, AbsBoardPos(np_buffer->fire_position.x, np_buffer->fire_position.y));
}

void ParseNetworkPacketTyped(NPFireResponse* np_buffer, size_t np_size, IPacketReceiver& receiver)
{
	if (np_buffer->PacketSize != sizeof(NPFireResponse))
	{
		THROW_TEXT("Invalid packet size !");
	}

	receiver.ReceiveNPFireResponse(np_buffer->SequenceNumber, np_buffer->event_number, np_buffer->request_sequence_number, (FireResult)np_buffer->fire_result);
}

void ParseNetworkPacketTyped(NPUseAbility* np_buffer, size_t np_size, IPacketReceiver& receiver)
{
	if (np_buffer->PacketSize != sizeof(NPUseAbility))
	{
		THROW_TEXT("Invalid packet size !");
	}

	ShipAbilityPtr ability;

	switch (np_buffer->ability_type)
	{
	case sa_shot:
	{
		ability = std::make_shared<ShotShipAbility>(AbsBoardPos(np_buffer->shot_ability.pos.x, np_buffer->shot_ability.pos.y));
		break;
	}
	default:
	{
		THROW_TEXT("Unsupported ability type !");
		break;
	}
	}

	receiver.ReceiveNPUseAbility(np_buffer->SequenceNumber, np_buffer->event_number, ability);
}

void ParseNetworkPacketTyped(NPGameUpdate* np_buffer, size_t np_size, IPacketReceiver& receiver)
{
	if (np_buffer->PacketSize != sizeof(NPGameUpdate))
	{
		THROW_TEXT("Invalid packet size !");
	}

	GameUpdatePtr update;

	switch (np_buffer->update_type)
	{
	case gu_shot_result:
	{
		auto& shot_result = np_buffer->shot_result;

		update = std::make_shared<ShotResultUpdate>((FireResult)shot_result.result, AbsBoardPos(shot_result.pos.x, shot_result.pos.y));
		break;
	}
	default:
	{
		THROW_TEXT("Unsupported update type !");
		break;
	}
	}

	receiver.ReceiveNPGameUpdate(np_buffer->SequenceNumber, np_buffer->event_number, update);
}

void ParseNetworkPacketTyped(NPEndGame* np_buffer, size_t np_size, IPacketReceiver& receiver)
{
	if (np_buffer->PacketSize < sizeof(NPFireResponse))
	{
		THROW_TEXT("Invalid packet size !");
	}

	int gb_size = np_buffer->gameboard_size;

	if (np_buffer->PacketSize != ALIGN_UP(sizeof(NPEndGame) + (gb_size * gb_size - 1), 4))
	{
		THROW_TEXT("Invalid packet size !");
	}

	GameBoard* game_board = new GameBoard;

	game_board->CreateEmpty(gb_size);

	char* np_bitmap_it = np_buffer->bitmap;

	for (int x = 0; x < gb_size; x = x + 1)
	{
		for (int y = 0; y < gb_size; y = y + 1)
		{
			BoardCell& gb_cell = game_board->Cell(AbsBoardPos(x, y));

			gb_cell.cell_type = (*np_bitmap_it) ? ct_ship : ct_empty;

			np_bitmap_it = np_bitmap_it + 1;
		}
	}

	receiver.ReceiveNPEndGame(np_buffer->SequenceNumber, np_buffer->event_number, (EndGameState)np_buffer->end_game_state, std::shared_ptr<GameBoard>(game_board));
}

void ParseNetworkPacketTyped(NPPing* np_buffer, size_t np_size, IPacketReceiver& receiver)
{
	if (np_buffer->PacketSize != sizeof(NPPing))
	{
		THROW_TEXT("Invalid packet size !");
	}

	receiver.ReceiveNPPing(np_buffer->SequenceNumber);
}

void ParseNetworkPacketTyped(NPPingResponse* np_buffer, size_t np_size, IPacketReceiver& receiver)
{
	if (np_buffer->PacketSize != sizeof(NPPingResponse))
	{
		THROW_TEXT("Invalid packet size !");
	}

	receiver.ReceiveNPPingResponse(np_buffer->SequenceNumber);
}

void ParseNetworkPacket(const void* np_buffer, size_t np_size, IPacketReceiver& receiver)
{
	if (np_size < sizeof(NPBasic) || np_size > npl_max_packet_size)
	{
		THROW_TEXT("Invalid packet size !");
	}

	NPBasic* np_basic_buffer = (NPBasic*)np_buffer;

	// FIXME: np_size must be >= np_buffer.size

	switch (np_basic_buffer->PacketType)
	{
	case net_handshake:
	{
		ParseNetworkPacketTyped((NPHandshake*)np_buffer, np_size, receiver);
		break;
	}
	case net_handshake_response:
	{
		ParseNetworkPacketTyped((NPHandshakeResponse*)np_buffer, np_size, receiver);
		break;
	}
	case net_client_start_game:
	{
		ParseNetworkPacketTyped((NPClientStartGame*)np_buffer, np_size, receiver);
		break;
	}
	case net_fire:
	{
		ParseNetworkPacketTyped((NPFire*)np_buffer, np_size, receiver);
		break;
	}
	case net_fire_response:
	{
		ParseNetworkPacketTyped((NPFireResponse*)np_buffer, np_size, receiver);
		break;
	}
	case net_use_ability:
	{
		ParseNetworkPacketTyped((NPUseAbility*)np_buffer, np_size, receiver);
		break;
	}
	case net_game_update:
	{
		ParseNetworkPacketTyped((NPGameUpdate*)np_buffer, np_size, receiver);
		break;
	}
	case net_end_game:
	{
		ParseNetworkPacketTyped((NPEndGame*)np_buffer, np_size, receiver);
		break;
	}
	case net_ping:
	{
		ParseNetworkPacketTyped((NPPing*)np_buffer, np_size, receiver);
		break;
	}
	case net_ping_response:
	{
		ParseNetworkPacketTyped((NPPingResponse*)np_buffer, np_size, receiver);
		break;
	}
	default:
	{
		THROW_TEXT("Unknown packet type !");
		break;
	}
	}
}
