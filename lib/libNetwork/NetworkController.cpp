#include "NetworkController.h"
#include "NetworkProtocol.h"
#include "PacketMaker.h"

#include <libGameEngine/GameEngine.h>
#include <libSystem/Socket.h>
#include <libSystem/Mutex.h>
#include <libSystem/Thread.h>

#include <queue>

#define TRACE_NC(_text) \
{ \
	std::ostringstream trace_text; \
	trace_text << std::dec << __LINE__ << ":" << __FUNCTION__ << ":" << "\"" << DecodeStateName (m_state) << "\"" << ": " << _text << "\n"; \
	OutputDebugStringA (trace_text.str ().c_str ()); \
}

const char* DecodeStateName(NetworkServerState state)
{
	switch (state)
	{
	case nss_not_ready: return "NOT READY";
	case nss_wait_for_connect: return "WAIT FOR CONNECT";
	case nss_wait_for_handshake: return "WAIT FOR HANDSHAKE";
	case nss_connected: return "CONNECTED";
	case nss_disconnected: return "DESCONNECTED";
	case nss_final: return "FINAL";
	default: return "UNKNOWN";
	}
}

const char* DecodeStateName(NetworkClientState state)
{
	switch (state)
	{
	case ncs_not_ready: return "NOT READY";
	case ncs_wait_for_handshake_reply: return "WAIT FOR HANDSHAKE REPLY";
	case ncs_connected: return "CONNECTED";
	case ncs_disconnected: return "DISCONNECTED";
	case ncs_final: return "FINAL";
	default: return "UNKNOWN";
	}
}

struct NIMLocalStartGame;
struct NIMLocalEndGame;
struct NIMLocalFireRequest;
struct NIMLocalFireResponce;
struct NIMLocalUseAbility;
struct NIMLocalGameUpdate;

class INetInternalMessageHandler
{
public:
	virtual void Handle(NIMLocalStartGame&) = 0;
	virtual void Handle(NIMLocalEndGame&) = 0;
	virtual void Handle(NIMLocalFireRequest&) = 0;
	virtual void Handle(NIMLocalFireResponce&) = 0;
	virtual void Handle(NIMLocalUseAbility&) = 0;
	virtual void Handle(NIMLocalGameUpdate&) = 0;
};

enum NImType
{
	nim_local_side_start_game = 0,
	nim_local_side_end_game,
	nim_local_side_fire_request,
	nim_local_side_fire_responce,
	nim_local_side_use_ability,
	nim_local_side_game_update
	//sim_my_start_game,
	//sim_my_end_game,
	//sim_my_fire_request,
	//sim_my_fire_response
};

struct NetInternalMessage
{
public:
	NetInternalMessage() = delete;

	explicit NetInternalMessage(NImType im_type) : m_type(im_type) {}
	virtual ~NetInternalMessage() {}

	virtual void Visit(INetInternalMessageHandler& handler) = 0;

	NImType m_type;

};

struct NIMLocalStartGame : public NetInternalMessage
{
	NIMLocalStartGame(gen_t event_number) : NetInternalMessage(nim_local_side_start_game), m_event_number(event_number) {}

	void Visit(INetInternalMessageHandler& handler) { handler.Handle(*this); }

	gen_t m_event_number;

};

struct NIMLocalEndGame : public NetInternalMessage
{
	NIMLocalEndGame(gen_t event_number, EndGameState enemy_state, std::shared_ptr<GameBoard> enemy_gameboard) :
		NetInternalMessage(nim_local_side_end_game),
		m_event_number(event_number),
		m_enemy_state(enemy_state),
		m_enemy_gameboard(enemy_gameboard) {}

	void Visit(INetInternalMessageHandler& handler) { handler.Handle(*this); }

	gen_t m_event_number;
	EndGameState m_enemy_state;
	std::shared_ptr<GameBoard> m_enemy_gameboard;

};

struct NIMLocalFireRequest : public NetInternalMessage
{
	NIMLocalFireRequest(gen_t event_number, AbsBoardPos hit_pos) :
		NetInternalMessage(nim_local_side_fire_request),
		m_event_number(event_number),
		m_hit_pos(hit_pos) {}

	void Visit(INetInternalMessageHandler& handler) { handler.Handle(*this); }

	gen_t m_event_number;
	AbsBoardPos m_hit_pos;

};

struct NIMLocalFireResponce : public NetInternalMessage
{
	NIMLocalFireResponce(gen_t event_number, FireResult fire_result, gen_t request_number) :
		NetInternalMessage(nim_local_side_fire_responce),
		m_event_number(event_number),
		m_fire_result(fire_result),
		m_request_number(request_number) {}

	void Visit(INetInternalMessageHandler& handler) { handler.Handle(*this); }

	gen_t m_event_number;
	FireResult m_fire_result;
	gen_t m_request_number;

};

struct NIMLocalUseAbility : public NetInternalMessage
{
	NIMLocalUseAbility(gen_t event_number, ShipAbilityPtr ability) :
		NetInternalMessage(nim_local_side_use_ability),
		m_event_number(event_number),
		m_ability(ability)
	{}

	void Visit(INetInternalMessageHandler& handler) { handler.Handle(*this); }

	gen_t m_event_number;
	ShipAbilityPtr m_ability;

};

struct NIMLocalGameUpdate : public NetInternalMessage
{
	NIMLocalGameUpdate(gen_t event_number, GameUpdatePtr update) :
		NetInternalMessage(nim_local_side_game_update),
		m_event_number(event_number),
		m_update(update)
	{}

	void Visit(INetInternalMessageHandler& handler) { handler.Handle(*this); }

	gen_t m_event_number;
	GameUpdatePtr m_update;

};

template<typename TStatename, typename TCompleteType> class BasicNetworkController : public INetInternalMessageHandler
{
public:
	BasicNetworkController(TCompleteType& owner) :
		m_other_side_interface(0),
		m_game_controller_net_notify_interface(0),
		m_sequence_number(2),
		m_state((TStatename)nbs_not_ready),
		m_final_state(fgs_game),
		m_pump_thread(owner)
	{};

	void Handle(NIMLocalEndGame& message) override
	{
		size_t end_game_packet_size = ProjectNPEndGame(message.m_enemy_state, *message.m_enemy_gameboard);
		char* end_game_packet = (char*)_alloca(end_game_packet_size);
		FillNPEndGame(end_game_packet, NextSequenceNumber(), message.m_enemy_state, *message.m_enemy_gameboard, message.m_event_number);

		// TODO: реализовать повторную попытку и реконнект
		send((SOCKET)*GetIOSocket(), end_game_packet, end_game_packet_size, 0);
	}

	void Handle(NIMLocalFireRequest& message) override
	{
		size_t fire_request_packet_size = ProjectNPFire(message.m_hit_pos);
		char* fire_request_packet = (char*)_alloca(fire_request_packet_size);
		FillNPFire(fire_request_packet, NextSequenceNumber(), message.m_hit_pos, message.m_event_number);

		// TODO: реализовать повторную попытку и реконнект
		send((SOCKET)*GetIOSocket(), fire_request_packet, fire_request_packet_size, 0);
	}

	void Handle(NIMLocalFireResponce& message) override
	{
		size_t fire_responce_packet_size = ProjectNPFireResponse(message.m_request_number, message.m_fire_result);
		char* fire_responce_packet = (char*)_alloca(fire_responce_packet_size);
		FillNPFireResponse(fire_responce_packet, NextSequenceNumber(), message.m_request_number, message.m_fire_result, message.m_event_number);

		// TODO: реализовать повторную попытку и реконнект
		send((SOCKET)*GetIOSocket(), fire_responce_packet, fire_responce_packet_size, 0);
	}

	void Handle(NIMLocalUseAbility& message) override
	{
		size_t use_ability_packet_size = ProjectNPUseAbility(message.m_ability);
		char* use_ability_packet = (char*)_alloca(use_ability_packet_size);
		FillNPUseAbility(use_ability_packet, NextSequenceNumber(), message.m_ability, message.m_event_number);

		send((SOCKET)*GetIOSocket(), use_ability_packet, use_ability_packet_size, 0);
	}

	void Handle(NIMLocalGameUpdate& message) override
	{
		size_t game_update_packet_size = ProjectNPGameUpdate(message.m_update);
		char* game_update_packet = (char*)_alloca(game_update_packet_size);
		FillNPGameUpdate(game_update_packet, game_update_packet_size, message.m_update, message.m_event_number);

		send((SOCKET)*GetIOSocket(), game_update_packet, game_update_packet_size, 0);
	}

	virtual BasicSocket* GetIOSocket() = 0;

protected:
	IGameEngineSide* m_other_side_interface;
	IControllerNetNotify* m_game_controller_net_notify_interface;

	void PostInternal(std::shared_ptr<NetInternalMessage> message)
	{
		//AutoLocker locker (m_game_state_mutex);
		// ^ mutex getted anyway

		m_queue.push(message);

		if (m_queue.size() == 1)
		{
			m_event_queue_not_empty.Set();
		}
	}

	template<typename T> void Post(T& message)
	{
		auto message_copy = std::make_shared<T>(message);

		PostInternal(message_copy);
	}

	std::queue<std::shared_ptr<NetInternalMessage>> m_queue;
	NotificationEvent m_event_queue_not_empty;

	NotificationEvent m_receive_event;

	NotificationEvent m_close_event;

	void PendRecvBuffer(std::shared_ptr<AsyncRequest> bucket)
	{
		TRACE_NC("Entered");

		//AutoLocker locker (m_state_mutex);

		ResetEvent(bucket->m_overlapped_packet.hEvent);

		DWORD unused_flags = 0;

		int recv_return = WSARecv((SOCKET)*GetIOSocket(), &bucket->m_wsa_buffer, 1, 0, &unused_flags, &bucket->m_overlapped_packet, 0);

		m_recieve_bucket_list.push_back(*m_recieve_bucket_list.begin());
		m_recieve_bucket_list.pop_front();

		if (recv_return)
		{
			int error_code = WSAGetLastError();

			_ASSERT(error_code);		// в назидание потомкам настоящего поколения нервных клеток

			if (error_code != WSA_IO_PENDING)
			{
				TRACE_SOCKET("Can't pend receive buffer !");
				THROW_TEXT("Can't pend receive buffer !");

				// TODO: реализовать повторную попытку и реконнект
				m_other_side_interface->EndGame(0, egs_canceled, 0);
			}
		}
	}

	class PumpThread : public Thread
	{
	public:
		PumpThread(TCompleteType& owner);

		void Run()
		{
			TRACE_TEXT("Entered");

			enum
			{
				ind_stop_event = 0,
				ind_close_socket_event,
				ind_queue_not_empty_event,

				ind_fixed_event_amount
			};

			const size_t handle_array_size = ind_fixed_event_amount + m_owner.m_recieve_bucket_list.size();

			HANDLE* handle_array = (HANDLE*)_alloca(handle_array_size * sizeof(HANDLE));

			handle_array[ind_stop_event] = m_stop_event.GetHandle();
			handle_array[ind_close_socket_event] = m_owner.m_close_event.GetHandle();
			handle_array[ind_queue_not_empty_event] = m_owner.m_event_queue_not_empty.GetHandle();

			for (;;)
			{
				try
				{
					int arr_counter = ind_fixed_event_amount;
					for (
						std::list<std::shared_ptr<AsyncRequest>>::iterator counter = m_owner.m_recieve_bucket_list.begin();
						counter != m_owner.m_recieve_bucket_list.end();
						counter++, arr_counter = arr_counter + 1)
					{
						handle_array[arr_counter] = counter->get()->m_overlapped_packet.hEvent;
					}

					DWORD wait_return = ::WaitForMultipleObjects(handle_array_size, handle_array, FALSE, INFINITE);

					switch (wait_return)
					{
					case WAIT_OBJECT_0 + ind_stop_event:		// Остановка connect-a
					{
						if (m_owner.GetIOSocket())
						{
							CancelIo((HANDLE)(SOCKET)*m_owner.GetIOSocket());
						}

						break;
					}
					case WAIT_OBJECT_0 + ind_close_socket_event:
					{
						TRACE_TEXT("Connection closed");

						AutoLocker locker(m_state_mutex);

						m_owner.m_close_event.Reset();

						if (m_owner.m_state != (TStatename)nbs_final)
						{
							m_owner.m_state = (TStatename)nbs_disconnected;

							for (;;)
							{
								DWORD data_wait_result = ::WaitForMultipleObjects(handle_array_size - ind_fixed_event_amount, handle_array + ind_fixed_event_amount, FALSE, 0);

								if (data_wait_result == WAIT_TIMEOUT)
								{
									break;
								}

								try
								{
									std::list<std::shared_ptr<AsyncRequest>>::iterator bucket_it = m_owner.m_recieve_bucket_list.begin();
									for (size_t counter = 0; counter != data_wait_result - WAIT_OBJECT_0; counter = counter + 1)
									{
										bucket_it++;
									}

									m_owner.NetworkReceive(*bucket_it);
								}
								catch (...)
								{
								}
							}

							if (m_owner.m_final_state != fgs_finished)
							{
								m_owner.m_other_side_interface->EndGame(0, egs_canceled, 0);
							}

							if (m_owner.m_game_controller_net_notify_interface)
							{
								m_owner.m_game_controller_net_notify_interface->GameProcessEnded();
							}
						}

						break;
					}
					case WAIT_OBJECT_0 + ind_queue_not_empty_event:		// Очередь не пуста
					{
						std::shared_ptr<NetInternalMessage> message_ptr;

						{
							AutoLocker locker(m_owner.m_state_mutex);

							message_ptr = m_owner.m_queue.front();
							m_owner.m_queue.pop();

							if (!m_owner.m_queue.size())
							{
								m_owner.m_event_queue_not_empty.Reset();
							}
						}

						message_ptr->Visit(m_owner);

						break;
					}
					default:
					{
						std::list<std::shared_ptr<AsyncRequest>>::iterator bucket_it = m_owner.m_recieve_bucket_list.begin();
						for (size_t counter = 0; counter != wait_return - WAIT_OBJECT_0 - ind_fixed_event_amount; counter = counter + 1)
						{
							bucket_it++;
						}

						m_owner.NetworkReceive(*bucket_it);

						break;
					}
					}

					if (wait_return == WAIT_OBJECT_0)		// Остановка connect-a
					{
						break;
					}
				}
				catch (std::exception& e)
				{
					TRACE_TEXT_CONTEXT(e.what(), this);
				}
				catch (...)
				{
					TRACE_TEXT_CONTEXT("Exception caught", this);
				}
			}

			TRACE_TEXT_CONTEXT("Exiting thread", this);
		}

	private:
		TCompleteType& m_owner;
	};

	PumpThread m_pump_thread;

	std::list<std::shared_ptr<AsyncRequest>> m_recieve_bucket_list;

	nsqn_t NextSequenceNumber() { return ++m_sequence_number; }
	nsqn_t m_sequence_number;

	Mutex m_state_mutex;

	TStatename m_state;

	FinalGameState m_final_state;
};

#undef SetPort

class ServerNetworkController :
	public IGameEngineSide,
	public IGameEngineManage,
	public INetworkServer,
	public IPacketReceiver,
	public BasicNetworkController <NetworkServerState, ServerNetworkController>
{
public:
	ServerNetworkController();
	~ServerNetworkController();

	//// IGameEngineSide
	void GetGameboardParameters(int& gameboard_size, bool& is_server_moves_first, WarShipPrototypeList& warships) override;
	void ConnectionEstablished() override;
	void StartGame(gen_t EventNumber) override;
	void EndGame(gen_t EventNumber, EndGameState enemy_state, std::shared_ptr<GameBoard> enemy_gameboard) override;
	void FireRequest(gen_t EventNumber, AbsBoardPos hit_pos) override;
	void FireResponse(gen_t EventNumber, FireResult fire_result, gen_t RequestNumber) override;
	void UseAbility(gen_t event_number, ShipAbilityPtr ability) override;
	void GameUpdate(gen_t event_number, GameUpdatePtr update) override;
	void MoveEnded() override;
	std::wstring GetAlias();

	//// IGameEngineManage
	void ConnectOtherSide(IGameEngineSide* other_side_interface) override;
	void ConnectGameController(IGameControllerNotify* game_controller_interface) override;
	void ConnectNetworkNotify(IControllerNetNotify* game_controller_net_notify_interface) override;
	void Ready() override;
	void Unready() override;

	//// INetworkServer
	void SetPort(int port_number) override;
	void SetAddress(DWORD ip4_address) override;
	void SetAlias(const std::wstring& alias) override;

	//// IPacketReceiver
	void ReceiveNPHandshake(nsqn_t sequence_number, const std::wstring& alias) override;
	void ReceiveNPHandshakeResponse(nsqn_t sequence_number, const std::wstring& alias, int gameboard_size, bool is_server_move_first, const WarShipPrototypeList& prototypes) override;
	void ReceiveNPClientStartGame(nsqn_t sequence_number, gen_t event_number) override;
	void ReceiveNPFire(nsqn_t sequence_number, gen_t event_number, const AbsBoardPos& fire_pos) override;
	void ReceiveNPFireResponse(nsqn_t sequence_number, gen_t event_number, gen_t request_sequence_number, FireResult fire_result) override;
	void ReceiveNPUseAbility(nsqn_t sequence_number, gen_t event_number, const ShipAbilityPtr& ability) override;
	void ReceiveNPGameUpdate(nsqn_t sequence_number, gen_t event_number, const GameUpdatePtr& update) override;
	void ReceiveNPEndGame(nsqn_t sequence_number, gen_t event_number, EndGameState end_gsme_state, std::shared_ptr<GameBoard> game_board) override;
	void ReceiveNPPing(nsqn_t sequence_number) override;
	void ReceiveNPPingResponse(nsqn_t sequence_number) override;

	//// INetInternalMessageHandler
	void Handle(NIMLocalStartGame&) override;
	void Handle(NIMLocalEndGame& message) override;

	BasicSocket* GetIOSocket() { return m_server_socket.get(); }

	void NetworkReceive(std::shared_ptr<AsyncRequest> bucket);		//Зовется при поступлении данных в подключенный сокет

private:
	int m_port_number;
	DWORD m_ip4_address;
	std::wstring m_my_alias;

	std::wstring m_enemy_alias;

	void SendHandshakeReply();

	std::unique_ptr<ServerSocket> m_server_socket;
	std::unique_ptr<ListeningSocket> m_listen_socket;
};

ServerNetworkController::ServerNetworkController() :
	BasicNetworkController(*this)
{}

ServerNetworkController::~ServerNetworkController()
{
	_ASSERT(!m_other_side_interface);
}

void ServerNetworkController::GetGameboardParameters(int& gameboard_size, bool& is_server_moves_first, WarShipPrototypeList& warships)
{
	_ASSERT(0);
}

void ServerNetworkController::ConnectionEstablished()
{
	_ASSERT(0);
}

void ServerNetworkController::StartGame(gen_t EventNumber)
{
	Post(NIMLocalStartGame(EventNumber));
}

void ServerNetworkController::EndGame(gen_t EventNumber, EndGameState enemy_state, std::shared_ptr<GameBoard> enemy_gameboard)
{
	if (!enemy_gameboard.get())
	{
		THROW_TEXT("Invalid enemy gameboard !");
	}

	Post(NIMLocalEndGame(EventNumber, enemy_state, enemy_gameboard));
}

void ServerNetworkController::FireRequest(gen_t EventNumber, AbsBoardPos hit_pos)
{
	Post(NIMLocalFireRequest(EventNumber, hit_pos));
}

void ServerNetworkController::FireResponse(gen_t EventNumber, FireResult fire_result, gen_t RequestNumber)
{
	Post(NIMLocalFireResponce(EventNumber, fire_result, RequestNumber));
}

void ServerNetworkController::UseAbility(gen_t event_number, ShipAbilityPtr ability)
{
	TRACE_NC("Entered");

	Post(NIMLocalUseAbility(event_number, ability));
}

void ServerNetworkController::GameUpdate(gen_t event_number, GameUpdatePtr update)
{
	TRACE_NC("Entered");

	Post(NIMLocalGameUpdate(event_number, update));
}

void ServerNetworkController::MoveEnded()
{
	TRACE_NC("Entered");

	_ASSERT(0);
}

void ServerNetworkController::ConnectOtherSide(IGameEngineSide* other_side_interface)
{
	AutoLocker game_status_locker(m_state_mutex);

	m_other_side_interface = other_side_interface;
}

void ServerNetworkController::ConnectGameController(IGameControllerNotify* game_controller_interface)
{
	_ASSERT(0);
}

void ServerNetworkController::ConnectNetworkNotify(IControllerNetNotify* game_controller_net_notify_interface)
{
	AutoLocker game_status_locker(m_state_mutex);

	m_game_controller_net_notify_interface = game_controller_net_notify_interface;
}

void ServerNetworkController::Ready()
{
	TRACE_NC("Entered");

	AutoLocker game_status_locker(m_state_mutex);

	if (m_state != nss_not_ready)
	{
		THROW_TEXT("Unexpected initialization !");
	}

	_ASSERT(m_other_side_interface);

	for (int counter = 0; counter < 3; counter = counter + 1)
	{
		std::shared_ptr<AsyncRequest> new_async_request = std::shared_ptr<AsyncRequest>(new AsyncRequest(npl_max_packet_size));

		new_async_request->m_overlapped_packet.hEvent = ::CreateEvent(0, TRUE, FALSE, 0);

		m_recieve_bucket_list.push_back(new_async_request);
	}

	m_listen_socket.reset(new ListeningSocket());

	m_listen_socket->Bind(m_port_number, m_ip4_address);
	m_listen_socket->ListenForOneConnection();

	std::ostringstream trace_text;
	trace_text << "Listening " << m_port_number << " port";

	TRACE_TEXT(trace_text.str());

	m_server_socket = m_listen_socket->AsyncAccept(*m_recieve_bucket_list.begin()->get());

	WSAEventSelect((SOCKET)*m_server_socket, m_close_event.GetHandle(), FD_CLOSE);

	m_state = nss_wait_for_connect;

	m_pump_thread.StartAsync();
}

void ServerNetworkController::Unready()
{
	TRACE_NC("Entered");

	m_pump_thread.StopAsync();

	m_pump_thread.WaitForThreadFinished();

	m_server_socket.reset(0);
	m_listen_socket.reset(0);
}

void ServerNetworkController::SetPort(int port_number)
{
	AutoLocker locker(m_state_mutex);

	if (m_state != nss_not_ready)
	{
		THROW_TEXT("Setup time is over !");
	}

	m_port_number = port_number;
}

void ServerNetworkController::SetAddress(DWORD ip4_address)
{
	AutoLocker locker(m_state_mutex);

	if (m_state != nss_not_ready)
	{
		THROW_TEXT("Setup time is over !");
	}

	m_ip4_address = ip4_address;
}

void ServerNetworkController::SetAlias(const std::wstring& alias)
{
	AutoLocker locker(m_state_mutex);

	if (m_state != nss_not_ready)
	{
		THROW_TEXT("Setup time is over !");
	}

	m_my_alias = alias;
}

void ServerNetworkController::ReceiveNPHandshake(nsqn_t sequence_number, const std::wstring& alias)
{
	m_enemy_alias = alias;

	if (m_game_controller_net_notify_interface)
	{
		m_game_controller_net_notify_interface->PlayerConnected();
	}

	SendHandshakeReply();

	m_state = nss_connected;
}

void ServerNetworkController::ReceiveNPHandshakeResponse(nsqn_t sequence_number, const std::wstring& alias, int gameboard_size, bool is_server_move_first, const WarShipPrototypeList& prototypes)
{
	_ASSERT(0);
}

void ServerNetworkController::ReceiveNPClientStartGame(nsqn_t sequence_number, gen_t event_number)
{
	TRACE_NC("Entered");

	m_other_side_interface->StartGame(event_number);
}

void ServerNetworkController::ReceiveNPFire(nsqn_t sequence_number, gen_t event_number, const AbsBoardPos& fire_pos)
{
	TRACE_NC("Entered");

	m_other_side_interface->FireRequest(event_number, fire_pos);
}

void ServerNetworkController::ReceiveNPFireResponse(nsqn_t sequence_number, gen_t event_number, gen_t request_sequence_number, FireResult fire_result)
{
	TRACE_NC("Entered");

	m_other_side_interface->FireResponse(event_number, fire_result, request_sequence_number);
}

void ServerNetworkController::ReceiveNPUseAbility(nsqn_t sequence_number, gen_t event_number, const ShipAbilityPtr& ability)
{
	TRACE_NC("Entered");

	m_other_side_interface->UseAbility(event_number, ability);
}

void ServerNetworkController::ReceiveNPGameUpdate(nsqn_t sequence_number, gen_t event_number, const GameUpdatePtr& update)
{
	TRACE_NC("Entered");

	m_other_side_interface->GameUpdate(event_number, update);
}

void ServerNetworkController::ReceiveNPEndGame(nsqn_t sequence_number, gen_t event_number, EndGameState end_gsme_state, std::shared_ptr<GameBoard> game_board)
{
	TRACE_NC("Entered");

	m_other_side_interface->EndGame(event_number, end_gsme_state, game_board);

	if (m_final_state == fgs_endgame_received || m_final_state == fgs_finished)
	{
		_ASSERT(0);
	}

	if (m_final_state == fgs_game)
	{
		m_final_state = fgs_endgame_received;
	}
	else
	{
		m_final_state = fgs_finished;

		m_game_controller_net_notify_interface->GameProcessEnded();
	}
}

void ServerNetworkController::ReceiveNPPing(nsqn_t sequence_number)
{}

void ServerNetworkController::ReceiveNPPingResponse(nsqn_t sequence_number)
{}

std::wstring ServerNetworkController::GetAlias()
{
	return m_enemy_alias;
}

void ServerNetworkController::SendHandshakeReply()
{
	int gameboard_size = 0;
	bool is_server_moves_first;
	WarShipPrototypeList prototypes;

	m_other_side_interface->GetGameboardParameters(gameboard_size, is_server_moves_first, prototypes);

	size_t handshake_packet_size = ProjectNPHandshakeResponse(m_my_alias, gameboard_size, is_server_moves_first, prototypes);
	char* handshake_packet = (char*)_alloca(handshake_packet_size);
	FillNPHandshakeResponse(handshake_packet, NextSequenceNumber(), m_my_alias, gameboard_size, is_server_moves_first, prototypes);

	// TODO: реализовать повторную попытку и реконнект
	send((SOCKET)*m_server_socket, handshake_packet, handshake_packet_size, 0);
}

void ServerNetworkController::NetworkReceive(const std::shared_ptr<AsyncRequest> bucket)
{
	TRACE_NC("Entered");

	AutoLocker locker(m_state_mutex);

	ResetEvent(bucket->m_overlapped_packet.hEvent);

	DWORD overlapped_result = 0;
	if (FALSE == GetOverlappedResult(m_server_socket.get(), &bucket->m_overlapped_packet, &overlapped_result, FALSE))
	{
		TRACE_NC(GetLastError());

		return;
	}

	try
	{
		if (m_state == nss_wait_for_connect)
		{
			m_server_socket->OnAccept(bucket->m_buffer, bucket->m_size);

			TRACE_NC("Received connection from address " << m_server_socket->GetRemoteAddress() << " port " << m_server_socket->GetRemotePort());

			for (size_t counter = m_recieve_bucket_list.size(); counter > 0; counter = counter - 1)
			{
				PendRecvBuffer(*m_recieve_bucket_list.begin());
			}

			m_state = nss_wait_for_handshake;

			TRACE_NC("Waiting for handshake");
		}
		else
		{
			_ASSERT(bucket->m_overlapped_packet.InternalHigh <= bucket->m_size);

			ParseNetworkPacket(bucket->m_buffer, bucket->m_overlapped_packet.InternalHigh, *this);

			if (m_state == nss_connected)
			{
				PendRecvBuffer(bucket);
			}
		}
	}
	catch (std::exception& e)
	{
		std::ostringstream text;
		text << "Exception caught: " << e.what();

		TRACE_NC(text.str().c_str());
		m_other_side_interface->EndGame(0, egs_canceled, 0);
	}
	catch (...)
	{
		// TODO: реализовать повторную попытку и реконнект
		// TODO: написать юзеру что произошла ошибка

		TRACE_NC("Exception caught");
		m_other_side_interface->EndGame(0, egs_canceled, 0);
	}
}

ServerNetworkController::PumpThread::PumpThread(ServerNetworkController& owner) :
	m_owner(owner)
{}

void ServerNetworkController::Handle(NIMLocalStartGame& message)
{
	_ASSERT(0);
}

void ServerNetworkController::Handle(NIMLocalEndGame& message)
{
	BasicNetworkController::Handle(message);

	m_state = nss_final;

	if (m_final_state == fgs_endgame_sended || m_final_state == fgs_finished)
	{
		_ASSERT(0);
	}

	if (m_final_state == fgs_game)
	{
		m_final_state = fgs_endgame_sended;
	}
	else
	{
		m_final_state = fgs_finished;

		m_game_controller_net_notify_interface->GameProcessEnded();
	}
}

class ClientNetworkController : public IGameEngineSide, public IGameEngineManage, public INetworkClient, public IPacketReceiver, public BasicNetworkController <NetworkClientState, ClientNetworkController>
{
public:
	ClientNetworkController();
	~ClientNetworkController();

	//// IGameEngineSide
	void GetGameboardParameters(int& gameboard_size, bool& is_first, WarShipPrototypeList& warships) override;
	void ConnectionEstablished() override;
	void StartGame(gen_t EventNumber) override;
	void EndGame(gen_t EventNumber, EndGameState enemy_state, std::shared_ptr<GameBoard> enemy_gameboard) override;
	void FireRequest(gen_t EventNumber, AbsBoardPos hit_pos) override;
	void FireResponse(gen_t EventNumber, FireResult fire_result, gen_t RequestNumber) override;
	void UseAbility(gen_t event_number, ShipAbilityPtr ability) override;
	void GameUpdate(gen_t event_number, GameUpdatePtr update) override;
	void MoveEnded() override;
	std::wstring GetAlias() override;

	//// IGameEngineManage
	void ConnectOtherSide(IGameEngineSide* other_side_interface) override;
	void ConnectGameController(IGameControllerNotify* game_controller_interface) override;
	void ConnectNetworkNotify(IControllerNetNotify* game_controller_net_notify_interface) override;
	void Ready() override;
	void Unready() override;

	//// INetworkClient
	void SetServerPort(int port_number) override;
	void SetServerAddress(DWORD ip4_address) override;
	void SetAlias(const std::wstring& alias) override;

	//// IPacketReceiver
	void ReceiveNPHandshake(nsqn_t sequence_number, const std::wstring& alias) override;
	void ReceiveNPHandshakeResponse(nsqn_t sequence_number, const std::wstring& alias, int gameboard_size, bool is_server_move_first, const WarShipPrototypeList& prototypes) override;
	void ReceiveNPClientStartGame(nsqn_t sequence_number, gen_t event_number) override;
	void ReceiveNPFire(nsqn_t sequence_number, gen_t event_number, const AbsBoardPos& fire_pos) override;
	void ReceiveNPFireResponse(nsqn_t sequence_number, gen_t event_number, gen_t request_sequence_number, FireResult fire_result) override;
	void ReceiveNPUseAbility(nsqn_t sequence_number, gen_t event_number, const ShipAbilityPtr& ability) override;
	void ReceiveNPGameUpdate(nsqn_t sequence_number, gen_t event_number, const GameUpdatePtr& update) override;
	void ReceiveNPEndGame(nsqn_t sequence_number, gen_t event_number, EndGameState end_gsme_state, std::shared_ptr<GameBoard> game_board) override;
	void ReceiveNPPing(nsqn_t sequence_number) override;
	void ReceiveNPPingResponse(nsqn_t sequence_number) override;

	//// INetInternalMessageHandler
	void Handle(NIMLocalStartGame&) override;
	void Handle(NIMLocalEndGame& message) override;

	BasicSocket* GetIOSocket() { return m_socket.get(); }

	void NetworkReceive(std::shared_ptr<AsyncRequest> bucket);		//Зовется при поступлении данных в подключенный сокет

private:

	std::unique_ptr<ClientSocket> m_socket;

	DWORD m_server_address;
	int m_server_port_number;
	std::wstring m_my_alias;

	std::wstring m_enemy_alias;
	int m_gameboard_size;
	WarShipPrototypeList m_prototypes;
	bool m_am_first;

	void SendHandshake();
};

ClientNetworkController::ClientNetworkController() :
	BasicNetworkController(*this)
{}

ClientNetworkController::~ClientNetworkController()
{}

void ClientNetworkController::GetGameboardParameters(int& gameboard_size, bool& is_first, WarShipPrototypeList& warships)
{
	gameboard_size = m_gameboard_size;
	is_first = m_am_first;
	warships = m_prototypes;
}

void ClientNetworkController::ConnectionEstablished()
{
	_ASSERT(0);
}

void ClientNetworkController::StartGame(gen_t EventNumber)
{
	TRACE_NC("Entered");

	Post(NIMLocalStartGame(EventNumber));
}

void ClientNetworkController::EndGame(gen_t EventNumber, EndGameState enemy_state, std::shared_ptr<GameBoard> enemy_gameboard)
{
	TRACE_NC("Entered");

	Post(NIMLocalEndGame(EventNumber, enemy_state, enemy_gameboard));
}

void ClientNetworkController::FireRequest(gen_t EventNumber, AbsBoardPos hit_pos)
{
	TRACE_NC("Entered");

	Post(NIMLocalFireRequest(EventNumber, hit_pos));
}

void ClientNetworkController::FireResponse(gen_t EventNumber, FireResult fire_result, gen_t RequestNumber)
{
	TRACE_NC("Entered");

	Post(NIMLocalFireResponce(EventNumber, fire_result, RequestNumber));
}

void ClientNetworkController::UseAbility(gen_t event_number, ShipAbilityPtr ability)
{
	TRACE_NC("Entered");

	Post(NIMLocalUseAbility(event_number, ability));
}

void ClientNetworkController::GameUpdate(gen_t event_number, GameUpdatePtr update)
{
	TRACE_NC("Entered");

	Post(NIMLocalGameUpdate(event_number, update));
}

void ClientNetworkController::MoveEnded()
{
	TRACE_NC("Entered");
}

std::wstring ClientNetworkController::GetAlias()
{
	TRACE_NC("Entered");

	return m_enemy_alias;
}

void ClientNetworkController::ConnectOtherSide(IGameEngineSide* other_side_interface)
{
	TRACE_NC("Entered");

	AutoLocker game_status_locker(m_state_mutex);

	m_other_side_interface = other_side_interface;
}

void ClientNetworkController::ConnectGameController(IGameControllerNotify* game_controller_interface)
{
	TRACE_NC("Entered");
}

void ClientNetworkController::ConnectNetworkNotify(IControllerNetNotify* game_controller_net_notify_interface)
{
	AutoLocker game_status_locker(m_state_mutex);

	m_game_controller_net_notify_interface = game_controller_net_notify_interface;
}

void ClientNetworkController::NetworkReceive(std::shared_ptr<AsyncRequest> bucket)
{
	TRACE_NC("Entered");

	AutoLocker locker(m_state_mutex);

	ResetEvent(bucket->m_overlapped_packet.hEvent);

	DWORD overlapped_result = 0;
	if (FALSE == GetOverlappedResult(m_socket.get(), &bucket->m_overlapped_packet, &overlapped_result, FALSE))
	{
		TRACE_NC(GetLastError());

		return;
	}

	try
	{
		_ASSERT(bucket->m_overlapped_packet.InternalHigh <= bucket->m_size);

		ParseNetworkPacket(bucket->m_buffer, bucket->m_overlapped_packet.InternalHigh, *this);

		if (m_state == ncs_connected)
		{
			PendRecvBuffer(bucket);
		}
	}
	catch (...)
	{
		// TODO: реализовать повторную попытку и реконнект
		// TODO: написать юзеру что произошла ошибка

		TRACE_NC("Exception caught");
		m_other_side_interface->EndGame(0, egs_canceled, 0);
	}
}

void ClientNetworkController::SendHandshake()
{
	size_t handshake_packet_size = ProjectNPHandshake(m_my_alias);
	char* handshake_packet = (char*)_alloca(handshake_packet_size);
	FillNPHandshake(handshake_packet, m_sequence_number, m_my_alias);

	if (SOCKET_ERROR == send((SOCKET)*m_socket, handshake_packet, handshake_packet_size, 0))
	{
		TRACE_SOCKET("SEND failed ! (handshake)");
		THROW_TEXT("Send failed !");
	}
}

void ClientNetworkController::Ready()
{
	TRACE_NC("Entered");

	AutoLocker game_status_locker(m_state_mutex);

	if (m_state != ncs_not_ready)
	{
		THROW_TEXT("Unexpected initialization !");
	}

	_ASSERT(m_other_side_interface);

	for (int counter = 0; counter < 3; counter = counter + 1)
	{
		std::shared_ptr<AsyncRequest> new_async_request = std::shared_ptr<AsyncRequest>(new AsyncRequest(npl_max_packet_size));

		new_async_request->m_overlapped_packet.hEvent = ::CreateEvent(0, TRUE, FALSE, 0);

		m_recieve_bucket_list.push_back(new_async_request);
	}

	m_socket.reset(new ClientSocket());

	m_socket->Connect(m_server_address, m_server_port_number);

	for (size_t counter = m_recieve_bucket_list.size(); counter > 0; counter = counter - 1)
	{
		PendRecvBuffer(*m_recieve_bucket_list.begin());
	}

	WSAEventSelect((SOCKET)*m_socket, m_close_event.GetHandle(), FD_CLOSE);

	m_state = ncs_connected;

	SendHandshake();

	m_pump_thread.StartAsync();
}

void ClientNetworkController::Unready()
{
	TRACE_NC("Entered");

	TRACE_NC("Entered");

	m_pump_thread.StopAsync();

	m_pump_thread.WaitForThreadFinished();

	m_socket.reset(0);
}

void ClientNetworkController::SetServerPort(int port_number)
{
	TRACE_NC("Entered");

	AutoLocker game_status_locker(m_state_mutex);

	m_server_port_number = port_number;
}

void ClientNetworkController::SetServerAddress(DWORD ip4_address)
{
	TRACE_NC("Entered");

	m_server_address = ip4_address;
}

void ClientNetworkController::SetAlias(const std::wstring& alias)
{
	TRACE_NC("Entered");

	m_my_alias = alias;
}

void ClientNetworkController::ReceiveNPHandshake(nsqn_t sequence_number, const std::wstring& alias)
{
	TRACE_NC("Entered");

	// TODO: Завершить игру
}

void ClientNetworkController::ReceiveNPHandshakeResponse(nsqn_t sequence_number, const std::wstring& alias, int gameboard_size, bool is_server_move_first, const WarShipPrototypeList& prototypes)
{
	TRACE_NC("Entered");

	m_enemy_alias = alias;
	m_gameboard_size = gameboard_size;
	m_prototypes = prototypes;
	m_am_first = !is_server_move_first;

	m_other_side_interface->ConnectionEstablished();
}

void ClientNetworkController::ReceiveNPClientStartGame(nsqn_t sequence_number, gen_t event_number)
{
	TRACE_NC("Entered");

	_ASSERT(0);
}

void ClientNetworkController::ReceiveNPFire(nsqn_t sequence_number, gen_t event_number, const AbsBoardPos& fire_pos)
{
	TRACE_NC("Entered");

	m_other_side_interface->FireRequest(event_number, fire_pos);
}

void ClientNetworkController::ReceiveNPFireResponse(nsqn_t sequence_number, gen_t event_number, gen_t request_sequence_number, FireResult fire_result)
{
	TRACE_NC("Entered");

	m_other_side_interface->FireResponse(event_number, fire_result, request_sequence_number);
}

void ClientNetworkController::ReceiveNPUseAbility(nsqn_t sequence_number, gen_t event_number, const ShipAbilityPtr& ability)
{
	TRACE_NC("Entered");

	m_other_side_interface->UseAbility(event_number, ability);
}

void ClientNetworkController::ReceiveNPGameUpdate(nsqn_t sequence_number, gen_t event_number, const GameUpdatePtr& update)
{
	TRACE_NC("Entered");

	m_other_side_interface->GameUpdate(event_number, update);
}

void ClientNetworkController::ReceiveNPEndGame(nsqn_t sequence_number, gen_t event_number, EndGameState end_gsme_state, std::shared_ptr<GameBoard> game_board)
{
	TRACE_NC("Entered");

	m_other_side_interface->EndGame(event_number, end_gsme_state, game_board);

	if (m_final_state == fgs_endgame_received || m_final_state == fgs_finished)
	{
		_ASSERT(0);
	}

	if (m_final_state == fgs_game)
	{
		m_final_state = fgs_endgame_received;
	}
	else
	{
		m_final_state = fgs_finished;

		m_game_controller_net_notify_interface->GameProcessEnded();
	}
}

void ClientNetworkController::ReceiveNPPing(nsqn_t sequence_number)
{
	TRACE_NC("Entered");
}

void ClientNetworkController::ReceiveNPPingResponse(nsqn_t sequence_number)
{
	TRACE_NC("Entered");
}

ClientNetworkController::PumpThread::PumpThread(ClientNetworkController& owner) : m_owner(owner)
{}

void ClientNetworkController::Handle(NIMLocalStartGame& message)
{
	TRACE_NC("Entered");

	size_t start_game_packet_size = ProjectNPClientStartGame();
	char* start_game_packet = (char*)_alloca(start_game_packet_size);
	FillNPClientStartGame(start_game_packet, NextSequenceNumber(), message.m_event_number);

	// TODO: реализовать повторную попытку и реконнект
	send((SOCKET)*GetIOSocket(), start_game_packet, start_game_packet_size, 0);
}

void ClientNetworkController::Handle(NIMLocalEndGame& message)
{
	BasicNetworkController::Handle(message);

	m_state = ncs_final;

	if (m_final_state == fgs_endgame_sended || m_final_state == fgs_finished)
	{
		_ASSERT(0);
	}

	if (m_final_state == fgs_game)
	{
		m_final_state = fgs_endgame_sended;
	}
	else
	{
		m_final_state = fgs_finished;

		m_game_controller_net_notify_interface->GameProcessEnded();
	}
}

std::shared_ptr<IGameEngineManage> CreateServerController(IGameEngineSide*& game_engine_side, INetworkServer*& network_server_side)
{
	ServerNetworkController* new_server_controller = new ServerNetworkController();

	std::shared_ptr<IGameEngineManage> new_server_controller_smartptr(new_server_controller);

	game_engine_side = new_server_controller;
	network_server_side = new_server_controller;

	return new_server_controller_smartptr;
}

std::shared_ptr<IGameEngineManage> CreateClientController(IGameEngineSide*& game_engine_side, INetworkClient*& network_client_side)
{
	ClientNetworkController* new_client_controller = new ClientNetworkController();

	std::shared_ptr<IGameEngineManage> new_client_controller_smartptr(new_client_controller);

	game_engine_side = new_client_controller;
	network_client_side = new_client_controller;

	return new_client_controller_smartptr;
}
