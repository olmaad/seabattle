#include "Socket.h"

#include "SeaExceptions.h"
#include "Printer.h"

#include <sstream>

std::string DecodeWsaError (int error_code)
{
	switch (error_code)
	{
	case WSANOTINITIALISED: return "WSANOTINITIALISED";
	case WSAENETDOWN: return "WSAENETDOWN";
	case WSAEAFNOSUPPORT: return "WSAEAFNOSUPPORT";
	case WSAEINPROGRESS: return "WSAEINPROGRESS";
	case WSAEMFILE: return "WSAEMFILE";
	case WSAEINVAL: return "WSAEINVAL";
	case WSAENOBUFS: return "WSAENOBUFS";
	case WSAEPROTONOSUPPORT: return "WSAEPROTONOSUPPORT";
	case WSAEPROTOTYPE: return "WSAEPROTOTYPE";
	case WSAEPROVIDERFAILEDINIT: return "WSAEPROVIDERFAILEDINIT";
	case WSAESOCKTNOSUPPORT: return "WSAESOCKTNOSUPPORT";
	case WSA_NOT_ENOUGH_MEMORY: return "WSA_NOT_ENOUGH_MEMORY";
	case WSAHOST_NOT_FOUND: return "WSAHOST_NOT_FOUND";
	case WSANO_DATA: return "WSANO_DATA";
	case WSANO_RECOVERY: return "WSANO_RECOVERY";
	case WSATRY_AGAIN: return "WSATRY_AGAIN";
	case WSATYPE_NOT_FOUND: return "WSATYPE_NOT_FOUND";
	default:
		{
			std::ostringstream return_string;
			return_string << "Unknown error. Code: " << error_code;
			return return_string.str ();
		}
	}
}




AsyncRequest::AsyncRequest (size_t size)
{
	memset (&m_overlapped_packet, 0, sizeof (m_overlapped_packet));

	m_overlapped_packet.hEvent = ::CreateEvent (NULL, TRUE, FALSE, NULL);

	if (!m_overlapped_packet.hEvent)
	{
		THROW_TEXT ("Can't create event !");
	}

	m_buffer = new char [size];

	m_size = size;

	m_wsa_buffer.buf = m_buffer;
	m_wsa_buffer.len = m_size;
}


AsyncRequest::~AsyncRequest ()
{
	delete [] m_buffer;

	CloseHandle (m_overlapped_packet.hEvent);
}

////////////////////////////////////////////////////////////////////////////////////////////////////


BasicSocket::BasicSocket ()
{
	m_socket = ::socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (m_socket == INVALID_SOCKET)
	{
		TRACE_SOCKET ("Can't create socket !");

		THROW_TEXT ("Can't create socket !");
	}
}


BasicSocket::~BasicSocket ()
{
	::closesocket (m_socket);
}



////////////////////////////////////////////////////////////////////////////////////////////////////

ServerSocket::ServerSocket ()
{
}


ServerSocket::~ServerSocket ()
{
}


void ListeningSocket::Bind (int port_number, DWORD ip4_address)
{
	addrinfo hints = {0};
	addrinfo* avaliable_addresses = 0;

	hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

	char port_string [10];
	sprintf (port_string, "%d", port_number);

	int get_info = ::getaddrinfo ("", port_string, &hints, &avaliable_addresses);

	if (get_info)
	{
		TRACE_SOCKET ("Can't get avaliable addresses !");

		THROW_TEXT ("Can't get avaliable addresses !");
	}

	_ASSERT (avaliable_addresses);
	
	try
	{
		bool is_address_found = false;

		for (addrinfo* counter = avaliable_addresses;counter != 0;counter = counter->ai_next)
		{
			if (counter->ai_family == AF_INET)
			{
				sockaddr_in* sockaddr_ipv4 = (sockaddr_in *) counter->ai_addr;

				std::ostringstream bind_text;
				bind_text << "Found address: " << inet_ntoa(sockaddr_ipv4->sin_addr);
				TRACE_TEXT (bind_text.str ());

				if (sockaddr_ipv4->sin_addr.S_un.S_addr != ip4_address)
				{
					continue;
				}

				is_address_found = true;
				int bind_result = ::bind (m_socket, counter->ai_addr, counter->ai_addrlen);

				if (bind_result)
				{
					TRACE_SOCKET ("Can't bind socket !");

					THROW_TEXT ("Can't bind socket !");
				}
			}
		}

		if (!is_address_found)
		{
			::freeaddrinfo (avaliable_addresses);

			int get_info = ::getaddrinfo (0, port_string, &hints, &avaliable_addresses);

			if (get_info)
			{
				TRACE_SOCKET ("Can't get avaliable addresses !");

				THROW_TEXT ("Can't get avaliable addresses !");
			}

			if (avaliable_addresses->ai_family == AF_INET)
			{
				sockaddr_in* sockaddr_ipv4 = (sockaddr_in *) avaliable_addresses->ai_addr;

				std::ostringstream bind_text;
				bind_text << "Found address: " << inet_ntoa(sockaddr_ipv4->sin_addr);
				TRACE_TEXT (bind_text.str ());

				is_address_found = true;
				int bind_result = ::bind (m_socket, avaliable_addresses->ai_addr, avaliable_addresses->ai_addrlen);

				if (bind_result)
				{
					TRACE_SOCKET ("Can't bind socket !");

					THROW_TEXT ("Can't bind socket !");
				}
			}
		}

		if (!is_address_found)
		{
			THROW_TEXT ("Selected address not availiable !");
		}
	}
	catch (...)
	{
		::freeaddrinfo (avaliable_addresses);

		throw;
	}

	::freeaddrinfo (avaliable_addresses);

}


void ListeningSocket::ListenForOneConnection ()
{
	int listen_result = ::listen (m_socket, 1);

	if (listen_result)
	{
		TRACE_SOCKET ("Unable to listen on socket !");

		THROW_TEXT ("Unable to listen on socket !");
	}
}


std::unique_ptr<ServerSocket> ListeningSocket::AsyncAccept (AsyncRequest& accept_packet)
{
	GUID GuidAcceptEx = WSAID_ACCEPTEX;
	LPFN_ACCEPTEX lpfnAcceptEx = NULL;
	DWORD dwBytes;

	int ioctl_result = ::WSAIoctl (m_socket, SIO_GET_EXTENSION_FUNCTION_POINTER,
		&GuidAcceptEx, sizeof (GuidAcceptEx),
		&lpfnAcceptEx, sizeof (lpfnAcceptEx),
		&dwBytes, 0, 0);
	
	if (ioctl_result == SOCKET_ERROR)
	{
		TRACE_SOCKET ("AcceptEX not supported !");

		THROW_TEXT ("AcceptEX not supported !");
	}

	std::unique_ptr<ServerSocket> accept_socket (new ServerSocket);

	// каст к dword-у надо будет поправить
	// TODO: data_length сделать не равной 0 (принятие handshak-a)

	BOOL accept_result = lpfnAcceptEx (m_socket, *accept_socket.get (), accept_packet.m_buffer, 0, sizeof (sockaddr_in) + 16, sizeof (sockaddr_in) + 16, (LPDWORD) &accept_packet.m_size, &accept_packet.m_overlapped_packet);

	if (accept_result == FALSE)
	{
		if (::WSAGetLastError () != ERROR_IO_PENDING)
		{
			TRACE_SOCKET ("AcceptEX failed !");

			THROW_TEXT ("AcceptEX failed !");
		}
	}
	else
	{
		_ASSERT (HasOverlappedIoCompleted (&accept_packet.m_overlapped_packet));
		_ASSERT (WAIT_OBJECT_0 == ::WaitForSingleObject (accept_packet.m_overlapped_packet.hEvent, 0));
	}

	return accept_socket;
}


std::string ServerSocket::GetRemoteAddress ()
{
	std::ostringstream addres_str;

	addres_str << (int) ((SOCKADDR_IN*) (&m_remote_addr))->sin_addr.S_un.S_un_b.s_b1 << ".";
	addres_str << (int) ((SOCKADDR_IN*) (&m_remote_addr))->sin_addr.S_un.S_un_b.s_b2 << ".";
	addres_str << (int) ((SOCKADDR_IN*) (&m_remote_addr))->sin_addr.S_un.S_un_b.s_b3 << ".";
	addres_str << (int) ((SOCKADDR_IN*) (&m_remote_addr))->sin_addr.S_un.S_un_b.s_b4;
	
	return addres_str.str ();
}


std::string ServerSocket::GetRemotePort ()
{
	std::ostringstream port_str;

	port_str << ((SOCKADDR_IN*) (&m_remote_addr))->sin_port;

	return port_str.str ();
}


void ServerSocket::OnAccept (char* accept_address_buffer, size_t accept_address_buffer_legth)
{
	LPSOCKADDR lp_local = 0;
	LPSOCKADDR lp_remote = 0;
	INT local_length = 0;
	INT remote_length = 0;

	GUID Guidgaesa = WSAID_GETACCEPTEXSOCKADDRS;
	LPFN_GETACCEPTEXSOCKADDRS lpfngaesa = NULL;
	DWORD dwBytes;

	int ioctl_result = ::WSAIoctl (m_socket, SIO_GET_EXTENSION_FUNCTION_POINTER,
		&Guidgaesa, sizeof (Guidgaesa),
		&lpfngaesa, sizeof (lpfngaesa),
		&dwBytes, 0, 0);
	
	if (ioctl_result == SOCKET_ERROR)
	{
		TRACE_SOCKET ("GAESA not supported !");

		THROW_TEXT ("GAESA not supported !");
	}

	lpfngaesa (accept_address_buffer, 0, sizeof (sockaddr_in) + 16, sizeof (sockaddr_in) + 16, &lp_local, &local_length, &lp_remote, &remote_length);

	memcpy (&m_local_addr, lp_local, local_length);
	memcpy (&m_remote_addr, lp_remote, remote_length);
}


////////////////////////////////////////////////////////////////////////////////////////////////////


ClientSocket::ClientSocket ()
{
}


ClientSocket::~ClientSocket ()
{
}


void ClientSocket::Connect (unsigned __int32 be_server_address, int server_port)
{
	sockaddr_in connect_info;

	connect_info.sin_family = AF_INET;
	connect_info.sin_addr.S_un.S_addr = be_server_address;
	connect_info.sin_port = htons (server_port);

	int connect_result = connect (m_socket, (sockaddr*)&connect_info, sizeof (connect_info));

	if (connect_result == SOCKET_ERROR)
	{
		TRACE_SOCKET ("Connect failed !");

		THROW_TEXT ("Connect failed !");
	}
}
