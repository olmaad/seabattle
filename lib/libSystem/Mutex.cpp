#include "Mutex.h"
#include "SeaExceptions.h"

#include <crtdbg.h>


Mutex::Mutex ()
{
	HANDLE new_mutex = CreateMutex (0, FALSE, NULL);

	if (!new_mutex)
	{
		THROW_TEXT ("Can't create mutex !");
	}

	m_mutex = new_mutex;
}

Mutex::~Mutex ()
{
	int closehandle_return = CloseHandle (m_mutex);

	_ASSERT (closehandle_return);
}

void Mutex::Acquire ()
{
	DWORD wait_result = WaitForSingleObject (m_mutex, INFINITE);

	_ASSERT (WAIT_OBJECT_0 == wait_result);
}

void Mutex::Release ()
{
	BOOL release_result = ReleaseMutex (m_mutex);

	_ASSERT (release_result == TRUE);
}



AutoLocker::AutoLocker (Mutex& mutex) : m_mutex (mutex)
{
	m_mutex.Acquire ();
}

AutoLocker::~AutoLocker ()
{
	m_mutex.Release ();
}
