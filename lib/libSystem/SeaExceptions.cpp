#include "SeaExceptions.h"

BasicException::BasicException ()
{}

BasicException::~BasicException ()
{}

TextException::TextException ()
{
	BasicException ();

	m_text = L"<no text>";
}

TextException::TextException (std::wstring text)
{
	BasicException ();

	m_text = text;
}

std::wstring TextException::ToString ()
{
	return m_text;
}
