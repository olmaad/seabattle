#include "Thread.h"

#include "SeaExceptions.h"
#include "Printer.h"

NotificationEvent::NotificationEvent ()
{
	m_event = ::CreateEvent (0, TRUE, FALSE, 0);

	if (!m_event)
	{
		THROW_TEXT ("Can't create event !");
	}
}

NotificationEvent::~NotificationEvent ()
{
	::CloseHandle (m_event);
}

void NotificationEvent::Set ()
{
	::SetEvent (m_event);
}

void NotificationEvent::Reset ()
{
	::ResetEvent (m_event);
}

DWORD WINAPI Thread::TreadProc (LPVOID lp_parameter)
{
	_ASSERT (lp_parameter);

	if (!lp_parameter)
	{
		TRACE_TEXT ("lp_parameter is null !");

		return 2;
	}

	Thread* that = (Thread*) lp_parameter;

	try
	{
		that->Run ();
	}
	catch (...)
	{
		TRACE_TEXT_CONTEXT ("Exception in TreadProc !", lp_parameter);

		_ASSERT (0);

		{
			AutoLocker auto_locker (that->m_state_mutex);

			that->m_run_state = rs_ended;
		}

		return 1;
	}

	{
		AutoLocker auto_locker (that->m_state_mutex);

		that->m_run_state = rs_ended;
	}

	return 0;
}


Thread::Thread () : m_run_state (rs_not_running)
{
	m_thread = ::CreateThread (0, 0, TreadProc, this, CREATE_SUSPENDED, &m_thread_id);

	if (!m_thread)
	{
		THROW_TEXT ("Can't create thread !");
	}
}


Thread::~Thread()
{
	{
		AutoLocker auto_locker (m_state_mutex);

		if (m_run_state == rs_not_running)
		{
			::CloseHandle (m_thread);
			return;
		}
	}

	StopAsync ();

	::WaitForSingleObject (m_thread, INFINITE);

	::CloseHandle (m_thread);
}


void Thread::StartAsync ()
{
	AutoLocker auto_locker (m_state_mutex);

	if (m_run_state != rs_not_running)
	{
		TRACE_TEXT_CONTEXT ("Thread already started !", this);
		return;
	}

	m_run_state = rs_running;

	::ResumeThread (m_thread);
}


void Thread::StopAsync ()
{
	AutoLocker auto_locker (m_state_mutex);

	if (m_run_state != rs_running)
	{
		TRACE_TEXT_CONTEXT ("Thread not started !", this);
		return;
	}


	m_run_state = rs_stopped;

	m_stop_event.Set ();
}


void Thread::WaitForThreadFinished ()
{
	::WaitForSingleObject (m_thread, INFINITE);
}
