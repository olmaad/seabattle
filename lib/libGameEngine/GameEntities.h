#pragma once

typedef __int32 eid_t;

enum GameEntityType
{
	ge_torpedo = 0,
	ge_rocket,
	ge_maac,
	ge_emp,
	ge_firgter_drone,
	ge_prospector_drone,
	ge_barrage_fire
};

struct GameEntity
{

};
