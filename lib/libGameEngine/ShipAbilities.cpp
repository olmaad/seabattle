#include "ShipAbilities.h"

ShipAbility::ShipAbility(ShipAbilityType type) :
	m_type(type)
{}

ShipAbility::~ShipAbility()
{}

ShotShipAbility::ShotShipAbility(AbsBoardPos pos) :
	ShipAbility(sa_shot),
	m_pos(pos)
{}

void ShotShipAbility::Accept(IShipAbilityHandler& handler)
{
	handler.Handle(*this);
}

MoveShipAbility::MoveShipAbility(AbsBoardPos from, AbsBoardPos to) :
	ShipAbility(sa_move),
	m_from(from),
	m_to(to)
{}

void MoveShipAbility::Accept(IShipAbilityHandler& handler)
{
	handler.Handle(*this);
}

TorpedoShipAbility::TorpedoShipAbility(AbsBoardPos entry_pos) :
	ShipAbility(sa_torpedo),
	m_entry_pos(entry_pos)
{}

void TorpedoShipAbility::Accept(IShipAbilityHandler& handler)
{
	handler.Handle(*this);
}

RocketShipAbility::RocketShipAbility(AbsBoardPos pos) :
	ShipAbility(sa_rocket),
	m_pos(pos)
{}

void RocketShipAbility::Accept(IShipAbilityHandler& handler)
{
	handler.Handle(*this);
}

MAACShipAbility::MAACShipAbility(AbsBoardPos pos) :
	ShipAbility(sa_maac),
	m_pos(pos)
{}

void MAACShipAbility::Accept(IShipAbilityHandler& handler)
{
	handler.Handle(*this);
}

IllusionShipAbility::IllusionShipAbility(AbsBoardPos pos, IllusionDirection direction) :
	ShipAbility(sa_illusion),
	m_pos(pos),
	m_direction(direction)
{}

void IllusionShipAbility::Accept(IShipAbilityHandler& handler)
{
	handler.Handle(*this);
}

EMPShipAbility::EMPShipAbility(AbsBoardPos pos) :
	ShipAbility(sa_emp),
	m_pos(pos)
{}

void EMPShipAbility::Accept(IShipAbilityHandler& handler)
{
	handler.Handle(*this);
}

ScoutingShipAbility::ScoutingShipAbility(AbsBoardPos pos) :
	ShipAbility(sa_scouting),
	m_pos(pos)
{}

void ScoutingShipAbility::Accept(IShipAbilityHandler& handler)
{
	handler.Handle(*this);
}

FighterDroneShipAbility::FighterDroneShipAbility(AbsBoardPos entry_pos) :
	ShipAbility(sa_fighter_drone),
	m_entry_pos(entry_pos)
{}

void FighterDroneShipAbility::Accept(IShipAbilityHandler& handler)
{
	handler.Handle(*this);
}

InterceptorDroneShipAbility::InterceptorDroneShipAbility(eid_t target) :
	ShipAbility(sa_interceptor_drone),
	m_target(target)
{}

void InterceptorDroneShipAbility::Accept(IShipAbilityHandler& handler)
{
	handler.Handle(*this);
}

ProspectorDroneShipAbility::ProspectorDroneShipAbility(AbsBoardPos entry_pos) :
	ShipAbility(sa_prospector_drone),
	m_entry_pos(entry_pos)
{}

void ProspectorDroneShipAbility::Accept(IShipAbilityHandler& handler)
{
	handler.Handle(*this);
}

ProspectorDroneUpdateShipAbility::ProspectorDroneUpdateShipAbility(AbsBoardPos pos) :
	ShipAbility(sa_prospector_drone_update),
	m_pos(pos)
{}

void ProspectorDroneUpdateShipAbility::Accept(IShipAbilityHandler& handler)
{
	handler.Handle(*this);
}

CarpetBombingShipAbility::CarpetBombingShipAbility(AbsBoardPos pos) :
	ShipAbility(sa_carpet_bombing),
	m_pos(pos)
{}

void CarpetBombingShipAbility::Accept(IShipAbilityHandler& handler)
{
	handler.Handle(*this);
}

BarrageFireShipAbility::BarrageFireShipAbility(AbsBoardPos pos) :
	ShipAbility(sa_barrage_fire),
	m_pos(pos)
{}

void BarrageFireShipAbility::Accept(IShipAbilityHandler& handler)
{
	handler.Handle(*this);
}

FullCombatReadinessShipAbility::FullCombatReadinessShipAbility() :
	ShipAbility(sa_full_combat_readiness)
{}

void FullCombatReadinessShipAbility::Accept(IShipAbilityHandler& handler)
{
	handler.Handle(*this);
}
