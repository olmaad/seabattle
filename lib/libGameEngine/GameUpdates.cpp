#include "GameUpdates.h"

GameUpdateBase::GameUpdateBase(GameUpdateType type) :
	m_type(type)
{}

ShotResultUpdate::ShotResultUpdate(FireResult result, AbsBoardPos pos) :
	GameUpdateBase(gu_shot_result),
	m_result(result),
	m_pos(pos)
{}

void ShotResultUpdate::Accept(IGameUpdateHandler& handler)
{
	handler.Handle(*this);
}

ScoutingResultUpdate::ScoutingResultUpdate() :
	GameUpdateBase(gu_scouting_result)
{}

void ScoutingResultUpdate::Accept(IGameUpdateHandler& handler)
{
	handler.Handle(*this);
}

ProspectorDroneUpdate::ProspectorDroneUpdate() :
	GameUpdateBase(gu_prospector_drone)
{}

void ProspectorDroneUpdate::Accept(IGameUpdateHandler& handler)
{
	handler.Handle(*this);
}

CarpetBombingResultUpdate::CarpetBombingResultUpdate() :
	GameUpdateBase(gu_carpet_bombing_result)
{}

void CarpetBombingResultUpdate::Accept(IGameUpdateHandler& handler)
{
	handler.Handle(*this);
}
