#include "GameEnginePrinter.h"

#include <libSystem/SeaExceptions.h>

GameEnginePrinter::GameEnginePrinter(const std::shared_ptr<Printer>& printer) :
	m_printer(printer)
{
	_ASSERT(m_printer);

	if (!m_printer)
	{
		throw InvalidArgumentException(L"GameEnginePrinter: Printer is null");
	}
}

void GameEnginePrinter::WarshipAdded(std::wstring alias, WarShipPrototypePtr prototype, AbsBoardPos warship_pos)
{
	std::wostringstream print_text;

	print_text << L"WA:" << alias << L":X" << warship_pos.x << L":Y" << warship_pos.y << L":" << L"P" << prototype->GetPoints().size() << L":";

	for each (RelBoardPos counter in prototype->GetPoints())
	{
		print_text << L"\r\n" << counter.x << L"," << counter.y;
	}

	print_text << L";\r\n";

	m_printer->PrintLine(print_text.str());
}

void GameEnginePrinter::GameStarted(const GameBoard& gameboard, std::wstring alias)
{
	std::wostringstream print_text;

	int gameboard_size = gameboard.GetSize();

	print_text << L"GS:" << alias << L":S" << gameboard_size << L":PGB:";

	for (int y_counter = 0; y_counter < gameboard_size; y_counter = y_counter + 1)
	{
		print_text << L"\r\n" << GetGameboardLine(gameboard, y_counter);
	}

	print_text << L";\r\n";

	m_printer->PrintLine(print_text.str());
}

void GameEnginePrinter::MoveActed(std::wstring alias, AbsBoardPos pos, FireResult result)
{
	std::wostringstream print_text;

	print_text << L"M:" << alias << L":X" << pos.x << L":Y" << pos.y << L":";

	switch (result)
	{
	case fr_hit:
		print_text << L"H";
		break;
	case fr_miss:
		print_text << L"M";
		break;
	case fr_kill:
		print_text << L"K";
		break;
	}

	print_text << L";\r\n";

	m_printer->PrintLine(print_text.str());
}

void GameEnginePrinter::GameEnded(std::wstring player_alias, std::wstring winner_alias, const GameBoard& my_gameboard, const GameBoard& enemy_gameboard)
{
	std::wostringstream print_text;

	int gameboard_size = my_gameboard.GetSize();

	print_text << L"GE:" << player_alias << L":W:" << winner_alias << L":S:" << gameboard_size << L":PGB:";

	for (int y_counter = 0; y_counter < gameboard_size; y_counter = y_counter + 1)
	{
		print_text << L"\r\n" << GetGameboardLine(my_gameboard, y_counter);
	}

	print_text << L":EGB:";

	for (int y_counter = 0; y_counter < gameboard_size; y_counter = y_counter + 1)
	{
		print_text << L"\r\n" << GetGameboardLine(enemy_gameboard, y_counter);
	}

	print_text << L";\r\n";

	m_printer->PrintLine(print_text.str());
}

std::wstring GameEnginePrinter::GetGameboardLine(const GameBoard& gameboard, int line)
{
	std::wostringstream print_text;

	int gameboard_size = gameboard.GetSize();

	for (int x_counter = 0; x_counter < gameboard_size; x_counter = x_counter + 1)
	{
		gameboard.Cell(AbsBoardPos(x_counter, line)).cell_type;

		switch (gameboard.Cell(AbsBoardPos(x_counter, line)).cell_type)
		{
		case ct_unknown:
			print_text << L".";
			break;
		case ct_empty:
			print_text << L"0";
			break;
		case ct_ship:
			print_text << L"S";
			break;
		case ct_damaged:
			print_text << L"D";
			break;
		case ct_miss:
			print_text << L"-";
			break;
		case ct_conflict:
			print_text << L"C";
			break;
		case ct_killed:
			print_text << L"K";
			break;
		default:
			_ASSERT(0);
		}

		if (x_counter != gameboard_size - 1)
		{
			print_text << L",";
		}
	}

	return print_text.str();
}
