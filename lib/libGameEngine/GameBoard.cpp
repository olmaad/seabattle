#include "GameBoard.h"

size_t WarShip::warship_counter = 0;
size_t WarShipPrototype::prototype_counter = 0;

WarShipPrototype::WarShipPrototype (const RelBoardPosList& src_points, const int src_limit) : m_points (src_points.begin (), src_points.end ()), m_limit (src_limit), m_allocated (0)
{
	prototype_counter = prototype_counter +1;
}

WarShipPrototype::WarShipPrototype () : m_limit (0), m_allocated (0)
{
	m_points.push_back (RelBoardPos (0, 0));

	prototype_counter = prototype_counter +1;
}

WarShipPrototype::~WarShipPrototype ()
{
	prototype_counter = prototype_counter -1;

	_ASSERT (0 == m_allocated);
}

// Calculate allocated warships (+ allocate)
void WarShipPrototype::AllocateShip ()
{
	_ASSERT (m_allocated <= m_limit);

	if (m_allocated < m_limit)
	{
		m_allocated = m_allocated +1;
		return;
	}

	THROW_TEXT ("Ship limit exceeded !");
}


//// Calculate allocated warships (- allocate)

void WarShipPrototype::DeAllocateShip ()
{
	_ASSERT (m_allocated <= m_limit);
	_ASSERT (0 < m_allocated);

	m_allocated = m_allocated -1;
}


//// Get warshipprototype's point list

const RelBoardPosList& WarShipPrototype::GetPoints () const
{
	return m_points;
}


//// Add point to prototype (for "edit")

bool WarShipPrototype::AddPoint (RelBoardPos cell_pos)
{
	if (m_points.end () == std::find (m_points.begin (), m_points.end (), cell_pos))
	{
		m_points.push_back (cell_pos);

		return true;
	}

	return false;
}


//// Delete point with known location fronm prototype (for editing exciting warships)

void WarShipPrototype::DelPoint (RelBoardPos cell_pos)
{
	m_points.remove (cell_pos);
}


//// Sort prototype points

RelBoardPos WarShipPrototype::Sort ()
{
	RelBoardPos minimal_coordinate (0, 0);

	for (RelBoardPosList::iterator counter = m_points.begin ();counter != m_points.end ();counter ++)
	{
		if (counter->x < minimal_coordinate.x)
		{
			minimal_coordinate.x = counter->x;
		}
		if (counter->y < minimal_coordinate.y)
		{
			minimal_coordinate.y = counter->y;
		}
	}

	for (RelBoardPosList::iterator counter = m_points.begin ();counter != m_points.end ();counter ++)
	{
		*counter = *counter - minimal_coordinate;
	}

	m_points.sort ();

	return minimal_coordinate;
}


////

WarShipPrototypePtr WarShipPrototype::Clone () const
{
	return WarShipPrototypePtr (new WarShipPrototype (m_points, m_limit));
}


//// WarShip ////////////////////////////////////////////////////////////////////////////////////////////////////


//// Constructor

WarShip::WarShip (WarShipPrototypePtr src_prototype, const AbsBoardPos& src_pos) : m_prototype (src_prototype), m_pos (src_pos)
{
	m_prototype->AllocateShip ();

	warship_counter = warship_counter +1;
}


//// Destructor

WarShip::~WarShip ()
{
	m_prototype->DeAllocateShip ();

	warship_counter = warship_counter -1;
}


//// GameBoard ////////////////////////////////////////////////////////////////////////////////////////////////////


//// Constructor

GameBoard::GameBoard () : m_size (0), m_board_storage (0), m_controller (0), m_ships_counter (0)
{
}


//// Destructor

GameBoard::~GameBoard ()
{
	_ASSERT (m_controller == 0);

	delete [] m_board_storage;
}


//// If warship fits to gameboard

bool GameBoard::PlaceTest (const WarShipPrototypePtr prototype, const AbsBoardPos& pos) const
{
	RelBoardPosList list = prototype->GetPoints ();

	for (RelBoardPosList::const_iterator counter = list.begin ();counter != list.end ();counter ++)
	{
		AbsBoardPos abs_point_pos (pos.x + counter->x, pos.y + counter->y);

		if ((abs_point_pos.x >= m_size) || (abs_point_pos.x < 0))
		{
			return false;
		}

		if ((abs_point_pos.y >= m_size) || (abs_point_pos.y < 0))
		{
			return false;
		}

		if (Cell (abs_point_pos).cell_type != ct_empty)
		{
			return false;
		}

		for (int x = -1;x <= 1;x = x +1)
		{
			for (int y = -1;y <= 1;y = y +1)
			{
				if ((abs_point_pos.x + x >= m_size) || (abs_point_pos.x + x < 0))
				{
					continue;
				}
				if ((abs_point_pos.y + y >= m_size) || (abs_point_pos.y + y < 0))
				{
					continue;
				}

				if (Cell (AbsBoardPos (abs_point_pos.x + x, abs_point_pos.y + y)).cell_type != ct_empty)
				{
					return false;
				}
			}
		}
	}

	return true;
}


//// PlaceTest + adding warship to warship_list + set cheks in gameboard to "ct_ship"

void GameBoard::AddWarShip (WarShipPrototypePtr adding_warship, const AbsBoardPos& adding_warship_pos)
{
	if (!PlaceTest (adding_warship, adding_warship_pos))
	{
		THROW_TEXT ("Unable to place warship !");
	}

	AbsBoardPos right_warship_pos = adding_warship_pos;

	WarShipPtr new_warship (new WarShip (adding_warship, right_warship_pos));

	m_ships_list.push_back (new_warship);

	m_ships_counter = m_ships_counter +1;

	RelBoardPosList list = adding_warship->GetPoints ();

	if (m_controller)
	{
		m_controller->NotifyWarshipCountChanged ();
	}

	for (RelBoardPosList::const_iterator counter = list.begin ();counter != list.end ();counter ++)
	{
		AbsBoardPos abs_point_pos = right_warship_pos + *counter;

		BoardCell& cell = Cell (abs_point_pos);
		cell.cell_type = ct_ship;
		cell.ship = new_warship.get ();
	}
}


////

template<typename t_element, typename t_list, typename t_list_iterator>
t_list_iterator FindPtrInAutoPtrList (t_element* ptr, t_list& list)
{
	for (t_list::iterator counter = list.begin ();counter != list.end ();counter ++)
	{
		if (ptr == counter->get ())
		{
			return counter;
		}
	}

	return list.end ();
}


////

void GameBoard::DelWarShip (const AbsBoardPos& delete_pos)
{
	WarShip* deleting_warship = Cell (delete_pos).ship;
	AbsBoardPos deleting_warship_pos = deleting_warship->GetPos ();

	WarShipPtrList::iterator deleting_warship_ptr = FindPtrInAutoPtrList<WarShip, WarShipPtrList, WarShipPtrList::iterator> (deleting_warship, m_ships_list);

	_ASSERT (m_ships_list.end () != deleting_warship_ptr);

	m_ships_counter = m_ships_counter -1;

	RelBoardPosList list = deleting_warship->GetPrototype ()->GetPoints ();

	for (RelBoardPosList::const_iterator counter = list.begin ();counter != list.end ();counter ++)
	{
		AbsBoardPos abs_point_pos = deleting_warship_pos + *counter;

		BoardCell& cell = Cell (abs_point_pos);
		cell.cell_type = ct_empty;
		cell.ship = 0;
	}

	if (m_controller)
	{
		m_controller->NotifyWarshipCountChanged ();
	}

	if (deleting_warship->GetPrototype ()->GetAllocated () == 1 && m_controller->IsHost ())
	{
		DelPrototype (deleting_warship->GetPrototype ());
	}

	m_ships_list.erase (deleting_warship_ptr);
}


//// Get cell contetnt (const variant)

const BoardCell& GameBoard::Cell (const AbsBoardPos& pos) const
{
	if ((pos.x < 0) || (pos.x >= m_size) || (pos.y < 0) || (pos.y >= m_size))
	{
		THROW_TEXT ("Invalid cell position !");
	}

	return *((const BoardCell*) m_board_storage + pos.x * m_size + pos.y);
}


//// Get cell contetnt

BoardCell& GameBoard::Cell (const AbsBoardPos& pos)
{
	if ((pos.x < 0) || (pos.x >= m_size) || (pos.y < 0) || (pos.y >= m_size))
	{
		THROW_TEXT ("Invalid cell position !");
	}

	return *((BoardCell*) m_board_storage + pos.x * m_size + pos.y);
}


//// Compare prototypes

bool ArePrototypesEqual (const WarShipPrototypePtr src_prototype, const WarShipPrototypePtr prototype)
{
	const RelBoardPosList& src_points = src_prototype->GetPoints ();
	const RelBoardPosList& points = prototype->GetPoints ();

	RelBoardPosList::const_iterator src_counter = src_points.begin ();
	RelBoardPosList::const_iterator counter = points.begin ();

	for (;src_counter != src_points.end () && counter != points.end ();)
	{
		if (*src_counter != *counter)
		{
			return false;
		}

		src_counter ++;
		counter ++;
	}

	return src_counter == src_points.end () && counter == points.end ();
}


//// Find equvival prototypes

WarShipPrototypeList::const_iterator GameBoard::FindEqualPrototype (const WarShipPrototypePtr src_prototype)
{
	WarShipPrototypeList::const_iterator counter = m_prototype_list.begin ();

	for (;counter != m_prototype_list.end ();counter ++)
	{
		if (ArePrototypesEqual (src_prototype, *counter))
		{
			return counter;
		}
	}

	return counter;
}


//// Add new prototype to prototype list

const WarShipPrototypePtr& GameBoard::AddPrototype (WarShipPrototypePtr adding_prototype, RelBoardPos* offset)
{
	*offset = adding_prototype->Sort ();

	WarShipPrototypeList::const_iterator find_result = FindEqualPrototype (adding_prototype);

	if (m_prototype_list.end () == find_result)
	{
		m_prototype_list.push_back (adding_prototype);

		if (m_controller)
		{
			m_controller->NotifyPrototypeAdded (adding_prototype.get ());
		}

		return m_prototype_list.back ();
	}

	return *find_result;
}


////

void GameBoard::DelPrototype (WarShipPrototypePtr deleting_prototype)
{
	const WarShipPrototype* dead_pointer = deleting_prototype.get ();

	WarShipPrototypeList::iterator deleting_prototype_ptr = FindPtrInAutoPtrList<WarShipPrototype, WarShipPrototypeList, WarShipPrototypeList::iterator> (deleting_prototype.get (), m_prototype_list);

	_ASSERT (m_prototype_list.end () != deleting_prototype_ptr);

	m_prototype_list.erase (deleting_prototype_ptr);

	if (m_controller)
	{
		m_controller->NotifyPrototypeDeleted (dead_pointer);
	}
}


//// Give prototype list from external source

void GameBoard::SetAllowedWarShips (const WarShipPrototypeList& allowed_warships)
{
	if (0 == allowed_warships.size ())
	{
		THROW_TEXT ("Warships list is empty !");
	}

	if (0 != m_prototype_list.size ())
	{
		THROW_TEXT ("Warships list already set !");
	}

	for (WarShipPrototypeList::const_iterator counter = allowed_warships.cbegin ();counter != allowed_warships.cend ();counter ++)
	{
		m_prototype_list.push_back ((*counter)->Clone ());
	}
}


//// Create gameboard

void GameBoard::CreateAndFill (int size, CellType fill)
{
	_ASSERT (0 == m_board_storage);
	_ASSERT (0 != size);
	_ASSERT (0 == m_size);

	if ((0 != m_board_storage) || (0 != m_size))
	{
		THROW_TEXT ("Board already exists !");
	}

	if ((size < min_size) || (size > max_size))
	{
		THROW_TEXT ("Unallowed board size !");
	}

	size_t storage_size = size * size * sizeof (BoardCell);

	m_board_storage = new char[storage_size];
	memset (m_board_storage, 0, storage_size);

	//пока забъем ^

	m_size = size;

	for (int x = 0;x < size;x = x +1)
	{
		for (int y = 0;y < size;y = y +1)
		{
			Cell (AbsBoardPos (x, y)).cell_type = fill;
		}
	}
}


//// CreateAndFill with ct_empty

void GameBoard::CreateEmpty (int size)
{
	CreateAndFill (size, ct_empty);
}


//// CreateAndFill with ct_unknown

void GameBoard::CreateEnemyUnknown (int size)
{
	CreateAndFill (size, ct_unknown);
}


//// Get gameboard size

int GameBoard::GetSize () const
{
	return m_size;
}


////

const WarShipPrototypeList& GameBoard::GetPrototypeList ()
{
	return m_prototype_list;
}

const WarShipPtrList& GameBoard::GetWarshipList () const
{
	return m_ships_list;
}


//// Mark miss (on enemy gameboard)

void GameBoard::MarkMiss (const AbsBoardPos& hit_pos)
{
	BoardCell& cell_to_mark = Cell (hit_pos);

	if (ct_unknown != cell_to_mark.cell_type)
	{
		THROW_TEXT ("Cell not unknown !");
	}

	cell_to_mark.cell_type = ct_empty;
}


////

void GameBoard::RecurseOpenEmpty (AbsBoardPos pos)
{
	for (int x = -1;x <= 1;x = x +1)
	{
		for (int y = -1;y <= 1;y = y +1)
		{
			if ((pos.x + x >= m_size) || (pos.x + x < 0))
			{
				continue;
			}
			if ((pos.y + y >= m_size) || (pos.y + y < 0))
			{
				continue;
			}

			AbsBoardPos real_pos (pos.x + x, pos.y + y);
			BoardCell& selected_cell = Cell (real_pos);

			if (selected_cell.cell_type == ct_unknown)
			{
				selected_cell.cell_type = ct_empty;
			}
			if (selected_cell.cell_type == ct_damaged)
			{
				selected_cell.cell_type = ct_killed;

				RecurseOpenEmpty (real_pos);
			}
		}
	}
}


////

void GameBoard::MarkKill (const AbsBoardPos& hit_pos)
{
	RecurseOpenEmpty (hit_pos);
}


//// 

void GameBoard::MarkHit (const AbsBoardPos& hit_pos)
{
	BoardCell& cell_to_mark = Cell (hit_pos);

	if (ct_unknown != cell_to_mark.cell_type)
	{
		THROW_TEXT ("Cell not unknown !");
	}

	cell_to_mark.cell_type = ct_damaged;
}


////

FireResult GameBoard::EnemyHit (const AbsBoardPos& hit_pos)
{
	BoardCell& hitted_cell = Cell (hit_pos);

	if (hitted_cell.cell_type == ct_empty || hitted_cell.cell_type == ct_damaged)
	{
		hitted_cell.cell_type = ct_miss;

		return fr_miss;
	}

	if (hitted_cell.cell_type == ct_ship)
	{
		hitted_cell.cell_type = ct_damaged;

		WarShip* ship = hitted_cell.ship;

		_ASSERT (ship);

		const RelBoardPosList& list = ship->GetPrototype ()->GetPoints ();
		const AbsBoardPos& pos = ship->GetPos ();

		bool killed = true;

		for (RelBoardPosList::const_iterator counter = list.begin ();counter != list.end ();counter ++)
		{
			if (Cell (pos + *counter).cell_type != ct_damaged)
			{
				killed = false;
				break;
			}
		}

		if (killed)
		{
			m_ships_counter = m_ships_counter -1;

			return fr_kill;
		}
		
		return fr_hit;
	}

	THROW_TEXT ("Unexpected cell type !");
}


void GameBoard::CloneGameboard (GameBoard& src_gameboarg)
{
	if (m_size != src_gameboarg.m_size)
	{
		THROW_TEXT ("Invalid gameboard !");
	}

	for (WarShipPrototypeList::iterator counter = src_gameboarg.m_prototype_list.begin ();counter != src_gameboarg.m_prototype_list.end ();counter ++)
	{
		m_prototype_list.push_back (WarShipPrototypePtr (new WarShipPrototype (counter->get ()->GetPoints (), counter->get ()->GetLimit ())));
	}

	for (WarShipPtrList::iterator counter = src_gameboarg.m_ships_list.begin ();counter != src_gameboarg.m_ships_list.end ();counter ++)
	{
		WarShipPrototypeList::const_iterator my_prototype = FindEqualPrototype (counter->get ()->GetPrototype ());

		if (my_prototype == m_prototype_list.end ())
		{
			THROW_TEXT ("Invalid prototype !");
		}

		m_ships_list.push_back (WarShipPtr (new WarShip (*my_prototype, counter->get ()->GetPos ())));
	}
}


bool GameBoard::ConsumeGameboard (GameBoard& src_gameboarg)
{
	bool cheat_undetected = true;

	if (m_size != src_gameboarg.m_size)
	{
		THROW_TEXT ("Invalid gameboard !");
	}

	CloneGameboard (src_gameboarg);

	for (int x = 0;x < m_size;x = x +1)
	{
		for (int y = 0;y < m_size;y = y +1)
		{
			AbsBoardPos board_pos (x, y);

			BoardCell& shadow_cell = Cell (board_pos);
			BoardCell& enemy_cell = src_gameboarg.Cell (board_pos);

			if (shadow_cell.cell_type == ct_damaged || shadow_cell.cell_type == ct_killed)
			{
				if (enemy_cell.cell_type != ct_damaged && enemy_cell.cell_type != ct_killed && enemy_cell.cell_type != ct_ship)
				{
					shadow_cell.cell_type = ct_conflict;
					cheat_undetected = false;
				}
			}
			if (shadow_cell.cell_type == ct_empty)
			{
				if (enemy_cell.cell_type != ct_miss && enemy_cell.cell_type != ct_empty)
				{
					shadow_cell.cell_type = ct_conflict;
					cheat_undetected = false;
				}
			}

			if (enemy_cell.cell_type == ct_ship && shadow_cell.cell_type == ct_unknown)
			{
				shadow_cell.cell_type = ct_ship;
			}
		}
	}

	return cheat_undetected;
}
