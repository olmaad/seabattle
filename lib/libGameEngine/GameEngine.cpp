#include "GameEngine.h"

#include "InternalMessage.h"
#include "PraySelector.h"
#include "GameEnginePrinter.h"

#include <libSystem/Mutex.h>
#include <libSystem/Thread.h>

#include <math.h>
#include <queue>

#ifdef _DEBUG
#define TRACE_GAMEENGINE(_text) \
{ \
	std::ostringstream trace_text; \
	trace_text << std::dec << __LINE__ << ":" << __FUNCTION__ << ":" << debug_tag << "\"" << GameStateToString(m_game_state) << "\"" << ": " << _text << "\n"; \
	OutputDebugStringA (trace_text.str().c_str()); \
}
#else
#define TRACE_GAMEENGINE(_text)
#endif

const char* GameStateToString(GameState gamestate)
{
	switch (gamestate)
	{
	case gs_not_ready: return "NOT READY";
	case gs_waiting_for_start: return "WAITING FOR START";
	case gs_my_move: return "MY MOVE";
	case gs_my_move_wait_for_response: return "WAIT FOR RESPONSE";
	case gs_enemy_move: return "ENEMY MOVE";
	case gs_win: return "WIN";
	case gs_fail: return "FAIL";
	case gs_canceled: return "CANCELED";
	case gs_final: return "FINAL";
	default: return "UNKNOWN";
	}
}

class GameEngine :
	public IGameControllerSide,
	public IGameEngineSide,
	public IShipAbilityHandler,
	public IGameUpdateHandler,
	public IGameEngineManage,
	public IInternalMessageHandler
{
public:

	GameEngine(std::wstring alias, std::shared_ptr<Printer> log_printer, bool am_server);
	~GameEngine();

	// IGameControllerSide
	void IStartGame() override;
	void IFire(AbsBoardPos hit_pos) override;
	void IUseAbility(ShipAbilityPtr ability) override;
	void IResign() override;
	void ISetGameParameters(std::shared_ptr<GameBoard> my_gameboard, std::shared_ptr<GameBoard> enemy_shadow_gameboard, bool am_first) override;
	void IGetGameParameters(int& gameboard_size, bool& am_first, WarShipPrototypeList& prototypes) override;
	std::wstring IGetEnemyAlias() override;
	std::wstring IGetMyAlias() override;

	// IGameEngineSide
	void GetGameboardParameters(int& gameboard_size, bool& is_server_moves_first, WarShipPrototypeList& warships) override;
	void ConnectionEstablished() override;
	void StartGame(gen_t event_number) override;
	void EndGame(gen_t event_number, EndGameState enemy_state, std::shared_ptr<GameBoard> enemy_gameboard) override;
	void FireRequest(gen_t event_number, AbsBoardPos hit_pos) override;
	void FireResponse(gen_t event_number, FireResult fire_result, gen_t request_number) override;
	void UseAbility(gen_t event_number, ShipAbilityPtr ability) override;
	void GameUpdate(gen_t event_number, GameUpdatePtr update) override;
	void MoveEnded() override;
	std::wstring GetAlias() override;

	// IShipAbilityHandler
	void Handle(ShotShipAbility&) override;
	void Handle(MoveShipAbility&) override;
	void Handle(TorpedoShipAbility&) override;
	void Handle(RocketShipAbility&) override;
	void Handle(MAACShipAbility&) override;
	void Handle(IllusionShipAbility&) override;
	void Handle(EMPShipAbility&) override;
	void Handle(ScoutingShipAbility&) override;
	void Handle(FighterDroneShipAbility&) override;
	void Handle(InterceptorDroneShipAbility&) override;
	void Handle(ProspectorDroneShipAbility&) override;
	void Handle(ProspectorDroneUpdateShipAbility&) override;
	void Handle(CarpetBombingShipAbility&) override;
	void Handle(BarrageFireShipAbility&) override;
	void Handle(FullCombatReadinessShipAbility&) override;

	// IGameUpdateHandler
	void Handle(const ShotResultUpdate&) override;
	void Handle(const ScoutingResultUpdate&) override;
	void Handle(const ProspectorDroneUpdate&) override;
	void Handle(const CarpetBombingResultUpdate&) override;

	// IGameEngineManage
	void ConnectOtherSide(IGameEngineSide* other_side_interface) override;
	void ConnectGameController(IGameControllerNotify* game_controller_interface) override;
	void ConnectNetworkNotify(IControllerNetNotify* game_controller_net_notify_interface) override;
	void Ready() override;
	void Unready() override;

	// IInternalMessageHandler
	void Handle(IMControllerFire&) override;
	void Handle(IMControllerUseAbility&) override;
	void Handle(IMControllerResign&) override;
	void Handle(IMEnemyStartGame&) override;
	void Handle(IMEnemyEndGame&) override;
	void Handle(IMEnemyFireRequest&) override;
	void Handle(IMEnemyFireResponse&) override;
	void Handle(IMEnemyUseAbility&) override;
	void Handle(IMEnemyGameUpdate&) override;
	void Handle(IMEnemyEndMove&) override;
	void Handle(IMMyStartGame&) override;
	void Handle(IMMyEndGame&) override;
	void Handle(IMMyFireRequest&) override;
	void Handle(IMMyFireResponse&) override;
	void Handle(IMMyUseAbility&) override;
	void Handle(IMMyGameUpdate&) override;
	void Handle(IMMyEndMove&) override;

#ifdef _DEBUG
	std::string debug_tag;
#endif

private:
	IGameControllerNotify* m_game_controller_interface;
	IGameEngineSide* m_other_side_interface;
	IControllerNetNotify* m_game_controller_net_notify_interface;

	std::shared_ptr<GameBoard> m_my_gameboard;
	std::shared_ptr<GameBoard> m_enemy_shadow_gameboard;

	Mutex m_game_state_mutex;
	GameState m_game_state;
	bool m_am_first;
	bool m_am_server;
	std::wstring m_my_alias;
	int m_enemy_warships_left;

	class PumpThread : public Thread
	{
	public:
		PumpThread(GameEngine& owner);

		void Run();

	private:
		GameEngine& m_owner;
	};

	void PostInternal(std::shared_ptr<InternalMessage> message);

	template<typename T> void Post(T& message)
	{
		std::shared_ptr<InternalMessage> message_copy = std::make_shared<T>(message);

		PostInternal(message_copy);
	}

	std::queue<std::shared_ptr<InternalMessage>> m_queue;
	NotificationEvent m_event_queue_not_empty;

	gen_t m_event_counter;
	gen_t m_enemy_last_event_number;

	AbsBoardPos m_last_hit_pos;
	gen_t m_last_hit_request_number;

	PumpThread m_pump_thread;

	std::shared_ptr<GameEnginePrinter> m_log_printer;

};

void GameEngine::Handle(IMControllerFire& message)
{
	TRACE_GAMEENGINE("IMControllerFire::Entered");

	AutoLocker game_status_locker(m_game_state_mutex);

	_ASSERT(m_my_gameboard.get());
	_ASSERT(m_enemy_shadow_gameboard.get());

	if (m_game_state != gs_my_move)
	{
		_ASSERT(0);

		return;
	}

	_ASSERT(m_other_side_interface);

	if (m_enemy_shadow_gameboard->Cell(message.m_hit_pos).cell_type != ct_unknown)
	{
		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"Make your move"));
		}

		return;
	}

	m_game_state = gs_my_move_wait_for_response;

	if (m_game_controller_interface)
	{
		m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"Wait for answer"));
	}

	m_last_hit_pos = message.m_hit_pos;
	m_last_hit_request_number = ++m_event_counter;

	Post(IMMyFireRequest(m_last_hit_request_number, message.m_hit_pos));
}

void GameEngine::Handle(IMControllerUseAbility& message)
{
	TRACE_GAMEENGINE("IMControllerUseAbility::Entered");

	AutoLocker game_status_locker(m_game_state_mutex);

	_ASSERT(m_my_gameboard);
	_ASSERT(m_enemy_shadow_gameboard);
	_ASSERT(m_other_side_interface);

	if (m_game_state != gs_my_move)
	{
		_ASSERT(0);

		return;
	}

	switch (message.m_ability->m_type)
	{
	case sa_shot:
	{
		auto ability_typed = std::static_pointer_cast<ShotShipAbility>(message.m_ability);

		if (m_enemy_shadow_gameboard->Cell(ability_typed->m_pos).cell_type != ct_unknown)
		{
			if (m_game_controller_interface)
			{
				m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"Make your move"));
			}

			return;
		}

		m_game_state = gs_my_move_wait_for_response;

		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"Wait for answer"));
		}

		break;
	}
	default:
	{
		THROW_TEXT("Unsupported ability type !");
		break;
	}
	}

	m_last_hit_request_number = ++m_event_counter;

	Post(IMMyUseAbility(m_last_hit_request_number, message.m_ability));
}

void GameEngine::Handle(IMControllerResign& message)
{
	TRACE_GAMEENGINE("IMControllerResign::Entered");

	AutoLocker game_status_locker(m_game_state_mutex);

	_ASSERT(m_my_gameboard.get());
	_ASSERT(m_enemy_shadow_gameboard.get());

	if (m_game_state != gs_my_move && m_game_state != gs_enemy_move)
	{
		_ASSERT(0);

		return;
	}

	m_game_state = gs_fail;

	Post(IMMyEndGame(++m_event_counter, egs_fail, m_my_gameboard));

	if (m_game_controller_interface)
	{
		m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"You have resigned"));
	}
}

void GameEngine::Handle(IMEnemyStartGame& message)
{
	TRACE_GAMEENGINE("IMEnemyStartGame::Entered");

	AutoLocker game_status_locker(m_game_state_mutex);

	_ASSERT(m_enemy_last_event_number < message.m_event_number);
	_ASSERT(m_my_gameboard.get());
	_ASSERT(m_enemy_shadow_gameboard.get());

	m_log_printer->GameStarted(*m_my_gameboard.get(), m_my_alias);

	m_enemy_last_event_number = message.m_event_number;

	if (m_am_server && m_game_state == gs_waiting_for_start)
	{
		if (m_am_first)
		{
			m_game_state = gs_my_move;

			if (m_game_controller_interface)
			{
				m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"Make your move"));
			}
		}
		else
		{
			m_game_state = gs_enemy_move;

			if (m_game_controller_interface)
			{
				m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"Waiting for other player move"));
			}
		}
	}
	else
	{
		m_game_state = gs_canceled;

		Post(IMMyEndGame(++m_event_counter, egs_canceled, m_my_gameboard));

		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"Other player cheats. Game ended, bro ;)"));
		}
	}

	m_game_controller_interface->NotifyGameStarted();
}

void GameEngine::Handle(IMEnemyEndGame& message)
{
	TRACE_GAMEENGINE("IMEnemyEndGame::Entered");

	AutoLocker game_status_locker(m_game_state_mutex);

	_ASSERT(m_enemy_last_event_number < message.m_event_number || message.m_enemy_state == egs_canceled);

	_ASSERT(message.m_enemy_gameboard.get() || message.m_enemy_state == egs_canceled);

	m_enemy_last_event_number = message.m_event_number;

	if (m_game_state == gs_win && message.m_enemy_state == egs_fail)
	{
		//m_game_state = gs_final;

		if (!m_enemy_shadow_gameboard->ConsumeGameboard(*message.m_enemy_gameboard))
		{
			if (m_game_controller_interface)
			{
				CONCAT_TEXT1_ALIAS_TEXT2(L"You win ! ", L" cheats ! See red marks");

				m_game_controller_interface->UpdGameState(m_game_state, upd_gs_string);
			}
		}
		else
		{
			if (m_game_controller_interface)
			{
				m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"You win !"));
			}
		}

		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdEnemyGB();
			m_game_controller_interface->UpdMyGB();
		}

		return;
	}

	if (m_game_state == gs_fail && message.m_enemy_state == egs_win)
	{
		//m_game_state = gs_final;

		if (!m_enemy_shadow_gameboard->ConsumeGameboard(*message.m_enemy_gameboard))
		{
			if (m_game_controller_interface)
			{
				CONCAT_TEXT1_ALIAS_TEXT2(L"You fail ! ", L" cheats ! See red marks");

				m_game_controller_interface->UpdGameState(m_game_state, upd_gs_string);
			}
		}
		else
		{
			if (m_game_controller_interface)
			{
				m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"You fail !"));
			}
		}

		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdEnemyGB();
			m_game_controller_interface->UpdMyGB();
		}

		return;
	}

	if (message.m_enemy_state == egs_fail && (m_game_state == gs_enemy_move || m_game_state == gs_my_move))
	{
		//m_game_state = gs_final;

		if (!m_enemy_shadow_gameboard->ConsumeGameboard(*message.m_enemy_gameboard))
		{
			if (m_game_controller_interface)
			{
				CONCAT_TEXT1_ALIAS_TEXT2(L"You win ! ", L" cheats ! See red marks");

				m_game_controller_interface->UpdGameState(m_game_state, upd_gs_string);
			}
		}
		else
		{
			if (m_game_controller_interface)
			{
				CONCAT_TEXT1_ALIAS_TEXT2(L"You win ! ", L" has resigned !");

				m_game_controller_interface->UpdGameState(gs_win, upd_gs_string);
				//m_game_controller_interface->UpdGameState (m_game_state, L"");
			}
		}

		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdEnemyGB();
			m_game_controller_interface->UpdMyGB();
		}

		Post(IMMyEndGame(++m_event_counter, egs_win, m_my_gameboard));

		return;
	}

	if (message.m_enemy_state == egs_canceled)
	{
		//m_game_state = gs_final;

		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdGameState(gs_canceled, std::wstring(L"Game aborted"));
			//m_game_controller_interface->UpdGameState (m_game_state, L"");
			m_game_controller_interface->UpdMyGB();
		}

		return;
	}

	if (m_game_state == gs_canceled)
	{
		//m_game_state = gs_final;

		m_enemy_shadow_gameboard->ConsumeGameboard(*message.m_enemy_gameboard);

		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdMyGB();
			m_game_controller_interface->UpdEnemyGB();
			m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"Game aborted"));
		}

		return;
	}

	m_enemy_shadow_gameboard->ConsumeGameboard(*message.m_enemy_gameboard);

	//m_game_state = gs_final;

	if (m_game_controller_interface)
	{
		m_game_controller_interface->UpdMyGB();
		m_game_controller_interface->UpdEnemyGB();

		CONCAT_TEXT1_ALIAS_TEXT2(L"Game aborted. ", L" cheats !");

		m_game_controller_interface->UpdGameState(gs_canceled, upd_gs_string);
	}

	m_log_printer->GameEnded(
		m_my_alias,
		(m_game_state == gs_win) ? (m_my_alias) : (m_other_side_interface->GetAlias()),
		*m_my_gameboard.get(),
		*m_enemy_shadow_gameboard.get());
}

void GameEngine::Handle(IMEnemyFireRequest& message)
{
	TRACE_GAMEENGINE("IMEnemyFireRequest::Entered");

	AutoLocker game_status_locker(m_game_state_mutex);

	_ASSERT(m_enemy_last_event_number < message.m_event_number);
	_ASSERT(m_my_gameboard.get());
	_ASSERT(m_enemy_shadow_gameboard.get());

	m_enemy_last_event_number = message.m_event_number;

	if (m_game_state != gs_enemy_move)
	{
		m_game_state = gs_final;

		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"Game aborted. Unexpected fire !"));
		}

		Post(IMMyEndGame(++m_event_counter, egs_canceled, m_my_gameboard));

		return;
	}

	BoardCell& hitted_cell = m_my_gameboard->Cell(message.m_hit_pos);

	FireResult fire_result = m_my_gameboard->EnemyHit(message.m_hit_pos);

	m_log_printer->MoveActed(m_other_side_interface->GetAlias(), message.m_hit_pos, fire_result);

	switch (fire_result)
	{
	case fr_miss:
	{
		m_game_state = gs_my_move;

		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdMyGB();
			m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"Your move"));
		}
		break;
	}
	case fr_hit:
	{
		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdMyGB();
		}

		break;
	}
	case fr_kill:
	{
		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdMyGB();
		}

		if (!m_my_gameboard->GetWarshipCount())
		{
			m_game_state = gs_fail;

			if (m_game_controller_interface)
			{
				m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"You fail !"));
			}

			Post(IMMyFireResponse(++m_event_counter, fire_result, message.m_event_number));
			Post(IMMyEndGame(++m_event_counter, egs_fail, m_my_gameboard));

			return;
		}

		break;
	}
	default:
		_ASSERT(0);

		break;
	}

	Post(IMMyFireResponse(++m_event_counter, fire_result, message.m_event_number));
}

void GameEngine::Handle(IMEnemyFireResponse& message)
{
	TRACE_GAMEENGINE("IMEnemyFireResponse::Entered");

	AutoLocker game_status_locker(m_game_state_mutex);

	_ASSERT(m_enemy_last_event_number < message.m_event_number);
	_ASSERT(m_my_gameboard.get());
	_ASSERT(m_enemy_shadow_gameboard.get());

	m_enemy_last_event_number = message.m_event_number;

	if (m_game_state != gs_my_move_wait_for_response || message.m_request_number != m_last_hit_request_number)
	{
		m_game_state = gs_final;

		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"Game aborted. Unexpected response !"));
		}

		Post(IMMyEndGame(++m_event_counter, egs_canceled, m_my_gameboard));

		return;
	}

	switch (message.m_fire_result)
	{
	case fr_miss:
		m_enemy_shadow_gameboard->MarkMiss(m_last_hit_pos);

		m_game_state = gs_enemy_move;

		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdEnemyGB();



			m_game_controller_interface->UpdGameState(m_game_state, m_other_side_interface->GetAlias() + L" makes a move");
		}
		break;
	case fr_hit:
		m_enemy_shadow_gameboard->MarkHit(m_last_hit_pos);

		m_game_state = gs_my_move;

		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdEnemyGB();
			m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"Nice shot ! Make your move"));
		}
		break;
	case fr_kill:
	{
		m_enemy_shadow_gameboard->MarkHit(m_last_hit_pos);
		m_enemy_shadow_gameboard->MarkKill(m_last_hit_pos);
		m_enemy_warships_left = m_enemy_warships_left - 1;

		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdEnemyGB();
		}

		if (m_enemy_warships_left == 0)
		{
			m_game_state = gs_win;

			Post(IMMyEndGame(++m_event_counter, egs_win, m_my_gameboard));

			if (m_game_controller_interface)
			{
				m_game_controller_interface->UpdEnemyGB();
				m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"You win !"));
			}
		}
		else
		{
			m_game_state = gs_my_move;

			//m_enemy_shadow_gameboard->MarkKill (m_last_hit_pos);

			if (m_game_controller_interface)
			{
				m_game_controller_interface->UpdEnemyGB();
				m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"You kill enemy warship ! Next move please"));
			}
		}

		break;
	}
	}
}

void GameEngine::Handle(IMEnemyUseAbility& message)
{
	TRACE_GAMEENGINE("IMEnemyAbilityRequest::Entered");

	message.m_ability->Accept(*this);
}

void GameEngine::Handle(IMEnemyGameUpdate& message)
{
	TRACE_GAMEENGINE("IMEnemyGameUpdate::Entered");

	message.m_update->Accept(*this);
}

void GameEngine::Handle(IMEnemyEndMove& message)
{
	TRACE_GAMEENGINE("IMEnemyEndMove::Entered");

	_ASSERT(0);
}

void GameEngine::Handle(IMMyStartGame& message)
{
	TRACE_GAMEENGINE("IMMyStartGame::Entered");

	_ASSERT(!m_am_server);

	AutoLocker game_status_locker(m_game_state_mutex);

	m_log_printer->GameStarted(*m_my_gameboard.get(), m_my_alias);

	if (m_am_first)
	{
		m_game_state = gs_my_move;
		m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"Make your move"));
	}
	else
	{
		m_game_state = gs_enemy_move;

		CONCAT_TEXT1_ALIAS_TEXT2(L"Waiting for ", L" move");

		m_game_controller_interface->UpdGameState(m_game_state, upd_gs_string);
	}

	m_other_side_interface->StartGame(++m_event_counter);
}

void GameEngine::Handle(IMMyEndGame& message)
{
	TRACE_GAMEENGINE("IMMyEndGame::Entered");

	m_other_side_interface->EndGame(message.m_event_number, message.m_my_state, message.m_my_gameboard);
}

void GameEngine::Handle(IMMyFireRequest& message)
{
	TRACE_GAMEENGINE("IMMyFireRequest::Entered");

	m_other_side_interface->FireRequest(message.m_event_number, message.m_hit_pos);
}

void GameEngine::Handle(IMMyFireResponse& message)
{
	TRACE_GAMEENGINE("IMMyFireResponse::Entered");

	m_other_side_interface->FireResponse(message.m_event_number, message.m_fire_result, message.m_request_number);
}

void GameEngine::Handle(IMMyUseAbility& message)
{
	TRACE_GAMEENGINE("IMMyAbilityRequest::Entered");

	m_other_side_interface->UseAbility(message.m_event_number, message.m_ability);
}

void GameEngine::Handle(IMMyGameUpdate& message)
{
	TRACE_GAMEENGINE("IMMyGameUpdate::Entered");

	m_other_side_interface->GameUpdate(message.m_event_number, message.m_update);
}

void GameEngine::Handle(IMMyEndMove& message)
{
	TRACE_GAMEENGINE("IMMyEndMove::Entered");

	m_other_side_interface->MoveEnded();
}

GameEngine::PumpThread::PumpThread(GameEngine& owner) :
	m_owner(owner)
{}

void GameEngine::PumpThread::Run()
{
	TRACE_TEXT_CONTEXT("Entering thread", this);

	for (;;)
	{
		try
		{
			HANDLE handle_array[2] = { m_stop_event.GetHandle(), m_owner.m_event_queue_not_empty.GetHandle() };

			DWORD wait_return = ::WaitForMultipleObjects(2, handle_array, FALSE, INFINITE);

			_ASSERT(wait_return == WAIT_OBJECT_0 || wait_return == WAIT_OBJECT_0 + 1);

			if (wait_return == WAIT_OBJECT_0 && !m_owner.m_queue.size()) //Добавил ожидание потока на опустошение очереди
			{
				TRACE_TEXT_CONTEXT("Thread stop event set but queue not empty", this);
				break;
			}

			std::shared_ptr<InternalMessage> message_ptr;

			{
				AutoLocker locker(m_owner.m_game_state_mutex);

				message_ptr = m_owner.m_queue.front();
				m_owner.m_queue.pop();

				if (!m_owner.m_queue.size())
				{
					m_owner.m_event_queue_not_empty.Reset();
				}
			}

			message_ptr->Visit(m_owner);
		}
		catch (std::exception& e)
		{
			TRACE_TEXT_CONTEXT(e.what(), this);
		}
		catch (InvalidTargetSearchResultException& e)
		{
			TRACE_TEXT_CONTEXT(e.ToString().c_str(), this);
		}
		catch (...)
		{
			TRACE_TEXT_CONTEXT("Exception caught", this);
		}
	}

	TRACE_TEXT_CONTEXT("Exiting thread", this);
}

GameEngine::GameEngine(std::wstring alias, std::shared_ptr<Printer> log_printer, bool am_server) :
	m_game_state(gs_not_ready),
	m_am_server(am_server),
	m_my_alias(alias),
	m_event_counter(0),
	m_enemy_last_event_number(0),
	m_last_hit_pos(-1, -1),
	m_pump_thread(*this),
	m_game_controller_interface(0),
	m_other_side_interface(0)
{
	m_log_printer = std::make_shared<GameEnginePrinter>(log_printer);

#ifdef _DEBUG

	if (am_server)
	{
		debug_tag = "server";
	}
	else
	{
		debug_tag = "client";
	}

#endif
}


////

GameEngine::~GameEngine()
{
	_ASSERT(!m_game_controller_interface && !m_other_side_interface);
}


void GameEngine::PostInternal(std::shared_ptr<InternalMessage> message)
{
	//AutoLocker locker (m_game_state_mutex);
	// ^ mutex getted anyway

	bool queue_was_empty = (m_queue.size() == 0);

	m_queue.push(message);

	if (queue_was_empty)
	{
		m_event_queue_not_empty.Set();
	}
}

void GameEngine::IStartGame()
{
	Post(IMMyStartGame());
}

void GameEngine::IFire(AbsBoardPos hit_pos)
{
	Post(IMControllerFire(hit_pos));
}

void GameEngine::IUseAbility(ShipAbilityPtr ability)
{
	Post(IMControllerUseAbility(ability));
}

void GameEngine::IResign()
{
	Post(IMControllerResign());
}

void GameEngine::ISetGameParameters(std::shared_ptr<GameBoard> my_gameboard, std::shared_ptr<GameBoard> enemy_shadow_gameboard, bool am_first)
{
	AutoLocker game_status_locker(m_game_state_mutex);

	_ASSERT(my_gameboard.get());
	_ASSERT(!m_my_gameboard.get());
	_ASSERT(!m_enemy_shadow_gameboard.get());
	_ASSERT(m_game_state < gs_my_move);

	m_my_gameboard = my_gameboard;
	m_enemy_warships_left = my_gameboard->GetWarshipCount();
	m_am_first = am_first;

	m_enemy_shadow_gameboard = enemy_shadow_gameboard;

	for each (WarShipPtr counter in m_my_gameboard->GetWarshipList())
	{
		m_log_printer->WarshipAdded(m_my_alias, counter->GetPrototype(), counter->GetPos());
	}
}


void GameEngine::IGetGameParameters(int& gameboard_size, bool& am_first, WarShipPrototypeList& prototypes)
{
	_ASSERT(!m_am_server);

	m_other_side_interface->GetGameboardParameters(gameboard_size, am_first, prototypes);

	am_first = !am_first;
}


std::wstring GameEngine::IGetEnemyAlias()
{
	return m_other_side_interface->GetAlias();
}


std::wstring GameEngine::IGetMyAlias()
{
	return m_my_alias;
}


void GameEngine::ConnectGameController(IGameControllerNotify* game_controller_interface)
{
	AutoLocker game_status_locker(m_game_state_mutex);

	m_game_controller_interface = game_controller_interface;
}

void GameEngine::ConnectNetworkNotify(IControllerNetNotify* game_controller_net_notify_interface)
{
	AutoLocker game_status_locker(m_game_state_mutex);

	m_game_controller_net_notify_interface = game_controller_net_notify_interface;
}

void GameEngine::GetGameboardParameters(int& gameboard_size, bool& is_server_moves_first, WarShipPrototypeList& warships)
{
	gameboard_size = m_my_gameboard->GetSize();

	is_server_moves_first = (m_am_server == m_am_first);

	WarShipPrototypeList my_prototype_list = m_my_gameboard->GetPrototypeList();

	for (WarShipPrototypeList::iterator counter = my_prototype_list.begin(); counter != my_prototype_list.end(); counter++)
	{
		warships.push_back(WarShipPrototypePtr(new WarShipPrototype(counter->get()->GetPoints(), counter->get()->GetLimit())));
	}
}

void GameEngine::ConnectionEstablished()
{
	if (m_am_server)
	{
		_ASSERT(0);

		THROW_TEXT("Trying connect server-server. Vi vse eshe jivi ?");
	}

	AutoLocker game_status_locker(m_game_state_mutex);

	m_game_state = gs_waiting_for_start;
	m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"Connected to server"));
}

void GameEngine::StartGame(gen_t EventNumber)
{
	Post(IMEnemyStartGame(EventNumber));
}

void GameEngine::Ready()
{
	TRACE_GAMEENGINE("Entered");

	AutoLocker game_status_locker(m_game_state_mutex);

	if (m_game_state != gs_not_ready)
	{
		THROW_TEXT("Unexpected initialization ! O.o");
	}

	_ASSERT(m_game_controller_interface && m_other_side_interface);

	if (m_am_server)
	{
		m_game_state = gs_waiting_for_start;

		CONCAT_TEXT1_ALIAS_TEXT2(L"Waiting for ", L" to place ships");

		m_game_controller_interface->UpdGameState(m_game_state, upd_gs_string);
	}

	m_pump_thread.StartAsync();
}


void GameEngine::Unready()
{
	TRACE_GAMEENGINE("Entered");

	m_pump_thread.StopAsync();

	m_pump_thread.WaitForThreadFinished();
}


void GameEngine::EndGame(gen_t EventNumber, EndGameState enemy_state, std::shared_ptr<GameBoard> enemy_gameboard)
{
	TRACE_GAMEENGINE("Entered");

	if (!enemy_gameboard.get() && enemy_state != egs_canceled)
	{
		THROW_TEXT("Invalid enemy gameboard !");
	}

	Post(IMEnemyEndGame(EventNumber, enemy_state, enemy_gameboard));
}

void GameEngine::FireRequest(gen_t EventNumber, AbsBoardPos hit_pos)
{
	TRACE_GAMEENGINE("Entered");

	Post(IMEnemyFireRequest(EventNumber, hit_pos));
}

void GameEngine::FireResponse(gen_t EventNumber, FireResult fire_result, gen_t RequestNumber)
{
	TRACE_GAMEENGINE("Entered");

	Post(IMEnemyFireResponse(EventNumber, fire_result, RequestNumber));
}

void GameEngine::UseAbility(gen_t EventNumber, ShipAbilityPtr ability)
{
	TRACE_GAMEENGINE("Entered");

	Post(IMEnemyUseAbility(EventNumber, ability));
}

void GameEngine::GameUpdate(gen_t event_number, GameUpdatePtr update)
{
	TRACE_GAMEENGINE("Entered");

	Post(IMEnemyGameUpdate(event_number, update));
}

void GameEngine::MoveEnded()
{
	TRACE_GAMEENGINE("Entered");

	_ASSERT(0);
}

std::wstring GameEngine::GetAlias()
{
	AutoLocker game_status_locker(m_game_state_mutex);

	std::wstring return_alias = m_my_alias;

	return return_alias;
}

void GameEngine::Handle(ShotShipAbility& ability)
{
	TRACE_GAMEENGINE("ShotShipAbility::Entered");

	AutoLocker game_status_locker(m_game_state_mutex);

	// TODO: move to IM handler
	//_ASSERT(m_enemy_last_event_number < message.m_event_number);
	_ASSERT(m_my_gameboard.get());
	_ASSERT(m_enemy_shadow_gameboard.get());

	//m_enemy_last_event_number = message.m_event_number;

	if (m_game_state != gs_enemy_move)
	{
		m_game_state = gs_final;

		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"Game aborted. Unexpected enemy turn !"));
		}

		Post(IMMyEndGame(++m_event_counter, egs_canceled, m_my_gameboard));

		return;
	}

	BoardCell& hitted_cell = m_my_gameboard->Cell(ability.m_pos);

	FireResult fire_result = m_my_gameboard->EnemyHit(ability.m_pos);

	m_log_printer->MoveActed(m_other_side_interface->GetAlias(), ability.m_pos, fire_result);

	switch (fire_result)
	{
	case fr_miss:
	{
		m_game_state = gs_my_move;

		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdMyGB();
			m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"Your move"));
		}
		break;
	}
	case fr_hit:
	{
		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdMyGB();
		}

		break;
	}
	case fr_kill:
	{
		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdMyGB();
		}

		if (!m_my_gameboard->GetWarshipCount())
		{
			m_game_state = gs_fail;

			if (m_game_controller_interface)
			{
				m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"You fail !"));
			}

			//Post(IMMyFireResponse(++m_event_counter, fire_result, message.m_event_number));
			Post(IMMyGameUpdate(++m_event_counter, std::make_shared<ShotResultUpdate>(fire_result, ability.m_pos)));
			Post(IMMyEndGame(++m_event_counter, egs_fail, m_my_gameboard));

			return;
		}

		break;
	}
	default:
	{
		_ASSERT(0);

		break;
	}
	}

	Post(IMMyGameUpdate(++m_event_counter, std::make_shared<ShotResultUpdate>(fire_result, ability.m_pos)));
}

void GameEngine::Handle(MoveShipAbility& ability)
{
	TRACE_GAMEENGINE("MoveShipAbility::Entered");
}

void GameEngine::Handle(TorpedoShipAbility& ability)
{
	TRACE_GAMEENGINE("TorpedoShipAbility::Entered");
}

void GameEngine::Handle(RocketShipAbility& ability)
{
	TRACE_GAMEENGINE("RocketShipAbility::Entered");
}

void GameEngine::Handle(MAACShipAbility& ability)
{
	TRACE_GAMEENGINE("MAACShipAbility::Entered");
}

void GameEngine::Handle(IllusionShipAbility& ability)
{
	TRACE_GAMEENGINE("IllusionShipAbility::Entered");
}

void GameEngine::Handle(EMPShipAbility& ability)
{
	TRACE_GAMEENGINE("EMPShipAbility::Entered");
}

void GameEngine::Handle(ScoutingShipAbility& ability)
{
	TRACE_GAMEENGINE("ScoutingShipAbility::Entered");
}

void GameEngine::Handle(FighterDroneShipAbility& ability)
{
	TRACE_GAMEENGINE("FighterDroneShipAbility::Entered");
}

void GameEngine::Handle(InterceptorDroneShipAbility& ability)
{
	TRACE_GAMEENGINE("InterceptorDroneShipAbility::Entered");
}

void GameEngine::Handle(ProspectorDroneShipAbility& ability)
{
	TRACE_GAMEENGINE("ProspectorDroneShipAbility::Entered");
}

void GameEngine::Handle(ProspectorDroneUpdateShipAbility& ability)
{
	TRACE_GAMEENGINE("ProspectorDroneUpdateShipAbility::Entered");
}

void GameEngine::Handle(CarpetBombingShipAbility& ability)
{
	TRACE_GAMEENGINE("CarpetBombingShipAbility::Entered");
}

void GameEngine::Handle(BarrageFireShipAbility& ability)
{
	TRACE_GAMEENGINE("BarrageFireShipAbility::Entered");
}

void GameEngine::Handle(FullCombatReadinessShipAbility& ability)
{
	TRACE_GAMEENGINE("FullCombatReadinessShipAbility::Entered");
}

void GameEngine::Handle(const ShotResultUpdate& update)
{
	TRACE_GAMEENGINE("ShotResultUpdate::Entered");

	AutoLocker game_status_locker(m_game_state_mutex);

	//_ASSERT(m_enemy_last_event_number < message.m_event_number);
	_ASSERT(m_my_gameboard.get());
	_ASSERT(m_enemy_shadow_gameboard.get());

	//m_enemy_last_event_number = message.m_event_number;

	if (m_game_state != gs_my_move_wait_for_response/* || message.m_request_number != m_last_hit_request_number*/)
	{
		m_game_state = gs_final;

		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"Game aborted. Unexpected response !"));
		}

		Post(IMMyEndGame(++m_event_counter, egs_canceled, m_my_gameboard));

		return;
	}

	switch (update.m_result)
	{
	case fr_miss:
		m_enemy_shadow_gameboard->MarkMiss(update.m_pos);

		m_game_state = gs_enemy_move;

		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdEnemyGB();



			m_game_controller_interface->UpdGameState(m_game_state, m_other_side_interface->GetAlias() + L" makes a move");
		}
		break;
	case fr_hit:
		m_enemy_shadow_gameboard->MarkHit(update.m_pos);

		m_game_state = gs_my_move;

		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdEnemyGB();
			m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"Nice shot ! Make your move"));
		}
		break;
	case fr_kill:
	{
		m_enemy_shadow_gameboard->MarkHit(update.m_pos);
		m_enemy_shadow_gameboard->MarkKill(update.m_pos);
		m_enemy_warships_left = m_enemy_warships_left - 1;

		if (m_game_controller_interface)
		{
			m_game_controller_interface->UpdEnemyGB();
		}

		if (m_enemy_warships_left == 0)
		{
			m_game_state = gs_win;

			Post(IMMyEndGame(++m_event_counter, egs_win, m_my_gameboard));

			if (m_game_controller_interface)
			{
				m_game_controller_interface->UpdEnemyGB();
				m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"You win !"));
			}
		}
		else
		{
			m_game_state = gs_my_move;

			//m_enemy_shadow_gameboard->MarkKill (m_last_hit_pos);

			if (m_game_controller_interface)
			{
				m_game_controller_interface->UpdEnemyGB();
				m_game_controller_interface->UpdGameState(m_game_state, std::wstring(L"You kill enemy warship ! Next move please"));
			}
		}

		break;
	}
	}
}

void GameEngine::Handle(const ScoutingResultUpdate& update)
{
	TRACE_GAMEENGINE("ScoutingResultUpdate::Entered");
}

void GameEngine::Handle(const ProspectorDroneUpdate& update)
{
	TRACE_GAMEENGINE("ProspectorDroneUpdate::Entered");
}

void GameEngine::Handle(const CarpetBombingResultUpdate& update)
{
	TRACE_GAMEENGINE("CarpetBombingResultUpdate::Entered");
}

void GameEngine::ConnectOtherSide(IGameEngineSide* other_side_interface)
{
	AutoLocker game_status_locker(m_game_state_mutex);

	m_other_side_interface = other_side_interface;
}

std::shared_ptr<IGameEngineManage> CreateGameEngine(const std::wstring& alias, bool is_server, std::shared_ptr<Printer> log_printer, IGameControllerSide*& game_controller_side, IGameEngineSide*& game_engine_side)
{
	GameEngine* new_game_engine = new GameEngine(alias, log_printer, is_server);

	std::shared_ptr<IGameEngineManage> new_game_engine_smartptr(new_game_engine);

	game_controller_side = new_game_engine;
	game_engine_side = new_game_engine;

	return new_game_engine_smartptr;
}

// Emulator
class GameEmulator : public IGameEngineSide, public IGameEngineManage, IGameControllerNotify
{
public:
	GameEmulator(const WarShipPrototypeList& prototype_list, int gameboard_size, bool is_first, bool is_server, std::shared_ptr<Printer> log_printer, EmulatorDifficulty difficulty);
	~GameEmulator();

	void GetGameboardParameters(int& gameboard_size, bool& is_server_moves_first, WarShipPrototypeList& warships) override;
	void ConnectionEstablished() override;
	void StartGame(gen_t event_number) override;
	void EndGame(gen_t event_number, EndGameState enemy_state, std::shared_ptr<GameBoard> enemy_gameboard) override;
	void FireRequest(gen_t event_number, AbsBoardPos hit_pos) override;
	void FireResponse(gen_t event_number, FireResult fire_result, gen_t request_number) override;
	void UseAbility(gen_t event_number, ShipAbilityPtr ability) override;
	void GameUpdate(gen_t event_number, GameUpdatePtr update) override;
	void MoveEnded() override;
	std::wstring GetAlias()override;

	void ConnectOtherSide(IGameEngineSide* other_side_interface) override;
	void ConnectGameController(IGameControllerNotify* game_controller_interface) override;
	void ConnectNetworkNotify(IControllerNetNotify* game_controller_net_notify_interface) override;
	void Ready() override;
	void Unready() override;

private:
	std::shared_ptr<GameBoard> m_my_gameboard;
	std::shared_ptr<GameBoard> m_shadow_enemy_gameboard;

	bool m_am_first;
	bool m_am_server;

	bool m_is_first_turn;

	void RandomPlaceShips();

	AbsBoardPos SelectPrey();

	void UpdGameState(GameState state, const std::wstring& reason);
	void UpdEnemyGB();
	void UpdMyGB();
	void NotifyGameStarted() {}

	AbsBoardPos m_last_hit_pos; // это полноценная вторая сторона для gameengine
	AbsBoardPos m_last_succesful_pos;

	IGameControllerSide* m_game_controller_side;
	IGameEngineSide* m_game_engine_side;

	IControllerNetNotify* m_game_controller_net_notify_interface;

	EmulatorDifficulty m_difficulty;

	std::shared_ptr<IGameEngineManage> m_game_engine;

	std::shared_ptr<IPraySelector> m_pray_selector;
};


void GameEmulator::RandomPlaceShips()
{
	const WarShipPrototypeList& list = m_my_gameboard->GetPrototypeList();
	int gameboard_size = m_my_gameboard->GetSize();

	srand(GetTickCount());

	for (WarShipPrototypeList::const_iterator counter = list.begin(); counter != list.end(); counter++)
	{
		for (int prot_counter = (*counter)->GetLimit(); prot_counter > 0; prot_counter = prot_counter - 1)
		{
			for (int try_counter = 0;; try_counter = try_counter + 1)
			{
				if (try_counter > 1000)
				{
					THROW_TEXT("Can't place ships !");
				}

				AbsBoardPos random_pos(gameboard_size * rand() / RAND_MAX, gameboard_size * rand() / RAND_MAX);

				if (m_my_gameboard->PlaceTest(*counter, random_pos))
				{
					m_my_gameboard->AddWarShip(*counter, random_pos);
					break;
				}
			}
		}
	}
}


AbsBoardPos GameEmulator::SelectPrey()
{
	if (m_difficulty == ed_expert)
	{
		return m_pray_selector->SelectPray(m_shadow_enemy_gameboard);
	}

	int gameboard_size = m_shadow_enemy_gameboard->GetSize();

	for (int try_counter = 0; try_counter < 1000; try_counter = try_counter + 1)
	{
		AbsBoardPos pos(gameboard_size * rand() / RAND_MAX, gameboard_size * rand() / RAND_MAX);

		if (m_shadow_enemy_gameboard->Cell(pos).cell_type == ct_unknown)
		{
			return pos;
		}
	}

	for (int x_counter = 0; x_counter < gameboard_size; x_counter = x_counter + 1)
	{
		for (int y_counter = 0; y_counter < gameboard_size; y_counter = y_counter + 1)
		{
			if (m_shadow_enemy_gameboard->Cell(AbsBoardPos(x_counter, y_counter)).cell_type == ct_unknown)
			{
				return AbsBoardPos(x_counter, y_counter);
			}
		}
	}

	_ASSERT(0);
	throw InvalidTargetSearchResultException(L"gameboard does not contains 'unknown' cells");
}


GameEmulator::GameEmulator(const WarShipPrototypeList& prototype_list, int gameboard_size, bool is_first, bool is_server, std::shared_ptr<Printer> log_printer, EmulatorDifficulty difficulty) :
	m_am_first(is_first),
	m_am_server(is_server),
	m_game_controller_side(0),
	m_game_engine_side(0),
	m_is_first_turn(true),
	m_last_hit_pos(-1, -1),
	m_difficulty(difficulty),
	m_last_succesful_pos(-1, -1)
{
	m_my_gameboard.reset(new GameBoard());
	m_shadow_enemy_gameboard.reset(new GameBoard());

	m_my_gameboard->CreateEmpty(gameboard_size);
	m_shadow_enemy_gameboard->CreateEnemyUnknown(gameboard_size);

	switch (difficulty)
	{
	case ed_noob:
		m_pray_selector = CreateNoobSelector();
		break;
	case ed_expert:
		m_pray_selector = CreateExpertSelector();
		break;
	default:
		break;
	}

	m_my_gameboard->SetAllowedWarShips(prototype_list);
	RandomPlaceShips();

	m_game_engine = CreateGameEngine(std::wstring(L"Bender Rodriguez"), m_am_server, log_printer, m_game_controller_side, m_game_engine_side);

	m_game_controller_side->ISetGameParameters(m_my_gameboard, m_shadow_enemy_gameboard, is_first);

	m_shadow_enemy_gameboard->SetAllowedWarShips(m_my_gameboard->GetPrototypeList());

	_ASSERT(m_game_engine.get() && m_game_controller_side && m_game_engine_side);

	m_game_engine->ConnectGameController(this);
}

GameEmulator::~GameEmulator()
{
	if (m_game_engine.get())
	{
		m_game_engine->ConnectGameController(0);
	}
}


void GameEmulator::GetGameboardParameters(int& gameboard_size, bool& is_server_moves_first, WarShipPrototypeList& warships)
{
	_ASSERT(0);
}


void GameEmulator::ConnectionEstablished()
{
	_ASSERT(0);
}


void GameEmulator::StartGame(gen_t EventNumber)
{
	TRACE_TEXT_CONTEXT("Entered", this);

	m_game_engine_side->StartGame(EventNumber);
}


void GameEmulator::EndGame(gen_t EventNumber, EndGameState enemy_state, std::shared_ptr<GameBoard> enemy_gameboard)
{
	TRACE_TEXT_CONTEXT("Entered", this);

	m_game_engine_side->EndGame(EventNumber, enemy_state, enemy_gameboard);
}


void GameEmulator::FireRequest(gen_t EventNumber, AbsBoardPos hit_pos)
{
	TRACE_TEXT_CONTEXT("Entered", this);

	m_game_engine_side->FireRequest(EventNumber, hit_pos);
}


void GameEmulator::FireResponse(gen_t EventNumber, FireResult fire_result, gen_t RequestNumber)
{
	TRACE_TEXT_CONTEXT("Entered", this);

	m_pray_selector->HitResponce(fire_result, m_last_hit_pos, m_shadow_enemy_gameboard->GetPrototypeList());

	m_game_engine_side->FireResponse(EventNumber, fire_result, RequestNumber);
}

void GameEmulator::UseAbility(gen_t event_number, ShipAbilityPtr ability)
{
	TRACE_TEXT_CONTEXT("Entered", this);

	_ASSERT(0);
}

void GameEmulator::GameUpdate(gen_t event_number, GameUpdatePtr update)
{
	TRACE_TEXT_CONTEXT("Entered", this);

	_ASSERT(0);
}

void GameEmulator::MoveEnded()
{
	TRACE_TEXT_CONTEXT("Entered", this);

	_ASSERT(0);
}

std::wstring GameEmulator::GetAlias()
{
	TRACE_TEXT_CONTEXT("Entered", this);

	return m_game_engine_side->GetAlias();
}


void GameEmulator::ConnectOtherSide(IGameEngineSide* other_side_interface)
{
	m_game_engine->ConnectOtherSide(other_side_interface);
}


void GameEmulator::ConnectGameController(IGameControllerNotify* game_controller_interface)
{
	_ASSERT(0);
}


void GameEmulator::ConnectNetworkNotify(IControllerNetNotify* game_controller_net_notify_interface)
{
	m_game_controller_net_notify_interface = game_controller_net_notify_interface;
}


void GameEmulator::Ready()
{
	TRACE_TEXT_CONTEXT("Entered", this);

	m_game_engine->Ready();

	m_game_controller_side->IStartGame();
}


void GameEmulator::Unready()
{
	TRACE_TEXT_CONTEXT("Entered", this);

	m_game_engine->Unready();
}


std::shared_ptr<IGameEngineManage> CreateEnemyEmulator (const WarShipPrototypeList& prototype_list, int gameboard_size, bool is_first, bool is_server, std::shared_ptr<Printer> log_printer, EmulatorDifficulty difficulty, IGameEngineSide*& game_engine_side)
{
	GameEmulator* new_game_emulator = new GameEmulator (prototype_list, gameboard_size, is_first, is_server, log_printer, difficulty);

	std::shared_ptr<IGameEngineManage> new_game_emulator_smartptr (new_game_emulator);

	game_engine_side = new_game_emulator;

	return new_game_emulator_smartptr;
}


void GameEmulator::UpdGameState (GameState state, const std::wstring& reason)
{
	switch (state)
	{
	case gs_not_ready:
		break;
	case gs_waiting_for_start:
		break;
	case gs_my_move:
		{
			if (m_is_first_turn)
			{
				srand (GetTickCount ());
				m_is_first_turn = false;
			}

			Sleep (300);

			AbsBoardPos prey = SelectPrey ();

			m_last_hit_pos = prey;

			m_game_controller_side->IFire (prey);

			break;
		}
	case gs_my_move_wait_for_response:
		break;
	case gs_enemy_move:
		break;
	case gs_win:
	case gs_fail:
	case gs_canceled:
		{
			if (m_game_controller_net_notify_interface)
			{
				// FIXME: игра полностью завершается (GameProcessEnded) до обработки endgame gameengin-ом окна

				//Sleep (2000);

				m_game_controller_net_notify_interface->GameProcessEnded ();
			}

			break;
		}
	case gs_final:
		break;
	default:
		break;
	}
}

void GameEmulator::UpdEnemyGB ()
{
}

void GameEmulator::UpdMyGB ()
{
}
