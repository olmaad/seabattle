#pragma once

#include "GameEngine.h"

struct IMControllerFire;
struct IMControllerUseAbility;
struct IMControllerResign;
struct IMEnemyStartGame;
struct IMEnemyEndGame;
struct IMEnemyFireRequest;
struct IMEnemyFireResponse;
struct IMEnemyUseAbility;
struct IMEnemyGameUpdate;
struct IMEnemyEndMove;
struct IMMyStartGame;
struct IMMyEndGame;
struct IMMyFireRequest;
struct IMMyFireResponse;
struct IMMyUseAbility;
struct IMMyGameUpdate;
struct IMMyEndMove;

class IInternalMessageHandler
{
public:
	virtual void Handle(IMControllerFire&) = 0;
	virtual void Handle(IMControllerUseAbility&) = 0;
	virtual void Handle(IMControllerResign&) = 0;
	virtual void Handle(IMEnemyStartGame&) = 0;
	virtual void Handle(IMEnemyEndGame&) = 0;
	virtual void Handle(IMEnemyFireRequest&) = 0;
	virtual void Handle(IMEnemyFireResponse&) = 0;
	virtual void Handle(IMEnemyUseAbility&) = 0;
	virtual void Handle(IMEnemyGameUpdate&) = 0;
	virtual void Handle(IMEnemyEndMove&) = 0;
	virtual void Handle(IMMyStartGame&) = 0;
	virtual void Handle(IMMyEndGame&) = 0;
	virtual void Handle(IMMyFireRequest&) = 0;
	virtual void Handle(IMMyFireResponse&) = 0;
	virtual void Handle(IMMyUseAbility&) = 0;
	virtual void Handle(IMMyGameUpdate&) = 0;
	virtual void Handle(IMMyEndMove&) = 0;

};

enum ImType
{
	im_controller_fire = 0,
	im_controller_use_ability,
	im_controller_resign,
	im_enemy_start_game,
	im_enemy_end_game,
	im_enemy_fire_request,
	im_enemy_fire_responce,
	im_enemy_use_ability,
	im_enemy_game_update,
	im_enemy_end_move,
	im_my_start_game,
	im_my_end_game,
	im_my_fire_request,
	im_my_fire_response,
	im_my_use_ability,
	im_my_game_update,
	im_my_end_move
};

struct InternalMessage
{
public:
	explicit InternalMessage(ImType im_type) : m_type(im_type) {}
	virtual ~InternalMessage() {}

	virtual void Visit(IInternalMessageHandler& handler) = 0;

	ImType m_type;
private:
	InternalMessage();

};

struct IMControllerFire : public InternalMessage
{
	explicit IMControllerFire(AbsBoardPos hit_pos) : InternalMessage(im_controller_fire), m_hit_pos(hit_pos) {}

	void Visit(IInternalMessageHandler& handler) override { handler.Handle(*this); }

	AbsBoardPos m_hit_pos;

};

struct IMControllerUseAbility : public InternalMessage
{
	IMControllerUseAbility(ShipAbilityPtr ability) : InternalMessage(im_controller_use_ability), m_ability(ability) {}

	void Visit(IInternalMessageHandler& handler) override { handler.Handle(*this); }

	ShipAbilityPtr m_ability;

};

struct IMControllerResign : public InternalMessage
{
	IMControllerResign() : InternalMessage(im_controller_resign) {}

	void Visit(IInternalMessageHandler& handler) override { handler.Handle(*this); }

};

struct IMEnemyStartGame : public InternalMessage
{
	explicit IMEnemyStartGame(gen_t event_number) : InternalMessage(im_enemy_start_game), m_event_number(event_number) {}

	void Visit(IInternalMessageHandler& handler) override { handler.Handle(*this); }

	int m_event_number;
};

struct IMEnemyEndGame : public InternalMessage
{
	IMEnemyEndGame(gen_t event_number, EndGameState enemy_state, std::shared_ptr<GameBoard> enemy_gameboard) :
		InternalMessage(im_enemy_end_game),
		m_event_number(event_number),
		m_enemy_state(enemy_state),
		m_enemy_gameboard(enemy_gameboard) {}

	void Visit(IInternalMessageHandler& handler) override { handler.Handle(*this); }

	int m_event_number;
	EndGameState m_enemy_state;
	std::shared_ptr<GameBoard> m_enemy_gameboard;

};

struct IMEnemyFireRequest : public InternalMessage
{
	IMEnemyFireRequest(gen_t event_number, AbsBoardPos hit_pos) :
		InternalMessage(im_enemy_fire_request),
		m_event_number(event_number),
		m_hit_pos(hit_pos) {}

	void Visit(IInternalMessageHandler& handler) override { handler.Handle(*this); }

	int m_event_number;
	AbsBoardPos m_hit_pos;

};

struct IMEnemyFireResponse : public InternalMessage
{
	IMEnemyFireResponse(gen_t event_number, FireResult fire_result, int request_number) :
		InternalMessage(im_enemy_fire_responce),
		m_event_number(event_number),
		m_fire_result(fire_result),
		m_request_number(request_number) {}

	void Visit(IInternalMessageHandler& handler) override { handler.Handle(*this); }

	int m_event_number;
	FireResult m_fire_result;
	int m_request_number;

};

struct IMEnemyUseAbility : public InternalMessage
{
	IMEnemyUseAbility(gen_t event_number, ShipAbilityPtr ability) :
		InternalMessage(im_enemy_use_ability),
		m_event_number(event_number),
		m_ability(ability)
	{}

	void Visit(IInternalMessageHandler& handler) override { handler.Handle(*this); }

	int m_event_number;
	ShipAbilityPtr m_ability;

};

struct IMEnemyGameUpdate : public InternalMessage
{
	IMEnemyGameUpdate(gen_t event_number, GameUpdatePtr update) :
		InternalMessage(im_enemy_game_update),
		m_event_number(event_number),
		m_update(update)
	{}

	void Visit(IInternalMessageHandler& handler) override { handler.Handle(*this); }

	int m_event_number;
	GameUpdatePtr m_update;

};

struct IMEnemyEndMove : public InternalMessage
{
	IMEnemyEndMove(gen_t event_number) :
		InternalMessage(im_enemy_end_move),
		m_event_number(event_number)
	{}

	void Visit(IInternalMessageHandler& handler) override { handler.Handle(*this); }

	int m_event_number;

};

struct IMMyStartGame : public InternalMessage
{
	IMMyStartGame() : InternalMessage(im_my_start_game) {}

	void Visit(IInternalMessageHandler& handler) override { handler.Handle(*this); }

};

struct IMMyEndGame : public InternalMessage
{
	IMMyEndGame(gen_t event_number, EndGameState my_state, std::shared_ptr<GameBoard> my_gameboard) :
		InternalMessage(im_my_end_game),
		m_event_number(event_number),
		m_my_state(my_state),
		m_my_gameboard(my_gameboard) {}

	void Visit(IInternalMessageHandler& handler) override { handler.Handle(*this); }

	int m_event_number;
	EndGameState m_my_state;
	std::shared_ptr<GameBoard> m_my_gameboard;

};

struct IMMyFireRequest : public InternalMessage
{
	IMMyFireRequest(gen_t event_number, AbsBoardPos hit_pos) :
		InternalMessage(im_my_fire_request),
		m_event_number(event_number),
		m_hit_pos(hit_pos) {}

	void Visit(IInternalMessageHandler& handler) override { handler.Handle(*this); }

	int m_event_number;
	AbsBoardPos m_hit_pos;

};

struct IMMyFireResponse : public InternalMessage
{
	IMMyFireResponse(gen_t event_number, FireResult fire_result, gen_t request_number) :
		InternalMessage(im_my_fire_response),
		m_event_number(event_number),
		m_fire_result(fire_result),
		m_request_number(request_number) {}

	void Visit(IInternalMessageHandler& handler) override { handler.Handle(*this); }

	int m_event_number;
	FireResult m_fire_result;
	int m_request_number;

};

struct IMMyUseAbility : public InternalMessage
{
	IMMyUseAbility(gen_t event_number, ShipAbilityPtr ability) :
		InternalMessage(im_my_use_ability),
		m_event_number(event_number),
		m_ability(ability)
	{}

	void Visit(IInternalMessageHandler& handler) override { handler.Handle(*this); }

	int m_event_number;
	ShipAbilityPtr m_ability;

};

struct IMMyGameUpdate : public InternalMessage
{
	IMMyGameUpdate(gen_t event_number, GameUpdatePtr update) :
		InternalMessage(im_my_game_update),
		m_event_number(event_number),
		m_update(update)
	{}

	void Visit(IInternalMessageHandler& handler) override { handler.Handle(*this); }

	gen_t m_event_number;
	gen_t m_request_number;
	GameUpdatePtr m_update;

};

struct IMMyEndMove : public InternalMessage
{
	IMMyEndMove(gen_t event_number) :
		InternalMessage(im_my_end_move),
		m_event_number(event_number)
	{}

	gen_t m_event_number;

};

