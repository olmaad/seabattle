#include "GameBoard.h"
#include "PraySelctorInterface.h"

#include <libSystem/Mutex.h>
#include <libSystem/SeaExceptions.h>

#include <vector>
#include <algorithm>

enum PSState
{
	pss_free = 0,
	pss_catched
};

enum LineType
{
	lt_string = 0,
	lt_column
};

struct DamageImage
{
public:
	DamageImage ();

	void ClearImage ();

	WarShipPrototypePtr damage_image;
	AbsBoardPos image_pos;
};

struct TargetLineCell
{
public:
	TargetLineCell (LineType src_line_type, AbsBoardPos src_target_cell);

	LineType line_type;
	AbsBoardPos target_cell;
};

typedef std::vector<TargetLineCell> TargetLineCellVector;

struct TargetCell
{
public:
	TargetCell (AbsBoardPos src_pos, int src_priority);

	bool operator < (const TargetCell& target_cell) const
	{
		return priority < target_cell.priority;
	}
	bool operator == (const AbsBoardPos& rel_pos) const
	{
		return pos == rel_pos;
	}
	TargetCell& operator + (const int& extra_priority)
	{
		priority = priority + extra_priority;

		return *this;
	}

	int priority;
	AbsBoardPos pos;
};


typedef std::vector<TargetCell> TargetCellVector;


class PraySelector : public IPraySelector
{
public:
	PraySelector ();
	~PraySelector ();

	AbsBoardPos SelectPray (std::shared_ptr<GameBoard> enemy_gameboard);

	void HitResponce (FireResult result, AbsBoardPos last_hit_pos, WarShipPrototypeList prototypes);

private:
	AbsBoardPos SelectRandomTarget (std::shared_ptr<GameBoard> enemy_gameboard);
	AbsBoardPos SelectWarshipTarget (std::shared_ptr<GameBoard> enemy_gameboard);

	DamageImage m_damage_image;

	AbsBoardPos m_last_succesful_pos;

	PSState m_state;

	Mutex m_state_mutex;
};

DamageImage::DamageImage () :
	image_pos (InvalidAbsBoardPos),
	damage_image (new WarShipPrototype)
{
}


void DamageImage::ClearImage ()
{
	image_pos = InvalidAbsBoardPos;
	damage_image.reset (new WarShipPrototype);
}


TargetLineCell::TargetLineCell (LineType src_line_type, AbsBoardPos src_target_cell) :
	target_cell (src_target_cell),
	line_type (src_line_type)
{
}


TargetCell::TargetCell (AbsBoardPos src_pos, int src_priority) :
	pos (src_pos),
	priority (src_priority)
{
}


PraySelector::PraySelector () :
	m_state (pss_free),
	m_last_succesful_pos (InvalidAbsBoardPos)
{
	srand (GetTickCount ());
}


PraySelector::~PraySelector ()
{
}


AbsBoardPos PraySelector::SelectPray (std::shared_ptr<GameBoard> enemy_gameboard)
{
	AutoLocker m_state_locker (m_state_mutex);

	AbsBoardPos return_value = InvalidAbsBoardPos;

	if (m_state == pss_catched)
	{
		return SelectWarshipTarget (enemy_gameboard);
	}
	else
	{
		return SelectRandomTarget (enemy_gameboard);
	}

	return return_value;
}

// Throws InvalidFireResultException
void PraySelector::HitResponce (FireResult result, AbsBoardPos last_hit_pos, WarShipPrototypeList prototypes)
{
	AutoLocker m_state_locker (m_state_mutex);

	switch (result)
	{
	case fr_hit:
		{
			if (last_hit_pos == InvalidAbsBoardPos)
			{
				_ASSERT (0);
				throw InvalidFireResultException (L"m_last_hit_pos == InvalidAbsBoardPos");
			}

			m_last_succesful_pos = last_hit_pos;
			
			if (m_state == pss_free)
			{
				m_state = pss_catched;
				m_damage_image.image_pos = last_hit_pos;
			}

			m_damage_image.damage_image->AddPoint (last_hit_pos - m_damage_image.image_pos);

			m_damage_image.image_pos = m_damage_image.image_pos + m_damage_image.damage_image->Sort ();

			break;
		}
	case fr_miss:
		{
			// Must not be any reaction
			break;
		}
	case fr_kill:
		{
			m_last_succesful_pos = InvalidAbsBoardPos;

			m_state = pss_free;

			for (WarShipPrototypeList::const_iterator counter = prototypes.cbegin (); counter != prototypes.cend (); counter++)
			{
				if (ArePrototypesEqual (*counter, m_damage_image.damage_image))
				{
					if ((*counter)->GetLimit () > 0)
					{
						(*counter)->LimitDown ();
					}

					break;
				}
			}

			m_damage_image.ClearImage ();

			break;
		}
	default:
		{
			throw InvalidFireResultException (L"switch (result) going to 'default' case");
		}
	}
}


AbsBoardPos PraySelector::SelectRandomTarget (std::shared_ptr<GameBoard> enemy_gameboard)
{
	SIZE max_prototype_dimentions;
	max_prototype_dimentions.cx = 1;
	max_prototype_dimentions.cy = 1;

	for (WarShipPrototypeList::const_iterator counter = enemy_gameboard->GetPrototypeList().cbegin (); counter != enemy_gameboard->GetPrototypeList().cend (); counter++)
	{
		if ((*counter)->GetLimit())
		{
			continue;
		}

		RelBoardPosList points = (*counter)->GetPoints ();

		for (RelBoardPosList::const_iterator p_counter = points.cbegin (); p_counter != points.cend (); p_counter++)
		{
			if ((p_counter->x + 1) > max_prototype_dimentions.cx)
			{
				max_prototype_dimentions.cx = p_counter->x + 1;
			}
			if ((p_counter->y + 1) > max_prototype_dimentions.cy)
			{
				max_prototype_dimentions.cy = p_counter->y + 1;
			}
		}
	}

	int gameboard_size = enemy_gameboard->GetSize ();

	std::vector<int> column_sizes;
	std::vector<int> line_sizes;

	TargetLineCellVector line_targets;

	column_sizes.resize (gameboard_size, 0);
	line_sizes.resize (gameboard_size, 0);

	for (int x_counter = 0; x_counter < gameboard_size; x_counter = x_counter + 1)
	{
		for (int y_counter = 0; y_counter < gameboard_size; y_counter = y_counter + 1)
		{
			if (enemy_gameboard->Cell (AbsBoardPos (x_counter, y_counter)).cell_type == ct_unknown)
			{
				column_sizes[x_counter] = column_sizes[x_counter] + 1;
				line_sizes[y_counter] = line_sizes[y_counter] + 1;
			}
			else
			{
				column_sizes[x_counter] = 0;
				line_sizes[y_counter] = 0;
			}

			if (column_sizes[x_counter] % max_prototype_dimentions.cy == 0)
			{
				line_targets.push_back (TargetLineCell (lt_column, AbsBoardPos (x_counter, y_counter)));
			}
			if (line_sizes[y_counter] % max_prototype_dimentions.cx == 0)
			{
				line_targets.push_back (TargetLineCell (lt_string, AbsBoardPos (x_counter, y_counter)));
			}
		}
	}

	if (line_targets.size () == 0)
	{
		throw InvalidTargetSearchResultException (L"m_line_targets.size () == 0");
	}

	return line_targets[((line_targets.size () - 1) * rand ()) / RAND_MAX].target_cell;
}


AbsBoardPos PraySelector::SelectWarshipTarget (std::shared_ptr<GameBoard> enemy_gameboard)
{
	WarShipPrototypeList prototypes = enemy_gameboard->GetPrototypeList ();

	TargetCellVector targets;

	for (WarShipPrototypeList::const_iterator prototype = prototypes.cbegin (); prototype != prototypes.cend (); prototype++)
	{
		if ((*prototype)->GetLimit() == 0)
		{
			continue;
		}

		RelBoardPosList prototype_points = (*prototype)->GetPoints ();
		RelBoardPosList image_points = m_damage_image.damage_image->GetPoints ();

		if (prototype_points.size () - image_points.size () <= 0)
		{
			continue;
		}

		SIZE prototype_rect;
		prototype_rect.cx = 0;
		prototype_rect.cy = 0;

		for (RelBoardPosList::const_iterator counter = prototype_points.cbegin (); counter != prototype_points.cend (); counter++)
		{
			if (counter->x > prototype_rect.cx)
			{
				prototype_rect.cx = counter->x;
			}
			if (counter->y > prototype_rect.cy)
			{
				prototype_rect.cy = counter->y;
			}
		}

		for (int x_counter = 0; x_counter <= prototype_rect.cx; x_counter = x_counter + 1)
		{
			for (int y_counter = 0; y_counter <= prototype_rect.cy; y_counter = y_counter + 1)
			{
				bool is_image_allowed = true;

				for (RelBoardPosList::const_iterator img_counter = image_points.cbegin (); img_counter != image_points.cend (); img_counter++)
				{
					RelBoardPosList::const_iterator find_result = std::find (prototype_points.cbegin (), prototype_points.cend (), RelBoardPos (x_counter + img_counter->x, y_counter + img_counter->y));

					if (find_result == prototype_points.cend ())
					{
						is_image_allowed = false;

						break;
					}
				}

				if (is_image_allowed)
				{
					for (RelBoardPosList::const_iterator prot_counter = prototype_points.cbegin (); prot_counter != prototype_points.cend (); prot_counter++)
					{
						AbsBoardPos abs_target_point_pos = m_damage_image.image_pos + RelBoardPos (prot_counter->x - x_counter, prot_counter->y - y_counter);

						int gameboard_size = enemy_gameboard->GetSize ();

						if (abs_target_point_pos.x < 0 || abs_target_point_pos.y < 0 || abs_target_point_pos.x >= gameboard_size || abs_target_point_pos.y >= gameboard_size)
						{
							continue;
						}
						if (enemy_gameboard->Cell (abs_target_point_pos).cell_type != ct_unknown)
						{
							continue;
						}

						TargetCellVector::const_iterator find_result = std::find (targets.cbegin (), targets.cend (), abs_target_point_pos);

						if (find_result == targets.cend ())
						{
							targets.push_back (TargetCell (abs_target_point_pos, (*prototype)->GetLimit ()));
						}
						else
						{
							int index = find_result - targets.cbegin ();
							targets[index] = targets[index] + (*prototype)->GetLimit ();
						}
					}
				}
			}
		}
	}

	if (targets.size () == 0)
	{
		throw InvalidTargetSearchResultException (L"targets.size () == 0");
	}

	std::sort (targets.begin (), targets.end ());

	for (int max_priority = targets.back ().priority;;)
	{
		if (targets.begin ()->priority < max_priority)
		{
			targets.erase (targets.begin ());
		}
		else
		{
			break;
		}
	}

	return targets[((targets.size () - 1) * rand ()) / RAND_MAX].pos;
}


std::shared_ptr<IPraySelector> CreateExpertSelector ()
{
	return std::shared_ptr<IPraySelector> (new PraySelector());
}
