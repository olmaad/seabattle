#pragma once

#include "GameBoard.h"

#include <libSystem/Printer.h>

class GameEnginePrinter
{
public:
	GameEnginePrinter(const std::shared_ptr<Printer>& printer);

	void WarshipAdded(std::wstring alias, WarShipPrototypePtr prototype, AbsBoardPos warship_pos);
	void GameStarted(const GameBoard& gameboard, std::wstring alias);
	void MoveActed(std::wstring alias, AbsBoardPos pos, FireResult result);
	void GameEnded(std::wstring player_alias, std::wstring winner_alias, const GameBoard& my_gameboard, const GameBoard& enemy_gameboard);

private:
	std::shared_ptr<Printer> m_printer;

	std::wstring GetGameboardLine(const GameBoard& gameboard, int line);

};
