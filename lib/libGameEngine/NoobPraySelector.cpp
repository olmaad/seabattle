#include "PraySelctorInterface.h"

class NoobPraySelector : public IPraySelector
{
public:
	NoobPraySelector ();
	~NoobPraySelector ();

	AbsBoardPos SelectPray (std::shared_ptr<GameBoard> enemy_gameboard);
	void HitResponce (FireResult result, AbsBoardPos last_hit_pos, WarShipPrototypeList prototypes);
};

NoobPraySelector::NoobPraySelector ()
{
}


NoobPraySelector::~NoobPraySelector ()
{
}


AbsBoardPos NoobPraySelector::SelectPray (std::shared_ptr<GameBoard> enemy_gameboard)
{
	for (int try_counter = 0; try_counter < 100; try_counter++)
	{
		//((line_targets.size () - 1) * rand ()) / RAND_MAX

		AbsBoardPos target (((enemy_gameboard->GetSize () - 1) * rand ()) / RAND_MAX, ((enemy_gameboard->GetSize () - 1) * rand ()) / RAND_MAX);

		if (enemy_gameboard->Cell(target).cell_type == ct_unknown)
		{
			return target;
		}
	}

	for (int x_counter = 0;x_counter < enemy_gameboard->GetSize();x_counter ++)
	{
		for (int y_counter = 0;y_counter < enemy_gameboard->GetSize();y_counter ++)
		{
			AbsBoardPos target (x_counter, y_counter);

			if (enemy_gameboard->Cell (target).cell_type == ct_unknown)
			{
				return target;
			}
		}
	}

	throw InvalidTargetSearchResultException (L"cant find free cell for fire target");
}


void NoobPraySelector::HitResponce (FireResult result, AbsBoardPos last_hit_pos, WarShipPrototypeList prototypes)
{
}


std::shared_ptr<IPraySelector> CreateNoobSelector ()
{
	return std::shared_ptr<IPraySelector> (new NoobPraySelector());
}
