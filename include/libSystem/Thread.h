#pragma once

#include "Mutex.h"

#include <Windows.h>

class NotificationEvent
{
public:
	NotificationEvent ();
	~NotificationEvent ();

	HANDLE GetHandle () {return m_event;}

	void Set ();
	void Reset ();

private:
	HANDLE m_event;

};

class Thread
{
public:
	Thread();
	~Thread();

	void StartAsync ();
	void StopAsync ();
	virtual void Run () = 0;

	void WaitForThreadFinished ();

	enum
	{
		rs_not_running = 0,
		rs_running,
		rs_stopped,
		rs_ended
	};

protected:
	HANDLE m_thread;
	DWORD m_thread_id;

	Mutex m_state_mutex;

	NotificationEvent m_stop_event;

	volatile int m_run_state;

	static DWORD WINAPI TreadProc (LPVOID lp_parameter);

private:
	Thread (const Thread&);

	Thread& operator = (const Thread&);
};
