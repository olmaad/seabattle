#pragma once

#include <sstream>

#define THROW_TEXT(_text)																\
{																						\
	std::ostringstream exception_text;													\
	exception_text << std::dec << __LINE__ << ":" << __FUNCTION__ << ": " << _text;		\
	throw std::exception (exception_text.str ().c_str ());								\
}

class BasicException
{
public:
	BasicException ();
	~BasicException ();
};

class TextException : public BasicException
{
public:
	TextException ();
	TextException (std::wstring text);

	std::wstring ToString ();

protected:
	std::wstring m_text;
};

class InvalidFireResultException : public TextException
{
public:
	InvalidFireResultException (std::wstring text) : TextException (text) {}
};

class InvalidTargetSearchResultException : public TextException
{
public:
	InvalidTargetSearchResultException (std::wstring text) : TextException (text) {}
};

class InvalidArgumentException : public TextException
{
public:
	InvalidArgumentException (std::wstring text) : TextException (text) {}
};

class InvalidPrivateParameterStateException : public TextException
{
public:
	InvalidPrivateParameterStateException (std::wstring text) : TextException (text) {}
};

class WrongCalculationException : public TextException
{
public:
	WrongCalculationException (std::wstring text) : TextException (text) {}
};

class InvalidGameStateException : public TextException
{
public:
	InvalidGameStateException (std::wstring text) : TextException (text) {}
};

class InvalidFunctionResultException : public TextException
{
public:
	InvalidFunctionResultException (std::wstring text) : TextException (text) {}
};

class InvalidFunctionCallException : public TextException
{
public:
	InvalidFunctionCallException (std::wstring text) : TextException (text) {}
};
