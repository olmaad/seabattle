#pragma once

#include <WinSock2.h>
#include <Ws2tcpip.h>
#include <mswsock.h>

#include <string>

#define TRACE_SOCKET(_text) \
{ \
	std::ostringstream trace_text; \
	trace_text << std::dec << __LINE__ << ":" << __FUNCTION__ << ":" << "WSAERROR\"" << DecodeWsaError (WSAGetLastError ()) << "\"" << ": " << _text << "\n"; \
	OutputDebugStringA (trace_text.str ().c_str ()); \
}

std::string DecodeWsaError (int error_code);

struct AsyncRequest
	{
		explicit AsyncRequest (size_t size);
		~AsyncRequest ();

		WSAOVERLAPPED m_overlapped_packet;
		WSABUF m_wsa_buffer;
		char* m_buffer;
		size_t m_size;
	};

class BasicSocket
{
public:
	BasicSocket ();
	virtual ~BasicSocket ();

	operator SOCKET () {return m_socket;}

	//bool GetCompletedPacket (size_t& packet_data_size, OVERLAPPED*& packet);

	//должна присутствовать возможность получения handle на completion port

protected:
	SOCKET m_socket;

private:
	BasicSocket (const BasicSocket&);
	BasicSocket& operator = (const BasicSocket&);
};

class ServerSocket : public BasicSocket
{
public:
	ServerSocket ();
	~ServerSocket ();

	std::string GetRemoteAddress ();
	std::string GetRemotePort ();

	void OnAccept (char* accept_address_buffer, size_t accept_address_buffer_legth);

private:
	//char m_accept_address_buffer [2 * (sizeof (sockaddr_in) + 16) + sizeof (NPHandshakeResponse)];

	SOCKADDR m_local_addr;
	SOCKADDR m_remote_addr;

	ServerSocket (const ServerSocket&);
	ServerSocket& operator = (const ServerSocket&);

};

class ListeningSocket : public BasicSocket
{
public:
	ListeningSocket () {}

	void Bind (int port_number, DWORD ip4_address);

	void ListenForOneConnection ();
	std::unique_ptr<ServerSocket> AsyncAccept (AsyncRequest& accept_packet);

private:
	ListeningSocket (const ListeningSocket&);
	ListeningSocket& operator = (const ListeningSocket&);
};

class ClientSocket : public BasicSocket
{
public:
	ClientSocket ();
	~ClientSocket ();

	void Connect  (unsigned __int32 be_server_address, int server_port);
};
