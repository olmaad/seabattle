#pragma once

#include <fstream>
#include <string>
#include <iomanip>

#define DEFAULT_FILENAME "log.log"

#define TRACE_TEXT(_text) \
{ \
	std::ostringstream _trace_text; \
	_trace_text << std::dec << __LINE__ << ":" << __FUNCTION__ << ": " << (_text) << "\n"; \
	OutputDebugStringA (_trace_text.str ().c_str ()); \
}

#define TRACE_TEXT_CONTEXT(_text, _context) \
{ \
	std::ostringstream trace_text; \
	trace_text << std::dec << __LINE__ << ":" << __FUNCTION__ << ":" << "0x" << std::hex << std::setw(sizeof(void*)) << _context << ": " << _text << "\n"; \
	OutputDebugStringA (trace_text.str ().c_str ()); \
}

class Printer
{
public:
	Printer ();
	~Printer ();

	void PrintLine (std::wstring str);
};
