#pragma once

#include <Windows.h>


class Mutex
{
public:
	Mutex ();
	~Mutex ();

	void Acquire ();
	void Release ();

private:
	HANDLE m_mutex;

	Mutex (const Mutex&);
	
	Mutex& operator = (const Mutex&);
};


class AutoLocker
{
public:
	AutoLocker (Mutex& mutex);
	~AutoLocker ();

private:
	Mutex& m_mutex;

	AutoLocker ();
	AutoLocker (const AutoLocker&);

	AutoLocker& operator = (const AutoLocker&);
};
