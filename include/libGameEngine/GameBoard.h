#pragma once

#include "IGameBoardShipController.h"

#include <libSystem/SeaExceptions.h>

#include <list>
#include <memory>

enum FireResult
{
	fr_hit = 0,
	fr_miss,
	fr_kill
};

struct RelBoardPos		//Координата клетки доски (относительно другой клетки)
{
	RelBoardPos (int src_x, int src_y) : x (src_x), y (src_y)
	{
	}

	bool operator == (const RelBoardPos& pos) const
	{
		return pos.x == x && pos.y == y;
	}
	bool operator != (const RelBoardPos& pos) const
	{
		return pos.x != x || pos.y != y;
	}
	bool operator < (const RelBoardPos& pos) const
	{
		return x < pos.x || (x == pos.x && y < pos.y);
	}
	RelBoardPos& operator - (const RelBoardPos& pos)
	{
		x = x - pos.x;
		y = y - pos.y;

		return *this;
	}

	int x;
	int y;
};

struct AbsBoardPos		//Координата клетки доски (относительно левого верхнего угла)
{
	AbsBoardPos (int src_x, int src_y) : x (src_x), y (src_y)
	{
	}

	bool operator == (const AbsBoardPos& pos) const
	{
		return pos.x == x && pos.y == y;
	}
	bool operator != (const AbsBoardPos& pos) const
	{
		return pos.x != x || pos.y != y;
	}
	AbsBoardPos operator + (const RelBoardPos& rel_pos) const
	{
		return AbsBoardPos (x + rel_pos.x, y + rel_pos.y);
	}
	RelBoardPos operator - (const AbsBoardPos& abs_pos) const
	{
		return RelBoardPos (x - abs_pos.x, y - abs_pos.y);
	}

	int x;
	int y;
};

#define InvalidAbsBoardPos AbsBoardPos (-1, -1)

typedef std::list<RelBoardPos> RelBoardPosList;

class WarShipPrototype;

typedef std::shared_ptr<WarShipPrototype> WarShipPrototypePtr;

class WarShipPrototype		//WarShip сам по себе (не привязан к доске)
{
public:
	static size_t prototype_counter;

	WarShipPrototype (const RelBoardPosList& src_points, const int src_limit);		//m_points (src_points.begin (), src_points.end ()), m_limit = src_limit), m_allocated = 0
	WarShipPrototype ();
	~WarShipPrototype ();		//:peka: <- Что это ? :peka::peka::spk:
	void AllocateShip ();		//m_allocated = m_allocated +1;
	void DeAllocateShip ();		//m_allocated = m_allocated -1;
	const RelBoardPosList& GetPoints () const;		//Достает m_points

	bool AddPoint (RelBoardPos cell_pos);
	void DelPoint (RelBoardPos cell_pos);

	WarShipPrototypePtr Clone () const;

	void LimitUp ()
	{
		m_limit = m_limit +1;
	}
	void LimitDown ()
	{
		m_limit = m_limit -1;
	}

	const int GetAllocated () const
	{
		return m_allocated;
	}
	const int GetLimit () const
	{
		return m_limit;
	}
	
	enum
	{
		max_size = 5
	};

	RelBoardPos Sort ();
private:
	WarShipPrototype (const WarShipPrototype&);

	RelBoardPosList m_points;		//Координаты точек корабля (относительно левого верхнего угла описаного прямоугольника)
	int m_limit;		//Максимальное число кораблей данной формы на поле
	int m_allocated;		//Количество уже находящихся на поле кораблей

};

typedef std::list<WarShipPrototypePtr> WarShipPrototypeList;

class WarShip		//Размещенный WarShipPrototype
{
public:

	static size_t warship_counter;

	WarShip (WarShipPrototypePtr src_prototype, const AbsBoardPos& src_pos);		//m_prototype = src_prototype, m_pos = src_pos, зовет AllocateShip ()
	~WarShip ();		//Зовет DeAllocateShip ()

	WarShipPrototypePtr GetPrototype ()
	{
		return m_prototype;
	}
	AbsBoardPos& GetPos ()
	{
		return m_pos;
	}

private:
	WarShipPrototypePtr m_prototype;
	AbsBoardPos m_pos;		//Позиция корабля (верхнего левого угла описаного прямоугольника)

};

typedef std::shared_ptr<WarShip> WarShipPtr;
typedef std::list<WarShipPtr> WarShipPtrList;

enum CellType		//Состояние клетки поля
{
	ct_unknown = 0,
	ct_empty,
	ct_ship,
	ct_prototype,
	ct_damaged,
	ct_miss,
	ct_conflict,
	ct_killed
};

struct BoardCell		//Элементарная часть доски. Состояние клетки и указатель на WarShip/WarShipPrototype, находящийся в этой клетке
{
	CellType cell_type;
	WarShip* ship;
};

class GameBoard		//Доска
{
public:
	GameBoard ();		//m_size = 0, m_board_storage = 0
	~GameBoard ();		//Чистка/удаление списка кораблей и памяти доски
	
	void CreateEmpty (int size);		//Создание чистой доски (через CreateAndFill ct_empty)
	void CreateEnemyUnknown (int size);		//Создание неизвестной доски (через CreateAndFill ct_unknown)

	const WarShipPrototypePtr& AddPrototype (WarShipPrototypePtr adding_prototype, RelBoardPos* offset);
	void DelPrototype (WarShipPrototypePtr deleting_prototype);
	void SetAllowedWarShips (const WarShipPrototypeList& allowed_warships);

	WarShipPrototypeList::const_iterator FindEqualPrototype (const WarShipPrototypePtr src_prototype);

	void AddWarShip (WarShipPrototypePtr adding_warship, const AbsBoardPos& adding_warship_pos);
	void DelWarShip (const AbsBoardPos& delete_pos);

	void RecurseOpenEmpty (AbsBoardPos pos);

	void MarkHit (const AbsBoardPos& hit_pos);
	void MarkMiss (const AbsBoardPos& hit_pos);
	void MarkKill (const AbsBoardPos& hit_pos);

	FireResult EnemyHit (const AbsBoardPos& hit_pos);

	bool PlaceTest (const WarShipPrototypePtr prototype, const AbsBoardPos& pos) const;
	BoardCell& Cell (const AbsBoardPos& pos);
	const BoardCell& Cell (const AbsBoardPos& pos) const;
	int GetSize () const;
	const WarShipPrototypeList& GetPrototypeList ();
	const WarShipPtrList& GetWarshipList () const;

	void CloneGameboard (GameBoard& src_gameboarg);

	bool ConsumeGameboard (GameBoard& src_gameboarg);

	int GetWarshipCount ()
	{
		return m_ships_counter;
	}

	// Throws InvalidPrivateParameterStateException
	void BindController (IGameBoardShipController* src_controller)
	{
		if (m_controller != 0)
		{
			throw InvalidPrivateParameterStateException (L"m_controller != 0 - controller already binded");
		}

		m_controller = src_controller;
	}

	// Throws InvalidPrivateParameterStateException
	void UnBindController (IGameBoardShipController* src_controller)
	{
		if (src_controller != m_controller)
		{
			throw InvalidPrivateParameterStateException (L"src_controller != m_controller - unbinding unknown controller");
		}

		m_controller = 0;
	}

	enum 
	{
		min_size = 5,
		max_size = 20,
	};

private:
	GameBoard (const GameBoard&);
	GameBoard& operator = (const GameBoard&);

protected:
	IGameBoardShipController* m_controller;

	void CreateAndFill (int size, CellType fill);

	int m_size;

	int m_ships_counter;

	void* m_board_storage;
	WarShipPrototypeList m_prototype_list;
	WarShipPtrList m_ships_list;
};


bool ArePrototypesEqual (const WarShipPrototypePtr src_prototype, const WarShipPrototypePtr prototype);
