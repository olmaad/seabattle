#pragma once

class WarShipPrototype;

class IGameBoardShipController
{
public:
	virtual void NotifyPrototypeAdded(const WarShipPrototype* prototype) = 0;
	virtual void NotifyPrototypeDeleted(const WarShipPrototype* prototype) = 0;
	virtual void NotifyWarshipCountChanged() = 0;

	virtual bool IsHost() = 0;

};
