#pragma once

#include "GameEntities.h"
#include "GameBoard.h"

#include <memory>

struct ShotShipAbility;
struct MoveShipAbility;
struct TorpedoShipAbility;
struct RocketShipAbility;
struct MAACShipAbility;
struct IllusionShipAbility;
struct EMPShipAbility;
struct ScoutingShipAbility;
struct FighterDroneShipAbility;
struct InterceptorDroneShipAbility;
struct ProspectorDroneShipAbility;
struct ProspectorDroneUpdateShipAbility;
struct CarpetBombingShipAbility;
struct BarrageFireShipAbility;
struct FullCombatReadinessShipAbility;

class IShipAbilityHandler
{
public:
	virtual void Handle(ShotShipAbility&) = 0;
	virtual void Handle(MoveShipAbility&) = 0;
	virtual void Handle(TorpedoShipAbility&) = 0;
	virtual void Handle(RocketShipAbility&) = 0;
	virtual void Handle(MAACShipAbility&) = 0;
	virtual void Handle(IllusionShipAbility&) = 0;
	virtual void Handle(EMPShipAbility&) = 0;
	virtual void Handle(ScoutingShipAbility&) = 0;
	virtual void Handle(FighterDroneShipAbility&) = 0;
	virtual void Handle(InterceptorDroneShipAbility&) = 0;
	virtual void Handle(ProspectorDroneShipAbility&) = 0;
	virtual void Handle(ProspectorDroneUpdateShipAbility&) = 0;
	virtual void Handle(CarpetBombingShipAbility&) = 0;
	virtual void Handle(BarrageFireShipAbility&) = 0;
	virtual void Handle(FullCombatReadinessShipAbility&) = 0;

};

enum ShipAbilityType
{
	sa_shot = 0,
	sa_move,
	sa_torpedo,
	sa_rocket,
	sa_maac,
	sa_illusion,
	sa_emp,
	sa_scouting,
	sa_fighter_drone,
	sa_interceptor_drone,
	sa_prospector_drone,
	sa_prospector_drone_update,
	sa_carpet_bombing,
	sa_barrage_fire,
	sa_full_combat_readiness,
	sa_last
};

struct ShipAbility
{
	ShipAbility() = delete;

	ShipAbility(ShipAbilityType type);
	virtual ~ShipAbility();

	virtual void Accept(IShipAbilityHandler& handler) = 0;

	const ShipAbilityType m_type;
};

typedef std::shared_ptr<ShipAbility> ShipAbilityPtr;

struct ShotShipAbility : public ShipAbility
{
	ShotShipAbility(AbsBoardPos pos);

	void Accept(IShipAbilityHandler& handler) override;

	const AbsBoardPos m_pos;

};

struct MoveShipAbility : public ShipAbility
{
	MoveShipAbility(AbsBoardPos from, AbsBoardPos to);

	void Accept(IShipAbilityHandler& handler) override;

	const AbsBoardPos m_from;
	const AbsBoardPos m_to;

};

struct TorpedoShipAbility : public ShipAbility
{
	TorpedoShipAbility(AbsBoardPos entry_pos);

	void Accept(IShipAbilityHandler& handler) override;

	AbsBoardPos m_entry_pos;

};

struct RocketShipAbility : public ShipAbility
{
	RocketShipAbility(AbsBoardPos pos);

	void Accept(IShipAbilityHandler& handler) override;

	const AbsBoardPos m_pos;

};

struct MAACShipAbility : public ShipAbility
{
	MAACShipAbility(AbsBoardPos pos);

	void Accept(IShipAbilityHandler& handler) override;

	const AbsBoardPos m_pos;

};

struct IllusionShipAbility : public ShipAbility
{
	enum IllusionDirection
	{
		id_horizontal = 0,
		id_vertical
	};

	IllusionShipAbility(AbsBoardPos pos, IllusionDirection direction);

	void Accept(IShipAbilityHandler& handler) override;

	const AbsBoardPos m_pos;
	const IllusionDirection m_direction;

};

struct EMPShipAbility : public ShipAbility
{
	EMPShipAbility(AbsBoardPos pos);

	void Accept(IShipAbilityHandler& handler) override;

	const AbsBoardPos m_pos;

};

struct ScoutingShipAbility : public ShipAbility
{
	ScoutingShipAbility(AbsBoardPos pos);

	void Accept(IShipAbilityHandler& handler) override;

	const AbsBoardPos m_pos;

};

struct FighterDroneShipAbility : public ShipAbility
{
	FighterDroneShipAbility(AbsBoardPos entry_pos);

	void Accept(IShipAbilityHandler& handler) override;

	const AbsBoardPos m_entry_pos;

};

struct InterceptorDroneShipAbility : public ShipAbility
{
	InterceptorDroneShipAbility(eid_t target);

	void Accept(IShipAbilityHandler& handler) override;

	eid_t m_target;

};

struct ProspectorDroneShipAbility : public ShipAbility
{
	ProspectorDroneShipAbility(AbsBoardPos entry_pos);

	void Accept(IShipAbilityHandler& handler) override;

	const AbsBoardPos m_entry_pos;

};

struct ProspectorDroneUpdateShipAbility : public ShipAbility
{
	ProspectorDroneUpdateShipAbility(AbsBoardPos pos);

	void Accept(IShipAbilityHandler& handler) override;

	const AbsBoardPos m_pos;

};

struct CarpetBombingShipAbility : public ShipAbility
{
	CarpetBombingShipAbility(AbsBoardPos pos);

	void Accept(IShipAbilityHandler& handler) override;

	const AbsBoardPos m_pos;

};

struct BarrageFireShipAbility : public ShipAbility
{
	BarrageFireShipAbility(AbsBoardPos pos);

	void Accept(IShipAbilityHandler& handler) override;

	const AbsBoardPos m_pos;

};

struct FullCombatReadinessShipAbility : public ShipAbility
{
	FullCombatReadinessShipAbility();

	void Accept(IShipAbilityHandler& handler) override;

};
