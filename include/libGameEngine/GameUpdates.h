#pragma once

#include "GameBoard.h"

struct ShotResultUpdate;
struct ScoutingResultUpdate;
struct ProspectorDroneUpdate;
struct CarpetBombingResultUpdate;

class IGameUpdateHandler
{
public:
	virtual void Handle(const ShotResultUpdate&) = 0;
	virtual void Handle(const ScoutingResultUpdate&) = 0;
	virtual void Handle(const ProspectorDroneUpdate&) = 0;
	virtual void Handle(const CarpetBombingResultUpdate&) = 0;

};

enum GameUpdateType
{
	gu_shot_result = 0,
	gu_scouting_result,
	gu_prospector_drone,
	gu_carpet_bombing_result,
	gu_last
};

struct GameUpdateBase
{
	GameUpdateBase(GameUpdateType type);

	virtual void Accept(IGameUpdateHandler& handler) = 0;

	const GameUpdateType m_type;
	
};

typedef std::shared_ptr<GameUpdateBase> GameUpdatePtr;

struct ShotResultUpdate : public GameUpdateBase
{
	ShotResultUpdate(FireResult result, AbsBoardPos pos);

	void Accept(IGameUpdateHandler& handler) override;

	const FireResult m_result;
	const AbsBoardPos m_pos;

};

struct ScoutingResultUpdate : public GameUpdateBase
{
	ScoutingResultUpdate();

	void Accept(IGameUpdateHandler& handler) override;

};

struct ProspectorDroneUpdate : public GameUpdateBase
{
	ProspectorDroneUpdate();

	void Accept(IGameUpdateHandler& handler) override;

};

struct CarpetBombingResultUpdate : public GameUpdateBase
{
	CarpetBombingResultUpdate();

	void Accept(IGameUpdateHandler& handler) override;

};
