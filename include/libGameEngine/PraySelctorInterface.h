#pragma once
#include "GameBoard.h"


class IPraySelector
{
public:
	virtual AbsBoardPos SelectPray (std::shared_ptr<GameBoard> enemy_gameboard) = 0;
	virtual void HitResponce (FireResult result, AbsBoardPos last_hit_pos, WarShipPrototypeList prototypes) = 0;

};

std::shared_ptr<IPraySelector> CreateNoobSelector ();
std::shared_ptr<IPraySelector> CreateExpertSelector ();
