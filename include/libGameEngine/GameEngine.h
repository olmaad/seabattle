#pragma once

#include "GameBoard.h"
#include "ShipAbilities.h"
#include "GameUpdates.h"
#include "GameEntities.h"

#include <memory>
#include <string>

#include <libSystem/Printer.h>

typedef __int32 gen_t;
typedef __int32 uid_t;

enum GameState
{
	gs_not_ready = 0,
	gs_waiting_for_start,
	gs_my_move,
	gs_my_move_wait_for_response,
	gs_enemy_move,
	gs_win,
	gs_fail,
	gs_canceled,
	gs_final
};

enum EndGameState
{
	egs_win = 0,
	egs_fail,
	egs_canceled
};

enum EmulatorDifficulty
{
	ed_noob = 0,
	ed_expert
};

//// GameController interface from GameEngine side. GameEngine can call its functions to notify about changes in game.
class IGameControllerNotify
{
public:
	virtual void UpdGameState (GameState state, const std::wstring& reason) = 0;
	virtual void UpdEnemyGB () = 0;
	virtual void UpdMyGB () = 0;
	virtual void NotifyGameStarted () = 0;

};

class IControllerNetNotify
{
public:
	virtual void PlayerConnected () = 0;
	virtual void GameProcessEnded () = 0;

};

//// GameEngine interface from Game Controller side. GameController can call its functions to affect the game
class IGameControllerSide
{
public:
	virtual void IStartGame() = 0;
	virtual void IFire(AbsBoardPos hit_pos) = 0;
	virtual void IUseAbility(ShipAbilityPtr ability) = 0;
	virtual void IResign() = 0;
	virtual void ISetGameParameters(std::shared_ptr<GameBoard> my_gameboard, std::shared_ptr<GameBoard> enemy_shadow_gameboard, bool am_first) = 0;
	virtual void IGetGameParameters(int& gameboard_size, bool& am_first, WarShipPrototypeList& prototypes) = 0;
	virtual std::wstring IGetEnemyAlias() = 0;
	virtual std::wstring IGetMyAlias() = 0;

};

// Game participant interface
class IGameEngineSide
{
public:
	// Клиент спрашивает параметры игры
	virtual void GetGameboardParameters(int& gameboard_size, bool& is_server_moves_first, WarShipPrototypeList& warships) = 0;
	// Посылает client-controller client-game-engin-у для создания place-ships-window
	virtual void ConnectionEstablished() = 0;
	// Посылает (только) клиент, когда готов начать игру
	virtual void StartGame(gen_t event_number) = 0;
	// Сообщение об окончании игры
	virtual void EndGame(gen_t event_number, EndGameState enemy_state, std::shared_ptr<GameBoard> enemy_gameboard) = 0;
	// Запрос результата стрельбы
	virtual void FireRequest(gen_t event_number, AbsBoardPos hit_pos) = 0;
	// Сообщение о результате стрельбы
	virtual void FireResponse(gen_t event_number, FireResult fire_result, gen_t request_number) = 0;
	// Сообщение о использовании способности
	virtual void UseAbility(gen_t event_number, ShipAbilityPtr ability) = 0;
	// Различные игровые обновления вроде результата использования способности, или обновления сущностей
	virtual void GameUpdate(gen_t event_number, GameUpdatePtr update) = 0;
	// Сообщение о конце хода
	virtual void MoveEnded() = 0;
	// Получение gamename
	virtual std::wstring GetAlias() = 0;

};

class IGameEngineManage
{
public:
	virtual ~IGameEngineManage () {}

	virtual void ConnectOtherSide (IGameEngineSide* other_side_interface) = 0;
	virtual void ConnectGameController (IGameControllerNotify* game_controller_interface) = 0;
	virtual void ConnectNetworkNotify (IControllerNetNotify* game_controller_net_notify_interface)= 0;
	virtual void Ready () = 0;
	virtual void Unready () = 0;

};

std::shared_ptr<IGameEngineManage> CreateGameEngine (const std::wstring& alias, bool is_server, std::shared_ptr<Printer> log_printer, IGameControllerSide*& game_controller_side, IGameEngineSide*& game_engine_side);
std::shared_ptr<IGameEngineManage> CreateEnemyEmulator (const WarShipPrototypeList& prototype_list, int gameboard_size, bool is_first, bool is_server, std::shared_ptr<Printer> log_printer, EmulatorDifficulty difficulty, IGameEngineSide*& game_engine_side);

#define CONCAT_TEXT1_ALIAS_TEXT2(_text1, _text2) \
	std::wstring upd_gs_string = _text1; \
	upd_gs_string += m_other_side_interface->GetAlias(); \
	upd_gs_string += _text2;
