#pragma once

#include <libGameEngine/GameEngine.h>

#define WIN32_LEAN_AND_MEAN
#include <windows.h> // DWORD !

enum NetworkBasicState
{
	nbs_not_ready = 0,
	nbs_connected = 100,
	nbs_disconnected,
	nbs_final
};

enum NetworkServerState
{
	nss_not_ready = nbs_not_ready,

	nss_wait_for_connect,
	nss_wait_for_handshake,

	nss_connected = nbs_connected,
	nss_disconnected = nbs_disconnected,
	nss_final = nbs_final
};

enum NetworkClientState
{
	ncs_not_ready = nbs_not_ready,

	ncs_wait_for_handshake_reply,

	ncs_connected = nbs_connected,
	ncs_disconnected = nbs_disconnected,
	ncs_final = nbs_final
};

enum FinalGameState
{
	fgs_game = 0,
	fgs_endgame_sended,
	fgs_endgame_received,
	fgs_finished
};


class INetworkServer
{
public:
	// calling before IGameEngineManage::Ready ()
	virtual void SetPort (int port_number) = 0;
	virtual void SetAddress (DWORD ip4_address) = 0;
	virtual void SetAlias (const std::wstring& alias) = 0;
};


class INetworkClient
{
public:
	// calling before IGameEngineManage::Ready ()
	virtual void SetServerPort (int port_number) = 0;
	virtual void SetServerAddress (DWORD ip4_address) = 0;
	virtual void SetAlias (const std::wstring& alias) = 0;
};


std::shared_ptr<IGameEngineManage> CreateServerController (IGameEngineSide*& game_engine_side, INetworkServer*& network_server_side);
std::shared_ptr<IGameEngineManage> CreateClientController (IGameEngineSide*& game_engine_side, INetworkClient*& network_client_side);
