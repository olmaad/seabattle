#pragma once

#include <guiddef.h>

#pragma pack (push)

typedef unsigned __int32 ngen_t;
typedef unsigned __int32 nsqn_t;

enum NetworkEventType
{
	net_handshake = 0, // при соединении клиент посылает информацию о версии протокола и свой alias
	net_handshake_response, // в случае успешного рукопожатия сервер отвечает, указывая свою версию протокола, alias, размер игровой доски, очередность ходов и список прототипов
	net_client_start_game, // клиент присылает подтверждение готовности к началу игры. игра начинается сразу же после этого сигнала
	net_fire,
	net_fire_response,
	net_use_ability,
	net_game_update,
	net_end_game,
	net_ping, // посылает сервер клиенту раз в несколько секунд
	net_ping_response
};

enum ProtocolVersion
{
	protocol_version_one = 1, // 25.01.2013
	protocol_version_two = 2, // 11.04.2019
	protocol_version_current = protocol_version_two
};

enum NetworkProtocolLimits
{
	npl_max_alias_length = 256,
	npl_max_ship_length = 5,
	npl_max_packet_size = 1024
};

// {3F5C72A1-B532-4FAB-BE3E-3907227A4B65}
DEFINE_GUID(seabattle_application_guid,
	0x3f5c72a1, 0xb532, 0x4fab, 0xbe, 0x3e, 0x39, 0x7, 0x22, 0x7a, 0x4b, 0x65);

#pragma pack (4)

__declspec(align(4)) struct BoardPoint
{
	unsigned __int8 x;
	unsigned __int8 y;
};

__declspec(align(4)) struct PrototypeDescription
{
	unsigned __int32 limit;
	unsigned __int8 point_amount;
	BoardPoint points[npl_max_ship_length];
};

__declspec(align(4)) struct ShotAbilityDescription
{
	BoardPoint pos;
};

__declspec(align(4)) struct ShotResultUpdateDescription
{
	unsigned __int32 result;
	BoardPoint pos;
};

__declspec(align(4)) struct NPBasic
{
	unsigned __int8 PacketType; // NetworkEventType
	unsigned __int32 PacketSize; // размер пакета целиком
	nsqn_t SequenceNumber;
};

__declspec(align(4)) struct NPHandshake : NPBasic
{
	unsigned __int32 protocol_version;
	GUID seabattle_app_guid;
	wchar_t alias [npl_max_alias_length]; // null in end. принимающая сторона должна проверить наличие закрывающего нуля
};

__declspec(align(4)) struct NPHandshakeResponse : NPBasic
{
	unsigned __int32 protocol_version;
	GUID seabattle_app_guid;
	wchar_t alias [npl_max_alias_length];
	unsigned __int8 gameboard_size;
	unsigned __int8 is_server_move_first;
	unsigned __int32 prototype_count;
	PrototypeDescription prototypes [1]; // prototypes
};

__declspec(align(4)) struct NPClientStartGame : NPBasic
{
	ngen_t event_number;
};

__declspec(align(4)) struct NPFire : NPBasic
{
	BoardPoint fire_position;
	ngen_t event_number;
};

__declspec(align(4)) struct NPFireResponse : NPBasic
{
	unsigned __int32 request_sequence_number;
	unsigned __int32 fire_result;
	ngen_t event_number;
};

__declspec(align(4)) struct NPUseAbility : NPBasic
{
	unsigned __int32 ability_type;
	union
	{
		ShotAbilityDescription shot_ability;
	};
	ngen_t event_number;
};

__declspec(align(4)) struct NPGameUpdate : NPBasic
{
	unsigned __int32 update_type;
	union
	{
		ShotResultUpdateDescription shot_result;
	};
	ngen_t event_number;
};

__declspec(align(4)) struct NPEndGame : NPBasic
{
	unsigned __int32 end_game_state;
	unsigned __int8 gameboard_size;
	ngen_t event_number;
	char bitmap [1];
};

__declspec(align(4)) struct NPPing : NPBasic
{};

__declspec(align(4)) struct NPPingResponse : NPBasic
{};

#pragma pack (pop)
