#pragma once

#include "NetworkProtocol.h"

#include <libGameEngine/GameBoard.h>
#include <libGameEngine/GameEngine.h>

#define ALIGN_UP(_what, _alignment) (((_what) + (_alignment) -1) & (-(_alignment)))

size_t ProjectNPHandshake(const std::wstring& alias);
void FillNPHandshake(void* buffer, nsqn_t sequence_number, const std::wstring& alias);

size_t ProjectNPHandshakeResponse(const std::wstring& alias, int gameboard_size, bool is_server_move_first, const WarShipPrototypeList& prototypes);
void FillNPHandshakeResponse(void* buffer, nsqn_t sequence_number, const std::wstring& alias, int gameboard_size, bool is_server_move_first, const WarShipPrototypeList& prototypes);

size_t ProjectNPClientStartGame();
void FillNPClientStartGame(void* buffer, nsqn_t sequence_number, ngen_t event_number);

size_t ProjectNPFire(const AbsBoardPos& fire_pos);
void FillNPFire(void* buffer, nsqn_t sequence_number, const AbsBoardPos& fire_pos, ngen_t event_number);

size_t ProjectNPFireResponse(int request_sequence_number, FireResult fire_result);
void FillNPFireResponse(void* buffer, nsqn_t sequence_number, ngen_t request_sequence_number, FireResult fire_result, ngen_t event_number);

size_t ProjectNPUseAbility(const ShipAbilityPtr& ability);
void FillNPUseAbility(void* buffer, nsqn_t sequence_number, const ShipAbilityPtr& ability, ngen_t event_number);

size_t ProjectNPGameUpdate(const GameUpdatePtr& update);
void FillNPGameUpdate(void* buffer, nsqn_t sequence_number, const GameUpdatePtr& update, ngen_t event_number);

size_t ProjectNPEndGame(EndGameState end_gsme_state, const GameBoard& game_board);
void FillNPEndGame(void* buffer, nsqn_t sequence_number, EndGameState end_gsme_state, const GameBoard& game_board, ngen_t event_number);

size_t ProjectNPPing();
void FillNPPing(void* buffer, nsqn_t sequence_number);

size_t ProjectNPPingResponse();
void FillNPPingResponse(void* buffer, nsqn_t sequence_number);


class IPacketReceiver
{
public:
	virtual void ReceiveNPHandshake(nsqn_t sequence_number, const std::wstring& alias) = 0;
	virtual void ReceiveNPHandshakeResponse(nsqn_t sequence_number, const std::wstring& alias, int gameboard_size, bool is_server_move_first, const WarShipPrototypeList& prototypes) = 0;
	virtual void ReceiveNPClientStartGame(nsqn_t sequence_number, gen_t event_number) = 0;
	virtual void ReceiveNPFire(nsqn_t sequence_number, gen_t event_number, const AbsBoardPos& fire_pos) = 0;
	virtual void ReceiveNPFireResponse(nsqn_t sequence_number, gen_t event_number, gen_t request_sequence_number, FireResult fire_result) = 0;
	virtual void ReceiveNPUseAbility(nsqn_t sequence_number, gen_t event_number, const ShipAbilityPtr& ability) = 0;
	virtual void ReceiveNPGameUpdate(nsqn_t sequence_number, gen_t event_number, const GameUpdatePtr& update) = 0;
	virtual void ReceiveNPEndGame(nsqn_t sequence_number, gen_t event_number, EndGameState end_gsme_state, std::shared_ptr<GameBoard> game_board) = 0;
	virtual void ReceiveNPPing(nsqn_t sequence_number) = 0;
	virtual void ReceiveNPPingResponse(nsqn_t sequence_number) = 0;

};

void ParseNetworkPacket(const void* np_buffer, size_t np_size, IPacketReceiver& receiver);
